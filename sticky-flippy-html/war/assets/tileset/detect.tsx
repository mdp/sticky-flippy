<?xml version="1.0" encoding="UTF-8"?>
<tileset name="detect" tilewidth="70" tileheight="70" spacing="2" margin="2">
 <image source="detect.png" width="512" height="256"/>
 <tile id="0">
  <properties>
   <property name="detect" value="KEY_BLUE"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="detect" value="KEY_YELLOW"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="detect" value="LOCK"/>
   <property name="lock" value="BLUE"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="detect" value="LOCK"/>
   <property name="lock" value="YELLOW"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="detect" value="KEY_RED"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="detect" value="KEY_YELLOW"/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="detect" value="LAVA"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="detect" value="EXIT"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="detect" value="KEY_GREEN"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="detect" value="LAVA"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="detect" value="Lock"/>
   <property name="lock" value="GREEN"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="detect" value="SIGN"/>
  </properties>
 </tile>
 <tile id="12">
  <properties>
   <property name="detect" value="SPIKES"/>
  </properties>
 </tile>
 <tile id="14">
  <properties>
   <property name="detect" value="COIN"/>
  </properties>
 </tile>
 <tile id="15">
  <properties>
   <property name="detect" value="EXIT"/>
   <property name="shape" value="BOTTOM_RECT"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="detect" value="KEY_RED"/>
  </properties>
 </tile>
 <tile id="17">
  <properties>
   <property name="detect" value="LAVA"/>
   <property name="shape" value="BOTTOM_RECT"/>
  </properties>
 </tile>
 <tile id="18">
  <properties>
   <property name="detect" value="LOCK"/>
   <property name="lock" value="RED"/>
  </properties>
 </tile>
 <tile id="19">
  <properties>
   <property name="detect" value="SPIKE"/>
   <property name="shape" value="BOTTOM_RECT"/>
  </properties>
 </tile>
</tileset>
