<?xml version="1.0" encoding="UTF-8"?>
<tileset name="full" tilewidth="70" tileheight="70" spacing="2">
 <tileoffset x="2" y="2"/>
 <image source="full.png" width="1024" height="128"/>
 <tile id="1">
  <properties>
   <property name="shape" value="ARC_DOWN"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="shape" value="ARC_UP"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="shape" value="ARC_DOWN"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="shape" value="ARC_UP"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="shape" value="ARC_DOWN"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="shape" value="ARC_UP"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="shape" value="ARC_DOWN"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="shape" value="ARC_UP"/>
  </properties>
 </tile>
</tileset>
