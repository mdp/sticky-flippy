<?xml version="1.0" encoding="UTF-8"?>
<tileset name="metal" tilewidth="70" tileheight="70" spacing="2">
 <tileoffset x="2" y="2"/>
 <image source="metal.png" width="1024" height="128"/>
 <tile id="0">
  <properties>
   <property name="shape" value="FULL"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="shape" value="RIGHT_RECT"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="shape" value="TOP_RECT"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="shape" value="TOP_RECT"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="shape" value="RIGHT_RECT"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="shape" value="TOP_RECT"/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="shape" value="RIGHT_RECT"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="shape" value="FULL"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="shape" value="FULL"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="shape" value="FULL"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="shape" value="FULL"/>
  </properties>
 </tile>
</tileset>
