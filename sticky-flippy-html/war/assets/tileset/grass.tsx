<?xml version="1.0" encoding="UTF-8"?>
<tileset name="grass" tilewidth="70" tileheight="70" spacing="2">
 <tileoffset x="2" y="2"/>
 <image source="grass.png" width="512" height="256"/>
 <tile id="0">
  <properties>
   <property name="shape" value="TOP_RECT"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="shape" value="TOP_RECT"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="shape" value="TOP_RECT"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="shape" value="LEFT_CORNER"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="shape" value="TOP_RECT"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="shape" value="NONE"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="shape" value="TOP_RECT"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="shape" value="TOP_RECT"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="shape" value="TOP_RECT"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="shape" value="RIGHT_SLIDE"/>
  </properties>
 </tile>
 <tile id="12">
  <properties>
   <property name="shape" value="TOP_RECT"/>
  </properties>
 </tile>
 <tile id="14">
  <properties>
   <property name="shape" value="TOP_RECT"/>
  </properties>
 </tile>
 <tile id="15">
  <properties>
   <property name="shape" value="NONE"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="shape" value="TOP_RECT"/>
  </properties>
 </tile>
 <tile id="17">
  <properties>
   <property name="shape" value="TOP_RECT"/>
  </properties>
 </tile>
 <tile id="18">
  <properties>
   <property name="shape" value="LEFT_SLIDE"/>
  </properties>
 </tile>
 <tile id="19">
  <properties>
   <property name="shape" value="RIGHT_CORNER"/>
  </properties>
 </tile>
 <tile id="20">
  <properties>
   <property name="shape" value="TOP_RECT"/>
  </properties>
 </tile>
</tileset>
