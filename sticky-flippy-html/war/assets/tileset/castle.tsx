<?xml version="1.0" encoding="UTF-8"?>
<tileset name="castle" tilewidth="70" tileheight="70" spacing="2">
 <tileoffset x="2" y="2"/>
 <image source="castle.png" width="512" height="256"/>
 <tile id="3">
  <properties>
   <property name="shape" value="RIGHT_CORNER"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="shape" value="NONE"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="shape" value="RIGHT_SLIDE"/>
  </properties>
 </tile>
 <tile id="15">
  <properties>
   <property name="shape" value="NONE"/>
  </properties>
 </tile>
 <tile id="18">
  <properties>
   <property name="shape" value="LEFT_SLIDE"/>
  </properties>
 </tile>
 <tile id="19">
  <properties>
   <property name="shape" value="LEFT_CORNER"/>
  </properties>
 </tile>
</tileset>
