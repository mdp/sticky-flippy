<?xml version="1.0" encoding="UTF-8"?>
<tileset name="newenemies" tilewidth="70" tileheight="70">
 <image source="newenemies.png" width="512" height="1024"/>
 <tile id="0">
  <properties>
   <property name="move" value="UP"/>
   <property name="type" value="FLY"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="move" value="UP"/>
   <property name="type" value="BAT"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="move" value="UP"/>
   <property name="type" value="BEE"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="move" value="UP"/>
   <property name="type" value="TRIPLATE"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="move" value="DOWN"/>
   <property name="type" value="FLY"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="move" value="DOWN"/>
   <property name="type" value="BAT"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="move" value="DOWN"/>
   <property name="type" value="BEE"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="move" value="DOWN"/>
   <property name="type" value="TRIPLATE"/>
  </properties>
 </tile>
 <tile id="14">
  <properties>
   <property name="move" value="LEFT"/>
   <property name="type" value="ALIEN"/>
  </properties>
 </tile>
 <tile id="15">
  <properties>
   <property name="move" value="LEFT"/>
   <property name="type" value="RAT"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="move" value="LEFT"/>
   <property name="type" value="PLATE"/>
  </properties>
 </tile>
 <tile id="18">
  <properties>
   <property name="move" value="UP"/>
   <property name="type" value="PLATE"/>
  </properties>
 </tile>
 <tile id="21">
  <properties>
   <property name="move" value="RIGHT"/>
   <property name="type" value="ALIEN"/>
  </properties>
 </tile>
 <tile id="22">
  <properties>
   <property name="move" value="RIGHT"/>
   <property name="type" value="RAT"/>
  </properties>
 </tile>
 <tile id="23">
  <properties>
   <property name="move" value="RIGHT"/>
   <property name="type" value="PLATE"/>
  </properties>
 </tile>
 <tile id="25">
  <properties>
   <property name="move" value="DOWN"/>
   <property name="type" value="PLATE"/>
  </properties>
 </tile>
 <tile id="28">
  <properties>
   <property name="move" value="LEFT"/>
   <property name="support" value="DOWN"/>
   <property name="type" value="SPIDER"/>
  </properties>
 </tile>
 <tile id="29">
  <properties>
   <property name="move" value="LEFT"/>
   <property name="support" value="UP"/>
   <property name="type" value="SPIDER"/>
  </properties>
 </tile>
 <tile id="30">
  <properties>
   <property name="move" value="UP"/>
   <property name="support" value="LEFT"/>
   <property name="type" value="SPIDER"/>
  </properties>
 </tile>
 <tile id="31">
  <properties>
   <property name="move" value="UP"/>
   <property name="support" value="RIGHT"/>
   <property name="type" value="SPIDER"/>
  </properties>
 </tile>
 <tile id="33">
  <properties>
   <property name="move" value="LEFT"/>
   <property name="type" value="SPINNER"/>
  </properties>
 </tile>
 <tile id="34">
  <properties>
   <property name="move" value="UP"/>
   <property name="type" value="SPINNER"/>
  </properties>
 </tile>
 <tile id="35">
  <properties>
   <property name="move" value="RIGHT"/>
   <property name="support" value="DOWN"/>
   <property name="type" value="SPIDER"/>
  </properties>
 </tile>
 <tile id="36">
  <properties>
   <property name="move" value="RIGHT"/>
   <property name="support" value="UP"/>
   <property name="type" value="SPIDER"/>
  </properties>
 </tile>
 <tile id="37">
  <properties>
   <property name="move" value="DOWN"/>
   <property name="support" value="LEFT"/>
   <property name="type" value="SPIDER"/>
  </properties>
 </tile>
 <tile id="38">
  <properties>
   <property name="move" value="DOWN"/>
   <property name="support" value="RIGHT"/>
   <property name="type" value="SPIDER"/>
  </properties>
 </tile>
 <tile id="40">
  <properties>
   <property name="move" value="RIGHT"/>
   <property name="type" value="SPINNER"/>
  </properties>
 </tile>
 <tile id="41">
  <properties>
   <property name="move" value="DOWN"/>
   <property name="type" value="SPINNER"/>
  </properties>
 </tile>
 <tile id="42">
  <properties>
   <property name="move" value="LEFT"/>
   <property name="support" value="DOWN"/>
   <property name="type" value="SPINNER_HALF"/>
  </properties>
 </tile>
 <tile id="43">
  <properties>
   <property name="move" value="LEFT"/>
   <property name="support" value="UP"/>
   <property name="type" value="SPINNER_HALF"/>
  </properties>
 </tile>
 <tile id="44">
  <properties>
   <property name="move" value="UP"/>
   <property name="support" value="LEFT"/>
   <property name="type" value="SPINNER_HALF"/>
  </properties>
 </tile>
 <tile id="45">
  <properties>
   <property name="move" value="UP"/>
   <property name="support" value="RIGHT"/>
   <property name="type" value="SPINNER_HALF"/>
  </properties>
 </tile>
 <tile id="49">
  <properties>
   <property name="move" value="RIGHT"/>
   <property name="support" value="DOWN"/>
   <property name="type" value="SPINNER_HALF"/>
  </properties>
 </tile>
 <tile id="50">
  <properties>
   <property name="move" value="RIGHT"/>
   <property name="support" value="UP"/>
   <property name="type" value="SPINNER_HALF"/>
  </properties>
 </tile>
 <tile id="51">
  <properties>
   <property name="move" value="DOWN"/>
   <property name="support" value="LEFT"/>
   <property name="type" value="SPINNER_HALF"/>
  </properties>
 </tile>
 <tile id="52">
  <properties>
   <property name="move" value="DOWN"/>
   <property name="support" value="RIGHT"/>
   <property name="type" value="SPINNER_HALF"/>
  </properties>
 </tile>
 <tile id="56">
  <properties>
   <property name="move" value="LEFT"/>
   <property name="support" value="DOWN"/>
   <property name="type" value="SLIME"/>
  </properties>
 </tile>
 <tile id="57">
  <properties>
   <property name="move" value="LEFT"/>
   <property name="support" value="UP"/>
   <property name="type" value="SLIME"/>
  </properties>
 </tile>
 <tile id="58">
  <properties>
   <property name="move" value="UP"/>
   <property name="support" value="LEFT"/>
   <property name="type" value="SLIME"/>
  </properties>
 </tile>
 <tile id="59">
  <properties>
   <property name="move" value="UP"/>
   <property name="support" value="RIGHT"/>
   <property name="type" value="SLIME"/>
  </properties>
 </tile>
 <tile id="60">
  <properties>
   <property name="move" value="LEFT"/>
   <property name="type" value="TRIPLATE"/>
  </properties>
 </tile>
 <tile id="63">
  <properties>
   <property name="move" value="RIGHT"/>
   <property name="support" value="DOWN"/>
   <property name="type" value="SLIME"/>
  </properties>
 </tile>
 <tile id="64">
  <properties>
   <property name="move" value="RIGHT"/>
   <property name="support" value="UP"/>
   <property name="type" value="SLIME"/>
  </properties>
 </tile>
 <tile id="65">
  <properties>
   <property name="move" value="DOWN"/>
   <property name="support" value="LEFT"/>
   <property name="type" value="SLIME"/>
  </properties>
 </tile>
 <tile id="66">
  <properties>
   <property name="move" value="DOWN"/>
   <property name="support" value="RIGHT"/>
   <property name="type" value="SLIME"/>
  </properties>
 </tile>
 <tile id="67">
  <properties>
   <property name="move" value="RIGHT"/>
   <property name="type" value="TRIPLATE"/>
  </properties>
 </tile>
</tileset>
