<?xml version="1.0" encoding="UTF-8"?>
<tileset name="enemySet" tilewidth="70" tileheight="70" spacing="2" margin="2">
 <image source="enemySet.png" width="512" height="512"/>
 <tile id="0">
  <properties>
   <property name="type" value="FLY"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="support" value="LEFT"/>
   <property name="type" value="SLIME"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="support" value="DOWN"/>
   <property name="type" value="SPIDER"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="support" value="UP"/>
   <property name="type" value="SPIDER"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="support" value="LEFT"/>
   <property name="type" value="SPINNER_HALF"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="support" value="LEFT"/>
   <property name="type" value="PLATE"/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="type" value="BLOCK"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="type" value="BAT"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="type" value="MOUSE"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="support" value="RIGHT"/>
   <property name="type" value="SLIME"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="support" value="LEFT"/>
   <property name="type" value="SPIDER"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="type" value="SPINNER"/>
  </properties>
 </tile>
 <tile id="12">
  <properties>
   <property name="support" value="RIGHT"/>
   <property name="type" value="SPINNER_HALF"/>
  </properties>
 </tile>
 <tile id="14">
  <properties>
   <property name="type" value="ALIEN"/>
  </properties>
 </tile>
 <tile id="15">
  <properties>
   <property name="type" value="BEE"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="support" value="DOWN"/>
   <property name="type" value="SLIME"/>
  </properties>
 </tile>
 <tile id="17">
  <properties>
   <property name="support" value="UP"/>
   <property name="type" value="SLIME"/>
  </properties>
 </tile>
 <tile id="18">
  <properties>
   <property name="support" value="RIGHT"/>
   <property name="type" value="SPIDER"/>
  </properties>
 </tile>
 <tile id="19">
  <properties>
   <property name="support" value="DOWN"/>
   <property name="type" value="SPINNER_HALF"/>
  </properties>
 </tile>
 <tile id="20">
  <properties>
   <property name="support" value="UP"/>
   <property name="type" value="SPINNER_HALF"/>
  </properties>
 </tile>
 <tile id="21">
  <properties>
   <property name="type" value="GUM"/>
  </properties>
 </tile>
 <tile id="22">
  <properties>
   <property name="type" value="BREAKABLE"/>
  </properties>
 </tile>
 <tile id="23">
  <properties>
   <property name="color" value="YELLOW"/>
   <property name="type" value="LASER"/>
  </properties>
 </tile>
 <tile id="24">
  <properties>
   <property name="color" value="RED"/>
   <property name="type" value="LASER"/>
  </properties>
 </tile>
 <tile id="25">
  <properties>
   <property name="color" value="GREEN"/>
   <property name="type" value="LASER"/>
  </properties>
 </tile>
 <tile id="26">
  <properties>
   <property name="color" value="BLUE"/>
   <property name="type" value="LASER"/>
  </properties>
 </tile>
 <tile id="28">
  <properties>
   <property name="color" value="YELLOW"/>
   <property name="type" value="LASER"/>
   <property name="laser" value="true"/>
  </properties>
 </tile>
 <tile id="29">
  <properties>
   <property name="color" value="RED"/>
   <property name="type" value="LASER"/>
   <property name="laser" value="true"/>
  </properties>
 </tile>
 <tile id="30">
  <properties>
   <property name="color" value="GREEN"/>
   <property name="type" value="LASER"/>
   <property name="laser" value="true"/>
  </properties>
 </tile>
 <tile id="31">
  <properties>
   <property name="color" value="BLUE"/>
   <property name="type" value="LASER"/>
   <property name="laser" value="true"/>
  </properties>
 </tile>
 
</tileset>
