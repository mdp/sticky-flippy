package com.solvevolve.flubber;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.flurry.android.FlurryAgent;
import com.solvevolve.actors.MyBoxActor;
import com.solvevolve.sticky.Analytics;
import com.solvevolve.sticky.Level;

public class FlurryAnalytics implements Analytics {

	@Override
	public void logSucess(Level level, float time, boolean foundStar,
			boolean beatTime) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("Level", level.getLevelIndentity());
		if(foundStar){
			FlurryAgent.logEvent("GoldStar",params);
		}
		if(beatTime){
			FlurryAgent.logEvent("SilverStar",params);
		}
		params.put("Time", MathUtils.floor(time*100)/100f+"");
		params.put("Secret", foundStar+"");
		params.put("Bet Time", beatTime+"");
		FlurryAgent.logEvent("Sucess", params);
	}

	@Override
	public void logFail(Level level, float time, MyBoxActor goodOne) {
		Vector2 pos = goodOne.body.getPosition();
		pos.x = MathUtils.ceil(pos.x*10)/10f;
		pos.y = MathUtils.ceil(pos.y*10)/10f;

		Map<String, String> params = new HashMap<String, String>();
		params.put("Level", level.getLevelIndentity());
		params.put("Time", MathUtils.floor(time*100)/100f+"");
		params.put("DueTo", goodOne.getClass().getSimpleName());
		params.put("Position", pos.toString());

		FlurryAgent.logEvent("Fail", params);
	}

}
