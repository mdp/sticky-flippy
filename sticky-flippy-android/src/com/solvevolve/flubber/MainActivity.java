package com.solvevolve.flubber;

import junit.framework.Test;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.flurry.android.FlurryAgent;
import com.solvevolve.sticky.Sticky;

public class MainActivity extends AndroidApplication {
    private View view;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
        cfg.useGL20 = false;
        AndroidCustom custom = new AndroidCustom(this);
        view = initializeForView(new Sticky(custom,new FlurryAnalytics()), cfg);
        setContentView(view);
        
    }

	
	
	@Override
	protected void onStart()
	{
		super.onStart();
		FlurryAgent.onStartSession(this, "PYYNSG3CWCPNPBVCQVQG");
	}
	 
	@Override
	protected void onStop()
	{
		super.onStop();		
		FlurryAgent.onEndSession(this);
	}
	
}