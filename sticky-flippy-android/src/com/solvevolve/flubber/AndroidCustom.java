package com.solvevolve.flubber;

import com.solvevolve.sticky.DeviceCustom;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

public class AndroidCustom implements DeviceCustom {

	
	private Activity activity;

	public AndroidCustom(Activity activity) {
		this.activity = activity;
	}
	
	@Override
	public void showReviewScreen() {
		openPackage("com.solvevolve.flubber");
	}

	@Override
	public void showFBlikeScreen() {
		String fbId = "306652019503041";
		try {
			activity.getPackageManager().getPackageInfo("com.facebook.katana",0);
			activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/" + fbId)));
		} catch (Exception e) {
			activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/pages/Rubber-Boy/306652019503041")));
		}
	}
	
	private void openPackage(String name){
		final String appPackageName = name; // getPackageName() from Context or Activity object
		try {
		    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
		} catch (android.content.ActivityNotFoundException anfe) {
		    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
		}
	}

}
