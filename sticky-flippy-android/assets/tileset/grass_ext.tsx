<?xml version="1.0" encoding="UTF-8"?>
<tileset name="grass_ext" tilewidth="70" tileheight="70">
 <image source="grass_ext.png" width="256" height="256"/>
 <tile id="0">
  <properties>
   <property name="shape" value="LEFT_RECT"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="shape" value="BOTTOM_RECT,RIGHT_RECT"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="shape" value="TOP_RECT,LEFT_RECT"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="shape" value="RIGHT_RECT"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="shape" value="BOTTOM_RECT"/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="shape" value="TOP_RECT"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="shape" value="TOP_RECT,RIGHT_RECT"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="shape" value="BOTTOM_RECT,LEFT_RECT"/>
  </properties>
 </tile>
</tileset>
