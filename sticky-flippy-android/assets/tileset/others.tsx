<?xml version="1.0" encoding="UTF-8"?>
<tileset name="others" tilewidth="70" tileheight="70">
 <image source="others.png" width="512" height="256"/>
  <tile id="0">
  <properties>
   <property name="type" value="GUM"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="type" value="BREAKABLE"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="type" value="BLOCK"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="color" value="YELLOW"/>
   <property name="type" value="LASER"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="color" value="RED"/>
   <property name="type" value="LASER"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="color" value="GREEN"/>
   <property name="type" value="LASER"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="color" value="BLUE"/>
   <property name="type" value="LASER"/>
  </properties>
 </tile>
 <tile id="14">
  <properties>
   <property name="color" value="YELLOW"/>
   <property name="type" value="LASER"/>
   <property name="laser" value="true"/>
  </properties>
 </tile>
 <tile id="15">
  <properties>
   <property name="color" value="RED"/>
   <property name="type" value="LASER"/>
   <property name="laser" value="true"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="color" value="GREEN"/>
   <property name="type" value="LASER"/>
   <property name="laser" value="true"/>
  </properties>
 </tile>
 <tile id="17">
  <properties>
   <property name="color" value="BLUE"/>
   <property name="type" value="LASER"/>
   <property name="laser" value="true"/>
  </properties>
 </tile>
</tileset>
