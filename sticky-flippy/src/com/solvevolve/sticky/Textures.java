package com.solvevolve.sticky;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class Textures {

	private TextureAtlas atlas;
	
	public TextureAtlas getAtlas(){
		if(atlas == null){
			atlas = new TextureAtlas("tileset/actors.atlas");
			Texture[] textures = (Texture[]) atlas.getTextures().toArray(new Texture[]{});
			for(int i=0; i<textures.length;i++){
//				textures[i].setFilter(TextureFilter.Linear, TextureFilter.Linear);
			}
		}
		return atlas;
	}

	public void dispose() {
		if(atlas != null){
			atlas.dispose();
			atlas = null;
		}
	}
	
}
