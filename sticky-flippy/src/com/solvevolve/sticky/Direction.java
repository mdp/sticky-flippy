package com.solvevolve.sticky;

import com.badlogic.gdx.math.Vector2;

public enum Direction {
	UP,DOWN,LEFT,RIGHT;
	public Vector2 getDirVector(){
		switch(this){
		case DOWN:
			return new Vector2(0,-1);
		case LEFT:
			return new Vector2(-1,0);
		case RIGHT:
			return new Vector2(1,0);
		case UP:
			return new Vector2(0,1);
		}
		return null;
	}

	public boolean isVertical() {
		return getDirVector().x == 0;
	}
}



