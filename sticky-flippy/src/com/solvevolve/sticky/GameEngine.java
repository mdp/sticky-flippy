package com.solvevolve.sticky;

import java.util.ArrayList;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.solvevolve.actors.ActorCell;
import com.solvevolve.actors.BackGround;
import com.solvevolve.actors.BackGround.BGType;
import com.solvevolve.actors.DetectType;
import com.solvevolve.actors.DetectType.LockType;
import com.solvevolve.actors.Dots;
import com.solvevolve.actors.Effects;
import com.solvevolve.actors.Enemy;
import com.solvevolve.actors.Gum;
import com.solvevolve.actors.MyBoxActor;
import com.solvevolve.actors.Spawner;
import com.solvevolve.actors.Spikes;
import com.solvevolve.actors.StickyJoint;
import com.solvevolve.actors.UIStar;
import com.solvevolve.actors.UIStar.StarType;
import com.solvevolve.minis.LevelCustomizer;
import com.solvevolve.screen.LevelUI;
import com.solvevolve.screen.LevelUI.PauseMode;
import com.solvevolve.screen.MyDialog;
import com.solvevolve.sticky.MyBoxStage.CamType;

public class GameEngine {

	public static Vector2 GRAVITY = new Vector2(0, -10f);
	public static Vector2 SCREEN_GRID = new Vector2(16, 10);
	private MyBoxStage stage;
	private Gum gum;
	public static float TIME_STEP = 1/60f;
	private Dots dots;

	private ArrayList<Body> deleteBodies = new ArrayList<Body>();
	private Vector2 down1;
	private Vector2 drag1;
	private Vector2 down2;
	private Vector2 drag2;
	private Vector2 dragSelect;
	private TiledMap map;
	private World world;
	private Box2DParser parser;
	private MyTileRenderer tiledMapRenderer;
	private Box2DDebugRenderer debugRenderer;
	public Map<Cell, Body> detectables;
	private Level lvl;
	private MyDialog deathDialog;
	private Spikes spikes;
	private BackGround backGround;
	private ArrayList<Spawner> spawningEnemies = new ArrayList<Spawner>();
	private float timer;
	private Vector2 camPos;
	private Sounds sounds;
	private MyDialog pauseDialog;
	private MyDialog doneDialog;
	private LevelCustomizer customizer;
	private String middleText ="";
	private Vector2 exitPos;
	private Tips tips;
	private int minStep = 120;
	private boolean done =false;
	private Effects effects;
	
	public void addSpawningEnemy(Spawner spawner){
		spawningEnemies.add(spawner);
	}
	
	public void actSpawns(float delta){
		for(int i=0; i<spawningEnemies.size();i++){
			boolean shouldSpawn = spawningEnemies.get(i).shouldSpawn(delta);
			if(shouldSpawn){
				Spawner refEnemy = spawningEnemies.get(i);
				Spawner spawnEnemy =  (Spawner) MyBoxActor.getBoxActor(refEnemy.getType());
				spawnEnemy.spawnNew(getStage(), refEnemy);
			}
		}
	}

	public GameEngine(MyBoxStage myBoxStage) {
	}
	
	
	
	
	public void update(float delta){
		int i = Math.round(delta*minStep);
		while(i>0){
			world.step(1f/(minStep), 4, 4);
			i--;
		}
		
//		pollDoubleTouch();
		actSpawns(delta);
//		timer += delta;
		if(customizer != null){
			customizer.act(delta);
		}
		removeBodies();
//		effects.act(delta);
	}

	public void setUpStage(MyBoxStage stage, int worldNo, int lvlNo) {
		this.stage = stage;
		world = new World(new Vector2(GRAVITY), true);

		lvl = Level.loadLevel(this,worldNo, lvlNo);
		lvl.initLevel();
		map = lvl.getMap();
		parser = new Box2DParser(world,this);
		tiledMapRenderer = new MyTileRenderer(map, 1/70f);
		parser.createObjs(map);
		debugRenderer = new Box2DDebugRenderer(true,true,true,true,true,true);
		world.setContactListener(new MyCollideDetector(this));
		
		
		sounds = stage.sticky.getSounds();
		tips = new Tips();
//		effects = new Effects();
//		stage.addActor(effects);
		
	}
	
	public void setGum(Gum gum){
		this.gum = gum;
		
	}
	
	public void fling(Vector2 velocity) {
		velocity = gum.adjustFlickVelocity(velocity);
		if(gum.jumps < 2){
			sounds.jump();
		}
		gum.flick(velocity);
		if(dots!=null){
			dots.remove();
		}
	}
	
	public void pause(){
		pause(PauseMode.PAUSE);
	}
	
	public void levelDone(){
		if(done) return;
		gum.levelDone();
		stage.sticky.getSounds().success();
		stage.sticky.getUserStats().setTime(lvl.worldNo, lvl.lvlNo, timer, gum.getStarFound());
		stage.sticky.getAnalytics().logSucess(lvl, getTimer(), gum.getStarFound(), lvl.getStarStatus().get(StarType.SILVER.toString()));
		LevelUI levelUI = new LevelUI(stage.getSkin());
		doneDialog = levelUI.getPauseDialog(stage,PauseMode.SUCCESS, lvl,tips.getRandomWinMsg());
		stage.showDialog(doneDialog);
		this.done = true;
	}
	
	public void deathDialogShow(MyBoxActor goodOne, Object enemy){
		
		stage.sticky.getAnalytics().logFail(lvl, getTimer(), goodOne);
		LevelUI levelUI = new LevelUI(stage.getSkin());
		deathDialog = levelUI.getPauseDialog(stage,PauseMode.DEATH, lvl,tips.getRandomDeathMsg(goodOne, enemy));
		stage.showDialog(deathDialog);
	}
	
	public void death(MyBoxActor goodOne, Object enemy){
		if(done) return;
		if(goodOne instanceof Gum){
//			deathDialogShow(goodOne, enemy);
			stage.setDeathEntities(goodOne, enemy);
			stage.setCamSpanType(CamType.SHAKE, true);
		}
		else {
			stage.setDeathEntities(goodOne, enemy);
			stage.setCamSpanType(CamType.LINEAR, true);
			
		}
		this.done = true;
	}
	
	public void flingIndicate(Vector2 velocity) {
		velocity = gum.adjustFlickVelocity(new Vector2(velocity));
		if(dots!=null){
			dots.remove();
		}
		dots = new Dots();
		stage.getGroup().addActor(dots);
		dots.init(gum.body.getPosition(), velocity, this);
	}

	public void keyDetected(ActorCell actorCell){
		if(customizer != null){
			customizer.detectedActorCell(actorCell);
		}
		DetectType type = DetectType.valueOf(actorCell.getProperties().get("detect", String.class));
		boolean removeBody = true;
		switch (type) {
		case KEY_BLUE:
			unlockKey(LockType.BLUE);
			break;
		case KEY_GREEN:
			unlockKey(LockType.GREEN);
			break;
		case KEY_RED:
			unlockKey(LockType.RED);
			break;
		case KEY_YELLOW:
			unlockKey(LockType.YELLOW);
			break;
		default:
			removeBody = false;
		}
		
		sounds.keyPicked();
		
//		if(removeBody)
//			destroyBody(actorCell.body);
	}
	

	public void detected(Gum gum, Body detectBody) {
		
		Cell detectCell = (Cell) detectBody.getUserData();
		DetectType type = null;
		try{
			type = DetectType.valueOf(detectCell.getTile().getProperties().get("detect", String.class));
		}
		catch(Exception e){
			
		}
		if(type == null) return;
		switch (type) {
		case LAVA:
			death(gum, detectCell);
			break;
		case SPIKE:
			death(gum, detectCell);
			break;
		case SIGN:
			showSign(detectCell, detectBody);
			break;
		case EXIT:
			levelDone();
			break;
		default:
			break;
		}
	}
	
	private void showSign(Cell detectCell, Body detectBody) {
		int signId = parser.getSignId(detectCell);
		if(signId  != -1){
			String message = lvl.getMessage(signId);
			getStage().showMessage(message, detectBody);
		}
	}


	

	private void unlockKey(DetectType.LockType lockType){
		TiledMapTileLayer tiledLayer = (TiledMapTileLayer) map.getLayers().get("platform");
		final int rows = (int) tiledLayer.getWidth();
		final int columns = (int) tiledLayer.getHeight();
		for(int i=0; i<rows; i++){
			for(int j=0; j<columns; j++){
				Cell cell = tiledLayer.getCell(i, j);
				if(cell != null && cell.getTile()!= null && cell.getTile().getProperties().get("lock", "", String.class).equalsIgnoreCase(lockType.toString())){
					destroyBody(detectables.get(cell));
					detectables.remove(cell);
				}
			}
		}
		
	}

	public void draw(SpriteBatch batch) {
		OrthographicCamera boxCamera = stage.getBoxCamera();
		tiledMapRenderer.setView(boxCamera);
		
		if(Gdx.input.isKeyPressed(Keys.A))
			debugRenderer.render(world, boxCamera.combined);
		else {
			tiledMapRenderer.render();
		}	
		
	}

	public void drawEffects(SpriteBatch batch){
//		batch.begin();
//		effects.draw(batch, 1);
//		batch.end();
	}
	
	public void pollDoubleTouch(){
		int[] touchIds = new int[2];
		touchIds[0] = -1;
		touchIds[1] = -1;
		int count = 0;
		for (int i = 0; i < 20; i++) { // 20 is max number of touch points
		   if (Gdx.input.isTouched(i)) {
			   touchIds[count] = i;
			   count++;
			   if(count == 2) break;
		   }
		}
		if(count >1 || (count==1 && Gdx.input.isKeyPressed(Keys.SHIFT_LEFT))){
			
			float x1 = Gdx.input.getX(touchIds[0]);
			float y1 = Gdx.input.getY(touchIds[0]);
			float x2 = count==2?Gdx.input.getX(touchIds[1]):0;
			float y2 = count==2?Gdx.input.getY(touchIds[1]):0;
			if(down1 == null ){
				down1 = stage.getGroup().screenToLocalCoordinates(new Vector2(x1, y1));
				down2 = stage.getGroup().screenToLocalCoordinates(new Vector2(x2, y2));
			}
			else {
				drag1 = stage.getGroup().screenToLocalCoordinates(new Vector2(x1, y1));
				drag1.sub(down1);
				drag2 = stage.getGroup().screenToLocalCoordinates(new Vector2(x2, y2));
				drag2.sub(down2);
				dragSelect = drag1;
				if(drag2.len2() > drag1.len2()){
					dragSelect = drag2;
				}
				dragSelect.scl(5);
				flingIndicate(dragSelect);
			}
		}
		else {
			if(down1 != null && down2 != null){
				fling(dragSelect);
				down1 = null;
				down2 = null;
			}
		}

	}


	public OrthogonalTiledMapRenderer getTiledMapRenderer() {
		return tiledMapRenderer;
	}

	public World getWorld() {
		return world;
	}
	public Gum getGum() {
		return gum;
	}

	public MyBoxStage getStage() {
		return stage;
	}
	
	public void destroyBody(Body body){
		
		deleteBodies.add(body);
	}
	
	private void removeBodies(){
		
		for(int i= deleteBodies.size()-1; i >=0; i-- ){
			Body body = deleteBodies.get(i);
			if(body == null){
				continue;
			}
			StickyJoint.notify(body,gum.getStickyJoints());
			if(body != null && body.getUserData() instanceof Cell){
				((Cell)body.getUserData()).setTile(null);
				body.getFixtureList().get(0).setSensor(true);
			}
			else {
				if(body != null){
					world.destroyBody(body);
				}
				
			}
			

			
		}
		
		
		deleteBodies = new ArrayList<Body>();
		
	}

	public void foundEnemy(Gum gum2, MyBoxActor userData) {
		if(userData instanceof Enemy)
			death(gum2,userData);
	}

	public Spikes getSpikes() {
		return this.spikes;
	}
	
	public void setSpikes(Spikes spikes){
		this.spikes = spikes;
	}

	public void setBackground(BGType type,int offset, int lvlHeight ) {
		this.backGround = new BackGround(type, offset, lvlHeight);
	}

	public BackGround getBackGround() {
		return backGround;
	}

	public Level getLevel() {
		return lvl;
	}

	public float getTimer() {
		return timer;
	}

	
	public Vector2 getCamPos() {
		return this.camPos;
	}

	public void setLevelCustomizer(LevelCustomizer levelCustomizer) {
		this.customizer = levelCustomizer;
	}

	public void setTimer(float i) {
		timer = i;
	}

	public String getMiddleText() {
		return middleText;
	}
	
	public void setMiddleText(String text) {
		middleText = text;
	}

	public Vector2 getExitPos() {
		Vector2 pos =  new Vector2(exitPos);
		return pos;
		
	}
	
	public void setExitPos(Vector2 exitPos) {
		this.exitPos = exitPos;
	}

	public void pause(PauseMode pause) {
		pauseDialog = new MyDialog("Pause", stage.getSkin());
		pauseDialog.text("Keep Going");
		
		LevelUI levelUI = new LevelUI(stage.getSkin());
		pauseDialog = levelUI.getPauseDialog(stage,pause, lvl,tips.getRandomTip());
		stage.showDialog(pauseDialog);
		
	}

	private Effects getEffects() {
		return effects;
	}
}
