package com.solvevolve.sticky;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.maps.Map;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.solvevolve.actors.ActorCell;
import com.solvevolve.actors.Bat;
import com.solvevolve.actors.Block;
import com.solvevolve.actors.Breakable;
import com.solvevolve.actors.Enemy;
import com.solvevolve.actors.Falling;
import com.solvevolve.actors.Gum;
import com.solvevolve.actors.LaserButton;
import com.solvevolve.actors.LaserButton.ColorType;
import com.solvevolve.actors.MyBoxActor;
import com.solvevolve.actors.MyBoxActor.Type;
import com.solvevolve.actors.Plate;
import com.solvevolve.minis.LevelCustomizer;
import com.sun.xml.internal.bind.v2.model.core.ID;



public class Box2DParser {
	World world;
	private GameEngine engine;
	private ArrayList<Cell> signsList;
	public static float rectWidth = 25/70f;
	public static float mushWidth = 40/70f;
	public HashMap<TiledMapTile, ArrayList<Vector2>> detectKeys = new HashMap<TiledMapTile, ArrayList<Vector2>>();
	
	
	public enum ShapeType {
		MUSHROOM, LEFT_SLIDE, RIGHT_SLIDE, RIGHT_CORNER, LEFT_CORNER, FULL,ARC_UP,ARC_DOWN, NONE, RIGHT_RECT,LEFT_RECT, BOTTOM_RECT,TOP_RECT;
	}
	public Box2DParser(World world, GameEngine engine) {
		this.world = world;
		this.engine = engine;
		engine.detectables = new HashMap<Cell, Body>();
	}

	public int getSignId(Cell cell){
		return this.signsList.indexOf(cell);
	}
	
	
	public void createObjs(TiledMap map) {
		MapLayer mapLayer = map.getLayers().get("platform");
		
		LevelCustomizer.getLevelCustomizer(map, engine);
		
		if(mapLayer instanceof TiledMapTileLayer){
			signsList = new ArrayList<TiledMapTileLayer.Cell>();
			handlePlatformLayer(mapLayer);
			
			TiledMapTileLayer tiledLayer = (TiledMapTileLayer) mapLayer;
			final int rows = (int) tiledLayer.getWidth();
			final int columns = (int) tiledLayer.getHeight();
			putBorderChain(rows, columns);
			mapLayer = map.getLayers().get("platform2");
			if(mapLayer != null)
				handlePlatformLayer(mapLayer);
			
			Set<TiledMapTile> keys = this.detectKeys.keySet();
			if(keys != null){
				Iterator<TiledMapTile> it = keys.iterator();
				while(it.hasNext()){
					TiledMapTile tile = it.next();
					ArrayList<Vector2> postions = this.detectKeys.get(tile);
					Vector2 postion = postions.get(MathUtils.random(postions.size()-1));
					ActorCell actorCell = ActorCell.getActorCell(tile.getProperties());
					actorCell.spawn(engine.getStage(), postion.x, postion.y);
				}
			}
		}	
		mapLayer = map.getLayers().get("others");
		TiledMapTileLayer platformLayer = (TiledMapTileLayer) map.getLayers().get("platform");
		TiledMapTileLayer spawnLayer = (TiledMapTileLayer) map.getLayers().get("spawn");
		if(spawnLayer.getProperties().containsKey("swarm")){
			float batSpeed = Float.valueOf(mapLayer.getProperties().get("bat", "2.5", String.class));
			this.addSwarm(spawnLayer,batSpeed);
		}
		Cell cellPlatform;
		if(mapLayer instanceof TiledMapTileLayer){
			TiledMapTileLayer tiledLayer = (TiledMapTileLayer) mapLayer;
			final int rows = (int) tiledLayer.getWidth();
			final int columns = (int) tiledLayer.getHeight();
			for(int i=0; i<rows; i++){
				for(int j=0; j<columns; j++){
					Cell cell = tiledLayer.getCell(i, j);
					if(cell != null && cell.getTile() != null){
						Enemy.Type type = Type.valueOf(cell.getTile().getProperties().get("type","NONE", String.class));
						MyBoxActor actor = MyBoxActor.getBoxActor(type);
						if(actor == null) continue;
						
						Direction moveDir = Direction.valueOf(cell.getTile().getProperties().get("move","DOWN", String.class));
						Direction supportDir = null;
						
						if(moveDir.isVertical()){
							supportDir = Direction.valueOf(cell.getTile().getProperties().get("support","LEFT", String.class));
						}
						else{
							supportDir = Direction.valueOf(cell.getTile().getProperties().get("support","DOWN", String.class));
						}
						
						
						int length = 0;
						
						if(actor instanceof Enemy || actor instanceof Plate){
							Direction peakDirection = moveDir;
							if(peakDirection == Direction.LEFT){
								peakDirection = Direction.RIGHT;
							}
							if(peakDirection == Direction.DOWN){
								peakDirection = Direction.UP;
							}
							
							Vector2 peak = peakDirection.getDirVector();
							while(true){
								Cell peakCell = tiledLayer.getCell(i+Math.round(peak.x),j+Math.round(peak.y));
								Cell peakCellNext = tiledLayer.getCell(i+2*Math.round(peak.x),j+2*Math.round(peak.y));
								if(peakCell != null  
										&& (peakCell.getTile() == cell.getTile() || (peakCellNext != null && peakCellNext.getTile()==cell.getTile() && peakCell.getTile().getProperties().get("type", "", String.class).equals("BREAKABLE") )) ){
									peak.add(peakDirection.getDirVector());
									length++;
									if(peakCell.getTile() == cell.getTile())
										peakCell.setTile(null);
								}
								else {
									break;
								}
							}
							
							if(length > 0) {
								length++;
							}
							
							if(actor instanceof Enemy){
								Enemy enemy = (Enemy) actor;
								Vector2 startPos = new Vector2(i, j);
								if(peakDirection != moveDir && length > 1){
									startPos.add(peakDirection.getDirVector().scl(length-1));
								}
								Vector2 otherEnd = peakDirection.getDirVector().scl(length-1);
								Cell endSpawn = spawnLayer.getCell(i+Math.round(otherEnd.x), j+Math.round(otherEnd.y));
								if(endSpawn != null && endSpawn.getTile() == cell.getTile()){
									length = 10000;
								}
								enemy.spawnAndSetMotion(engine.getStage(), startPos, supportDir, moveDir, length);
								float speed = Float.valueOf(mapLayer.getProperties().get(enemy.type.toString().toLowerCase(), "2.5", String.class));
								if(length == 0){
									enemy.setSpeed(0);
								}
								else {
									enemy.setSpeed(speed);
								}
								if(supportDir == Direction.UP){
									cellPlatform = platformLayer.getCell(i, j);
									if(cellPlatform != null){
										ShapeType shapeType = ShapeType.valueOf(cellPlatform.getTile().getProperties().get("shape", "FULL", String.class));
										if(shapeType == ShapeType.TOP_RECT){
											enemy.setOnTopRectUpSideDown();
										}
									}
								}
								Cell cellSpawn = spawnLayer.getCell(i, j);
								
								if(cellSpawn != null && cellSpawn.getTile() == cell.getTile()){
									enemy.setDieOnHit();
									engine.addSpawningEnemy(enemy);
									enemy.setSpawnerTimeGap(Float.valueOf(spawnLayer.getProperties().get(enemy.type.toString().toLowerCase(), "1.5", String.class)));
								}
								
							}
							else if(actor instanceof Plate){
								Plate plate = (Plate) actor;
								Vector2 startPos = new Vector2(i, j);
								if(peakDirection != moveDir && length > 1){
									startPos.add(peakDirection.getDirVector().scl(length-1));
								}
								
								Vector2 otherEnd = peakDirection.getDirVector().scl(length-1);
								Cell endSpawn = spawnLayer.getCell(i+Math.round(otherEnd.x), j+Math.round(otherEnd.y));
								if(endSpawn != null && endSpawn.getTile() == cell.getTile()){
									length = 10000;
								}
								plate.spawnAndSetMotion(engine.getStage(), startPos, supportDir,moveDir, length);
								float speed = Float.valueOf(mapLayer.getProperties().get("plate", "2.5", String.class));
								plate.setSpeed(speed);
								Cell cellSpawn = spawnLayer.getCell(i, j);
								if(cellSpawn != null && cellSpawn.getTile() == cell.getTile()){
									plate.setDieOnHit();
									engine.addSpawningEnemy(plate);
									plate.setSpawnerTimeGap(Float.valueOf(spawnLayer.getProperties().get("plate", "1.5", String.class)));
								}
							}
						}
						if(actor instanceof LaserButton){
							LaserButton laser = (LaserButton) actor;
							laser.setColorType(ColorType.valueOf((String) cell.getTile().getProperties().get("color")));
							if(cell.getTile().getProperties().containsKey("laser")){
								laser.setLaserOnPress(false);
							}
							laser.spawn(engine.getStage(), i, j);
						}
						else if(actor instanceof Block){
							Block block = (Block) actor;
							block.spawn(engine.getStage(), i, j);
						}
						else if(actor instanceof Breakable){
							Breakable breakable = (Breakable) actor;
							breakable.spawn(engine.getStage(), i, j);
						}
						else if(actor instanceof Gum){
							actor.spawn(engine.getStage(), i+0.5f, j+0.5f);
							engine.setGum((Gum)actor);
						}
						
					}	
				}
			}
		}
		
	}


	private void handlePlatformLayer(MapLayer mapLayer) {
		TiledMapTileLayer tiledLayer = (TiledMapTileLayer) mapLayer;
		final int rows = (int) tiledLayer.getWidth();
		final int columns = (int) tiledLayer.getHeight();
		for(int i=0; i<rows; i++){
			for(int j=0; j<columns; j++){
				Cell cell = tiledLayer.getCell(i, j);
				if(cell != null){
					int right=0, up =0;
					while(tiledLayer.getCell(i+right+1, j) != null && cell.getTile() == tiledLayer.getCell(i+right+1, j).getTile()){
						right++;
					}
					while(tiledLayer.getCell(i, j+up+1) != null && cell.getTile() == tiledLayer.getCell(i, j+up+1).getTile()){
						up++;
					}
					
					String detect = cell.getTile().getProperties().get("detect","",String.class);
					if(detect.equals("SIGN")){
						this.signsList.add(cell);
					}
					if(detect.equals("EXIT") && cell.getTile().getProperties().get("shape","FULL",String.class).equals("FULL")){
						engine.setExitPos(new Vector2(i, j));
					}
					
					if(detect.indexOf("KEY") != -1 || detect.indexOf("STAR") != -1){
						ArrayList<Vector2> positions = this.detectKeys.get(cell.getTile());
						if(positions == null){
							positions = new ArrayList<Vector2>();
						}
						positions.add(new Vector2(i, j));
						this.detectKeys.put(cell.getTile(), positions);
						cell.setTile(null);
					}
					else {
						if(detect.equalsIgnoreCase("LOCK")){
							createBodyAndManage(cell,i,j,0,0);
						}
						else {
							if(tiledLayer.getCell(i-1, j) !=null && tiledLayer.getCell(i-1, j).getTile() == cell.getTile() && cell.getTile().getProperties().get("shape","FULL",String.class).contains("RECT")){
								continue;
							}
							if(tiledLayer.getCell(i, j-1) !=null && tiledLayer.getCell(i, j-1).getTile() == cell.getTile() && (cell.getTile().getProperties().get("shape","FULL",String.class).equals("FULL") || cell.getTile().getProperties().get("shape","FULL",String.class).contains("RECT")) ){
								continue;
							}
							createBodyAndManage(cell,i,j,right,up);
						}							
					}
						
					
				}
			}
		}
		
		
	}

	private void addSwarm(TiledMapTileLayer spawnLayer, float batSpeed) {
		String swarm = spawnLayer.getProperties().get("swarm", "1:4:8:5", String.class);
		String[] swarmInfo = swarm.split(":");
		if(swarmInfo.length != 4) return;
		int yOff = Integer.valueOf(swarmInfo[0]);
		int yTill = Integer.valueOf(swarmInfo[1]);
		int sSize = Integer.valueOf(swarmInfo[2]);
		int sGap = Integer.valueOf(swarmInfo[3]);
		int rows = spawnLayer.getWidth();
		int columns = spawnLayer.getHeight();
		
		Bat.swarm(engine,batSpeed, yOff, yTill, sSize, sGap, rows, columns);
	}

	private void putBorderChain(int rows, int columns) {
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.StaticBody;
		bodyDef.position.set(0,0);
		ChainShape chainShape = new ChainShape();
		chainShape.createChain(new Vector2[]{
				new Vector2(rows, 0),
				new Vector2(rows, columns),
				new Vector2(0, columns),
				new Vector2(0, 0),
				new Vector2(rows, 0),
		});
		
		Body body = world.createBody(bodyDef);
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.density = 1;
		fixtureDef.friction = 1; 
		fixtureDef.restitution = 0f;
		
		fixtureDef.shape = chainShape;
		body.createFixture(fixtureDef);
		chainShape.dispose();
	}

	private void createBodyAndManage(Cell cell, int i, int j, int right, int up) {
		
		if(cell == null || cell.getTile() == null) {
			return ;
		}
		String shapeType = cell.getTile().getProperties().get("shape", "FULL", String.class);
		String shapeType2 = null;
		if(shapeType.equals("NONE")) return ;
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.StaticBody;
		if(cell != null && cell.getTile().getProperties().get("detect","",String.class).indexOf("key")!=-1 ){
			bodyDef.type = BodyType.DynamicBody;
		}
		bodyDef.position.set(i, j);
		Body body = world.createBody(bodyDef);
		
		
		int seperator = 0;
		if( (seperator = shapeType.indexOf(",")) != -1){
			shapeType2 = shapeType.substring(seperator+1);
			shapeType = shapeType.substring(0, seperator);
			right = 0;
			up =0;
		}
		FixtureDef fixtureDef =  getFixture(cell);
		PolygonShape shape = getShape(shapeType,right,up);
		fixtureDef.shape = shape;
		body.createFixture(fixtureDef);
		if(shapeType2 != null){
			shape = getShape(shapeType2,0,0);
			fixtureDef.shape = shape;
			body.createFixture(fixtureDef);
		}
		body.setUserData(cell);		
		shape.dispose();
		
		body.setAwake(false);
		if(cell.getTile().getProperties().containsKey("falling")){
			Falling falling = new Falling(cell.getTile(), i, j);
			falling.setUp(engine.getStage(), body);
			cell.setTile(null);
		}
		if(cell.getTile().getProperties().get("detect", "", String.class).equalsIgnoreCase("LOCK")){
			engine.detectables.put(cell, body);
		}	
		return;
	}



	private FixtureDef getFixture(Cell cell) {
		FixtureDef fixtureDef = new FixtureDef();
		if(cell != null && cell.getTile().getProperties().get("detect","",String.class).equals("SIGN")){
			fixtureDef.isSensor = true;
		}
		if(cell != null && cell.getTile().getProperties().containsKey("bounce")){
			fixtureDef.friction = 0;
			fixtureDef.restitution = 1f;
		}
		else {
			fixtureDef.friction = 1;
			fixtureDef.restitution = 0f;
		}
		fixtureDef.density = 1;
			
		return fixtureDef;
	}

	private PolygonShape getShape(String shapeType,int right,int up) {
		
		ShapeType types = ShapeType.valueOf(shapeType);
		float width = 1, height =1 ;
		float slideWidth = 25/70f;
		float arcHeight = 45/70f;
		float arcLength = 10/70f;
		
		PolygonShape shape = new PolygonShape();
		
		Vector2[] vertices;
		switch(types){
	
		case LEFT_CORNER:
			vertices = new Vector2[]{
					new Vector2(0, (1-slideWidth)*height),
					new Vector2(0, height),
					new Vector2(width*(slideWidth), height),
			};
			shape.set(vertices);
			break;
		case LEFT_SLIDE:
			vertices = new Vector2[]{
					new Vector2(0, 0),
					new Vector2(width, height),
					new Vector2(width, height*(1-slideWidth)),
					new Vector2(width*(slideWidth), 0),
					
			};
			shape.set(vertices);
			break;
		case RIGHT_CORNER:
			vertices = new Vector2[]{
					new Vector2(width, (1-slideWidth)*height),
					new Vector2(width, height),
					new Vector2(width*(1-slideWidth), height),
			};
			shape.set(vertices);
			break;
		case RIGHT_SLIDE:
			vertices = new Vector2[]{
					new Vector2(width, 0),
					new Vector2(0, height),
					new Vector2(0, height*(1-slideWidth)),
					new Vector2(width*(1-slideWidth), 0),
			};
			shape.set(vertices);
			break;
		case TOP_RECT:
			vertices = new Vector2[]{
					new Vector2(width*(right+1), (1-rectWidth)*height),
					new Vector2(width*(right+1), height),
					new Vector2(0, height),
					new Vector2(0, (1-rectWidth)*height),
			};
			shape.set(vertices);
			break;
		
		case LEFT_RECT:
			vertices = new Vector2[]{
					new Vector2(width*rectWidth, height*(up+1)),
					new Vector2(width*rectWidth, 0),
					new Vector2(0, 0),
					new Vector2(0, height*(up+1)),
			};
			shape.set(vertices);
			break;	
		
		case RIGHT_RECT:
			vertices = new Vector2[]{
					new Vector2(width, height*(up+1)),
					new Vector2(width, 0),
					new Vector2(width*(1-rectWidth), 0),
					new Vector2(width*(1-rectWidth), height*(up+1)),
			};
			shape.set(vertices);
			break;	
			
		
		case BOTTOM_RECT:
			vertices = new Vector2[]{
					new Vector2(width*(right+1), 0),
					new Vector2(width*(right+1), (rectWidth+0.1f)*height),
					new Vector2(0, (rectWidth+0.1f)*height),
					new Vector2(0, 0),
			};
			shape.set(vertices);
			break;
			

	
		case FULL:
			vertices = new Vector2[]{
					new Vector2(width, height*(up+1)),
					new Vector2(width, 0),
					new Vector2(0, 0),
					new Vector2(0, height*(up+1)),
			};
			shape.set(vertices);
			break;
		case MUSHROOM:
			vertices = new Vector2[]{
					new Vector2(width, 0),
					new Vector2(width, mushWidth*height),
					new Vector2(0, mushWidth*height),
					new Vector2(0, 0),
			};
			shape.set(vertices);
			break;
		case ARC_UP:
			vertices = new Vector2[]{
					new Vector2(0, arcHeight*height),
					new Vector2(width/2 - arcLength*width/2, height),
					new Vector2(width/2 + arcLength*width/2, height),
					new Vector2(width, arcHeight*height),
					new Vector2(width, 0),
					new Vector2(0, 0),
			};
			shape.set(vertices);
			break;
		case ARC_DOWN:
			vertices = new Vector2[]{
					new Vector2(0, height),
					new Vector2(width, height),
					new Vector2(width, height*(1-arcHeight)),
					new Vector2(width/2 - arcLength*width/2, 0),
					new Vector2(width/2 + arcLength*width/2, 0),
					new Vector2(0, height*(1-arcHeight)),
					
			};
			shape.set(vertices);
			break;	
		case NONE:
			return null;
		default:
			break;
		}
		

		
		return shape;
	}
}
