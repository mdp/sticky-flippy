package com.solvevolve.sticky;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;

public class TileScreen implements Screen{

	private OrthographicCamera camera;
	private OrthographicCamera boxCamera;
	private World world;
	private Box2DDebugRenderer renderer;
	
	private TiledMap map;
	private TiledMapRenderer tiledMapRenderer;
	private Box2DParser parser;
	
	
	
	
	@Override
	public void render(float delta) {
		if(Gdx.input.isKeyPressed(Keys.A)){
			renderer.render(world, camera.combined);
		}
		else {
			tiledMapRenderer.setView(camera);
			tiledMapRenderer.render();
		}
	}

	@Override
	public void resize(int width, int height) {
		width = 16;
		height = 9;
		camera.viewportWidth = width;
		camera.viewportHeight = height;
		camera.position.x = width/2f;
		camera.position.y = height/2f;
		camera.update();
		
		boxCamera.viewportWidth = 16;
		boxCamera.viewportHeight = 9;
		boxCamera.position.x = 8;
		boxCamera.position.y = 4.5f;
		boxCamera.update();
		
	}

	@Override
	public void show() {
		
		
		camera = new OrthographicCamera();
		boxCamera = new OrthographicCamera();
		world = new World(new Vector2(0, -9.8f), true);
		renderer = new Box2DDebugRenderer();
		parser = new Box2DParser(world,null);
		map = new TmxMapLoader().load("tilemaps/sample.tmx");
		tiledMapRenderer = new OrthogonalTiledMapRenderer(map,1/70f);
		parser.createObjs(map);
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		
	}

}
