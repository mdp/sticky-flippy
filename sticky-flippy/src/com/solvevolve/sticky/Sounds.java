package com.solvevolve.sticky;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.solvevolve.actors.Enemy;
import com.solvevolve.actors.MyBoxActor;
import com.solvevolve.actors.MyBoxActor.Type;
import com.solvevolve.actors.Spinner;


public class Sounds {

	private static int maxCount = 2;
	
	
	private Music loop;
	
	private UserStats userStats;
	
	public Sounds(UserStats userStats) {
		this.userStats = userStats;
		starFound = getSound("star");
	}
	
	private Sound getSound(String name){
		Sound sound;
		if(name.equals("spider"))
			sound = Gdx.audio.newSound(Gdx.files.internal("sounds/spider.ogg"));
		else
			sound = Gdx.audio.newSound(Gdx.files.internal("sounds/"+name+".ogg"));
		return sound;
	}
	
	private Sound btnPress;
	private Sound jump;
	private Sound keyPick;
	private Sound death;
	private Sound success;
	private Sound starFound;
	private HashMap<Type, Sound> actorSounds; 
	private HashMap<Type, ArrayList<Long>> actorSoundsPlaying;
	private HashMap<Type, Integer> actorSoundsCount;


	private String song;




	
	private int getActorSoundsCount(Type type){
		if(actorSoundsCount == null)
			actorSoundsCount = new HashMap<Type, Integer>();
		int count;
		if(actorSoundsCount.get(type) == null){
			actorSoundsCount.put(type, 0);
		}
		count = actorSoundsCount.get(type);
		return count;
	}
	
	private ArrayList<Long> getActorSoundPlaying(Type type){
		if(actorSoundsPlaying == null)
			actorSoundsPlaying = new HashMap<Type, ArrayList<Long>>();
		ArrayList<Long> soundsPlaying;
		if(actorSoundsPlaying.get(type) == null){
			actorSoundsPlaying.put(type, new ArrayList<Long>());
		}
		soundsPlaying = actorSoundsPlaying.get(type);
		return soundsPlaying;
	}
	
	private Sound getActorSound(Type type){
		if(actorSounds == null)
			actorSounds = new HashMap<Type, Sound>();
		Sound sound;
		if(actorSounds.get(type) == null){
			sound = actorSounds.put(type, getSound(type.toString().toLowerCase()));
		}
		sound = actorSounds.get(type);
		
		return sound;
	}
	public void actorEnter(Type type,boolean dieOnHit){
		if(noSound())
			return;
		Sound sound = getActorSound(type);
		ArrayList<Long> soundsPlaying= getActorSoundPlaying(type);
		if(dieOnHit){
			sound.play(0.5f);
			return;
		}
		int count = getActorSoundsCount(type);
		count++;
		actorSoundsCount.put(type, count);
		if(count <= maxCount){
			Long soundId = sound.loop(0.5f);
			soundsPlaying.add(soundId);
//			Gdx.app.log("adding sound", type.toString()+"");
			return ;
		}
		else {
//			Gdx.app.log("adding count", type.toString()+"");
			return ;
		}		
	}
	
	public void actorExit(Type type, boolean dieOnHit){
		if(noSound() || dieOnHit)
			return;
		Sound sound = getActorSound(type);
		ArrayList<Long> soundsPlaying= getActorSoundPlaying(type);
		int count = getActorSoundsCount(type);
		count--;
		actorSoundsCount.put(type, count);
		if(count < maxCount){
			try{
//			Gdx.app.log("stop sound", type.toString()+"");	
			sound.stop(soundsPlaying.get(0));
			soundsPlaying.remove(0);
			}
			catch(Exception e){
				Gdx.app.error("error", ""+e.getMessage());
			}
		}
		else {
//			Gdx.app.log("decrementing count", type.toString()+"");
		}
	}
	
	public void act(float delta){
		
	}
	
	
	public void stopSounds(){
		if(actorSounds != null ){
			Set<Type> types = actorSounds.keySet();
			Iterator<Type> it = types.iterator();
			Sound sound;
			while(it.hasNext()){
				sound = actorSounds.get(it.next());
				if(sound != null){
					sound.stop();
				}
			}
		}
		if(actorSoundsPlaying != null){
			Set<Type> types = actorSoundsPlaying.keySet();
			Iterator<Type> it = types.iterator();
			ArrayList<Long> sounds;
			Sound sound;
			while(it.hasNext()){
				Type enemy = it.next();
				sounds = actorSoundsPlaying.get(enemy);
				sound = actorSounds.get(enemy);
				if(sounds != null && sound != null){
					for(int i=0; i<sounds.size();i++){
						sound.stop(sounds.get(i));
					}
				}
			}
			actorSoundsPlaying = null;
		}
		actorSoundsCount = null;
	}
	private boolean noSound(){
		return !userStats.sound;
	}
	
	public void init(){
		if(starFound == null)
			starFound = getSound("star");
		if(death==null)
			death = getSound("death");
		if(keyPick == null)
			keyPick = getSound("unlock");
		if(success == null)
			success = getSound("winning");
		if(jump == null)
			jump = getSound("jump");
		
	}
	
	public void jump(){
		if(noSound())
			return;
		if(jump == null)
			jump = getSound("jump");
		jump.play(1f);
	}
	
	public void death(){
		if(noSound())
			return;
		if(death==null)
			death = getSound("death");
		stopSounds();
		stopLoop();
		death.play(0.8f);
	}
	
	public void btnPressed(){
		if(btnPress == null)
			btnPress = getSound("click1");
		btnPress.play(1f);
	}
	
	public void keyPicked(){
		if(noSound())
			return;
		if(keyPick == null)
			keyPick = getSound("unlock");
		keyPick.play(1f);
	}
	
	public void success(){
		if(noSound())
			return;
		
		if(success == null)
			success = getSound("winning");
		stopSounds();
		stopLoop();
		success.play(1f);
	}
	
	public void starFound(){
		if(noSound())
			return;
		if(starFound == null)
			starFound = getSound("star");
		starFound.play(1f);
	}
	
	public void stopLoop(){
		if(loop != null){
			loop.stop();
			loop.dispose();
			loop = null;
		}	
	}
	
	public void playLoop(Level lvl){
		stopSounds();
		if(loop!=null && loop.isPlaying() && this.song.equals("menu") && lvl==null)
			return;
		if(loop != null){
			loop.stop();
			loop.dispose();
			loop = null;
		}	
		if(lvl == null){
			this.song = "menu";
			loop = Gdx.audio.newMusic(Gdx.files.internal("music/Easy Lemon 60 second.ogg"));
			loop.setVolume(1f);
		}
		else {
			this.song = "other";
			if(lvl.lvlNo == 12 || (lvl.playChase())){
				loop = Gdx.audio.newMusic(Gdx.files.internal("music/Call to Adventure.ogg"));
			}
			else if(lvl.playSneak()){
				loop = Gdx.audio.newMusic(Gdx.files.internal("music/sneakpeek.ogg"));
			}
			else if(lvl.worldNo == 1){
				loop = Gdx.audio.newMusic(Gdx.files.internal("music/Bassa Island Game Loop.ogg"));
			}
			else if(lvl.worldNo == 2){
				loop = Gdx.audio.newMusic(Gdx.files.internal("music/Digya.ogg"));
			}
			else if(lvl.worldNo == 3){
				loop = Gdx.audio.newMusic(Gdx.files.internal("music/Monkeys Spinning Monkeys.ogg"));
			}
			loop.setVolume(0.25f);
		}
		
		if(lvl != null && lvl.worldNo > 1){
//			preLoadEnemySounds();
		}
		
		if(loop != null){
			loop.setLooping(true);
			if(userStats.music){
				loop.play();
			}
		}
		
	}
	
//	private void preLoadEnemySounds() {
////		if(noSound())
////			return;
////		Type[] types = new Type[]{Type.SPINNER,Type.BEE,Type.RAT,Type.FLY,Type.SPIDER,Type.SLIME};
////		for(int i=0; i<types.length;i++){
////			actorEnter(types[i]);
////			actorExit(types[i]);
////		}		
//	}

	public void dispose(){
		
		if(loop != null) {
			loop.stop();
			loop.dispose();
		}
		
		if(btnPress != null){
			btnPress.dispose();
		}
		if(jump != null){
			jump.dispose();
		}
		if(keyPick != null){
			keyPick.dispose();
		}
		if(death != null){
			death.dispose();
		}		
		if(success != null){
			success.dispose();
		}
		if(starFound != null){
			starFound.dispose();
		}
		if(actorSounds != null){
			Set<Type> set = actorSounds.keySet();
			Iterator<Type> it = set.iterator();
			Type type;
			while(it.hasNext()){
				type = it.next();
				actorSounds.get(type).stop();
				actorSounds.get(type).dispose();
			}
		}
		
		btnPress = null;
		jump = null;
		keyPick = null;
		death = null;
		success = null;
		starFound = null;
		actorSounds = null;
		actorSoundsCount = null;
		actorSoundsPlaying = null;
	}

	public void load(Type type) {
		getActorSound(type);
	}
	
	
	
	
	
	
	
}
