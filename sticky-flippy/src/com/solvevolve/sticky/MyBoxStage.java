package com.solvevolve.sticky;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.solvevolve.actors.Gum;
import com.solvevolve.actors.MyBoxActor;
import com.solvevolve.actors.Residue;
import com.solvevolve.screen.LevelUI.PauseMode;
import com.solvevolve.screen.MyDialog;
import com.solvevolve.screen.MySkin;
import com.solvevolve.screen.SClickListener;
import com.solvevolve.sticky.Level.CamLocation;

public class MyBoxStage extends Stage{

	private GameEngine engine;
	private MyGroup myGroup;
	private MySkin skin;
	private ArrayList<Residue> residues; 
	
	private OrthographicCamera boxCamera;
	public OrthographicCamera getBoxCamera() {
		return boxCamera;
	}

	private MyListener listener;

	private float scaleX;
	private float scaleY;
	private boolean dialogShown;
	private Label msgLabel;
	private Group hudGroup;
	private Body msgBody;
	private Label timerLabel;
	private Button pauseBtn;
	private int lvlNo;
	private int worldNo;
	public Sticky sticky;
	
	private Rectangle viewBounds;
	public FPSTester tester = new FPSTester();
	private CamType camSpanType;
	private Button rightBtn;
	private Button centerBtn;
	private Button leftBtn;
	private float viewMaxTime = 0.8f;
	private float viewTime;
	private float camLinearSpeed = 0;
	private boolean tapDone;
	private Label centerLabel;
	private Object enemy;
	private MyBoxActor goodOne;
	private Vector2 shakeAbout;
	private float shakeMaxTime = 1f;
	private float shakeTime;
	private float miniShakeMaxTime = 1/30f;
	private float miniShakeTime;
		
	
	public enum CamType{
		NORMAL, LINEAR, VIEW, SHAKE;
	}
	
	public void setTapDone(){
		tapDone = true;
		centerLabel.remove();
	}

	public Rectangle getViewBounds() {
		return viewBounds;
	}
	
	public class CamListener extends ClickListener {
		private CamLocation camLocation;
		public CamListener(CamLocation camLocation) {
			this.camLocation = camLocation;
		}
		@Override
		public void clicked(InputEvent event, float x, float y) {
			super.clicked(event, x, y);
			getEngine().getLevel().setCamLocation(camLocation);
			setCamLocUI(camLocation);
		}
	}

	public MyBoxStage(int worldNo, int lvlNo, ArrayList<Residue> residues2, boolean viewExit){
		this.skin = (((Sticky)Gdx.app.getApplicationListener()).getSkin());
		this.msgLabel = new Label("", getSkin(),"hoverMsg");
		this.worldNo = worldNo;
		this.lvlNo = lvlNo;
		this.hudGroup = new Group();
		init(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		sticky.getTextues().getAtlas();
		if(residues2 == null)
			residues = new ArrayList<Residue>();
		else {
			residues = new ArrayList<Residue>();
			for(int i=residues2.size()-1,j=0; i >=0 ;i--){
				getGroup().addActor(residues2.get(i));
				residues.add(residues2.get(i));
				j++;
				if(j> 2000)
					break;
			}
		}
		if(viewExit)
			setCamSpanType(CamType.VIEW, true);
		else {
			listener = new MyListener(this);
			addCaptureListener(listener);
			setCamSpanType(CamType.NORMAL, false);
		}
		
	}
	
	public void setCamLocUI(CamLocation camLocation) {
		centerBtn.setChecked(false);
		leftBtn.setChecked(false);
		rightBtn.setChecked(false);
		switch (camLocation) {
		case CENTER:
			centerBtn.setChecked(true);
			break;
		case LEFT:
			leftBtn.setChecked(true);
			break;
		case RIGHT:
			rightBtn.setChecked(true);
			break;
		default:
			break;
		}
	}

	private void init(int width, int height){
		sticky = (Sticky) Gdx.app.getApplicationListener();
		
		scaleX = (width/GameEngine.SCREEN_GRID.x);
		scaleY = (height/GameEngine.SCREEN_GRID.y);
		myGroup = new MyGroup(this);
		engine = new GameEngine(this);
		engine.setUpStage(this,worldNo, lvlNo);
		Camera camera = new OrthographicCamera(width, height);
		camera.translate(width/2, height/2, 0);
		camera.update();
		setCamera(camera);
		clear();
		Gdx.input.setInputProcessor(this);

		addActor(myGroup);
		addActor(hudGroup);
		addActor(msgLabel);
		boxCamera = new OrthographicCamera(GameEngine.SCREEN_GRID.x, GameEngine.SCREEN_GRID.y);
		boxCamera.translate(GameEngine.SCREEN_GRID.x/2, GameEngine.SCREEN_GRID.y/2);
		boxCamera.update();
		myGroup.setVisible(false);
		hudGroup.setSize(width, height);
		
		timerLabel = new Label("0:00", skin,"timer");
		
		
		hudGroup.addActor(timerLabel);
		pauseBtn = new Button(skin.getPauseButtonStyle());
		
		leftBtn = new Button(skin.getLeftButtonStyle());
		centerBtn = new Button(skin.getCenterButtonStyle());
		rightBtn = new Button(skin.getRightButtonStyle());
		
		hudGroup.addActor(pauseBtn);
		hudGroup.addActor(leftBtn);
		hudGroup.addActor(centerBtn);
		hudGroup.addActor(rightBtn);
		
		float size= Math.min(pauseBtn.getHeight(), height/7f),yOff = height-size,xOff=0;
		pauseBtn.setY(height-size);
		pauseBtn.setSize(size, size);
		pauseBtn.addListener(new SClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				engine.pause();
			}
		});
		
		
		size= Math.min(leftBtn.getHeight(), height/7f);
		yOff = height-size;
		leftBtn.setBounds(width/2 -50 -110 - xOff, yOff, size, size);
		leftBtn.addListener(new CamListener(CamLocation.LEFT));
		centerBtn.setBounds(width/2 -50 - xOff, yOff, size, size);
		centerBtn.addListener(new CamListener(CamLocation.CENTER));
		rightBtn.setBounds(width/2 -50 +110 - xOff, yOff, size, size);
		rightBtn.addListener(new CamListener(CamLocation.RIGHT));
		
		
		float largeFontSize = timerLabel.getTextBounds().width/4f;
		timerLabel.setX(width - largeFontSize*4f);
		timerLabel.setY(height - timerLabel.getTextBounds().height*1.8f);
		
		viewBounds = new Rectangle();
		viewTime = viewMaxTime;
		setCamLocUI(engine.getLevel().getCamLocation());
		centerLabel = new Label("Tap to Start", skin,"hoverMsg");
		centerLabel.setBounds(width/2 - largeFontSize*6, height/2-largeFontSize, largeFontSize*12, largeFontSize);
		
		Sticky sticky = (Sticky) Gdx.app.getApplicationListener();
		sticky.getSounds().init();
	}

	public void resize(int width, int height){
		sticky.getSounds().playLoop(getEngine().getLevel());
	}
	
	@Override
	public void draw() {
		SpriteBatch batch = getEngine().getTiledMapRenderer().getSpriteBatch();
		batch.begin();
		engine.getBackGround().draw(batch, 1,getViewBounds());
		batch.end();
		engine.draw(batch);
		
		if(!Gdx.input.isKeyPressed(Keys.A)){
			batch = getEngine().getTiledMapRenderer().getSpriteBatch();
			batch.begin();
			myGroup.draw(batch, 1);
			batch.end();
		}
		super.draw();
		
		engine.drawEffects(batch);
		
	}
	
	private Vector2 boxPostionToStage(Vector2 pos){
		Vector2 position = getGroup().localToStageCoordinates(pos);
		Vector3 boxCamPos = new Vector3(boxCamera.position);
		position.x -= boxCamPos.x;
		position.y -= boxCamPos.y;
		position.scl(scaleX, scaleY);
		position = stageToScreenCoordinates(position);
		position.x += Gdx.graphics.getWidth()/2;
		position.y -= Gdx.graphics.getHeight()/2;
		position = this.screenToStageCoordinates(position);
		return position;
	}
	
	@Override
	public void act(float delta) {
		tester.start("stage");
		super.act(delta);
		tester.stop("stage");
		tester.start("rest");
		OrthographicCamera camera = boxCamera;
		float width = GameEngine.SCREEN_GRID.x;//camera.viewportWidth * camera.zoom;
		float height = GameEngine.SCREEN_GRID.y;//camera.viewportHeight * camera.zoom;
		viewBounds.set(camera.position.x - width / 2, camera.position.y - height / 2, width, height);
		
		if(this.msgBody != null){
			float msgY = 2.5f;
			Vector2 pos = msgBody.getPosition();
			if(engine.getLevel().getCamLocation() == CamLocation.LEFT){
				pos.x -= GameEngine.SCREEN_GRID.x / 2-4;
			}
			if(engine.getLevel().getCamLocation() == CamLocation.RIGHT){
				pos.x += GameEngine.SCREEN_GRID.x / 2-4;
			}
			float labelWidth = msgLabel.getTextBounds().width+scaleX;//len*textScale/2
			pos = boxPostionToStage(pos.add(0.5f, msgY));
			
			msgLabel.setPosition(Math.min(Math.max(pos.x-labelWidth/2,scaleX),Gdx.graphics.getWidth()-labelWidth), Math.min(pos.y, Gdx.graphics.getHeight()*0.75f));
//			msgLabel.setWidth(scaleX*len*textScale);
			msgLabel.toBack();
		}
		
		
		timerLabel.setText(UserStats.getTimeFormat(engine.getTimer()));//+":"+Gdx.graphics.getFramesPerSecond());
		
		tester.stop("rest");
		if(dialogShown == false && camSpanType == CamType.NORMAL && tapDone == true){
			tester.start("engine");
			engine.update(delta);
			tester.stop("engine");
		}
		updateCamerasSlow(delta);
	}

	
	private void lerpCamera(Camera camera, Vector2 pos){
		final float lerp = 0.1f;
		Vector3 camPos = camera.position;
		Vector3 camOld = new Vector3(camPos);
		camPos.x += (pos.x - camPos.x)*lerp;
		camPos.y += (pos.y - camPos.y)*lerp;
		camPos.x = MathUtils.round(scaleX * camPos.x) / scaleX;
		camPos.y = MathUtils.round(scaleY * camPos.y) / scaleY;
		camera.update();
		getEngine().getBackGround().reactToMove(camPos,camOld);
		
	}
	
	public void updateCamerasSlow(float delta){
		Vector2 posInGroup;
		Vector3 camPos;
//		if(ignoreCam)
//			return;
		switch(camSpanType){
		case LINEAR:
			
			if(goodOne == null)
				posInGroup = getEngine().getLevel().getCamPosition();
			else{
				if(goodOne instanceof Gum)
					posInGroup = getEngine().getLevel().getCamPosition();
				else	
					posInGroup = getEngine().getLevel().getCamifiedPos(goodOne.body.getPosition());
			}
			
			posInGroup.x += GameEngine.SCREEN_GRID.x/2;
			posInGroup.y += GameEngine.SCREEN_GRID.y/2;
			camPos = boxCamera.position;
			if(camLinearSpeed == 0){
				setCamLinearSpeed(posInGroup,camPos);
			}
			
			Vector2 vel = new Vector2(posInGroup.x - camPos.x, posInGroup.y - camPos.y);
			float speed = camLinearSpeed;
			if(vel.len() < speed *delta){
				camPos.x = posInGroup.x;
				camPos.y = posInGroup.y;
				if(goodOne == null){
					listener = new MyListener(this);
					addCaptureListener(listener);
					setCamSpanType(CamType.NORMAL, true);
				}
				else {
					setCamSpanType(CamType.VIEW, true);
				}
				
			}
			else{
				vel.nor().scl(speed);
				camPos.x += vel.x*delta;
				camPos.y += vel.y*delta;
			}
			camPos.x = MathUtils.round(scaleX * camPos.x) / scaleX;
			camPos.y = MathUtils.round(scaleY * camPos.y) / scaleY;
			boxCamera.update();
			break;
		case NORMAL:
			posInGroup = getEngine().getLevel().getCamPosition();
			posInGroup.x += GameEngine.SCREEN_GRID.x/2;
			posInGroup.y += GameEngine.SCREEN_GRID.y/2;
			lerpCamera(boxCamera, posInGroup);
			break;
		case VIEW:
			viewTime -= delta;
			if(goodOne == null)
				posInGroup =  getEngine().getLevel().getCamPosition("EXIT");
			else{
				if(goodOne instanceof Gum)
					posInGroup = getEngine().getLevel().getCamPosition();
				else	
					posInGroup = getEngine().getLevel().getCamifiedPos(goodOne.body.getPosition());
			}
			posInGroup.x += GameEngine.SCREEN_GRID.x/2;
			posInGroup.y += GameEngine.SCREEN_GRID.y/2;
			
			camPos = boxCamera.position;
			camPos.x = (posInGroup.x );
			camPos.y = (posInGroup.y );
			camPos.x = MathUtils.round(scaleX * camPos.x) / scaleX;
			camPos.y = MathUtils.round(scaleY * camPos.y) / scaleY;
			boxCamera.update();
			if(viewTime <=0  ){
				if(goodOne == null){
					camSpanType = CamType.LINEAR;
					posInGroup = getEngine().getLevel().getCamPosition();
					setCamLinearSpeed(posInGroup,camPos);
				}
				else {
					setCamSpanType(CamType.SHAKE, true);
					this.shakeAbout = new Vector2(camPos.x, camPos.y);

				}
			}
			break;
		case SHAKE:
			shakeTime -= delta;
			camPos = boxCamera.position;
			Vector2 rand = new Vector2(0.04f,0);
			rand.setAngle(MathUtils.random(360));
			if(shakeAbout == null){
				shakeAbout = new Vector2(camPos.x, camPos.y);
			}
			miniShakeTime -= delta;
			if(miniShakeTime < 0){
				camPos.x = shakeAbout.x + rand.x;
				camPos.y = shakeAbout.y + rand.y;
				camPos.x = MathUtils.round(scaleX * camPos.x) / scaleX;
				camPos.y = MathUtils.round(scaleY * camPos.y) / scaleY;
				boxCamera.update();
				miniShakeTime = miniShakeMaxTime;
			}
			else {
			}
			if(shakeTime < 0 && dialogShown==false){
				getEngine().deathDialogShow(goodOne, enemy);
			}
		default:
			break;
		
		}
				
		
	}
	
	
	private void setCamLinearSpeed(Vector2 posInGroup, Vector3 camPos) {
		this.camLinearSpeed = new Vector2(posInGroup.x - camPos.x, posInGroup.y - camPos.y).len()/2;
		if(camLinearSpeed < 5)
			camLinearSpeed = 5;
	}

	public void setCamSpanType(CamType type, boolean pause){
		this.camSpanType = type;
		viewTime = viewMaxTime;
		shakeTime = shakeMaxTime;
		miniShakeTime = miniShakeMaxTime;
		camLinearSpeed = 0;
		if(type == CamType.NORMAL){
			if(pause)
				engine.pause(PauseMode.START);
			else if(dialogShown==false){
				addActor(centerLabel);
			}
		}
		if(type == CamType.SHAKE){
			sticky.getSounds().death();
		}
		
	}
	
	public CamType getCamSpanType(){
		return this.camSpanType;
	}
	

	public GameEngine getEngine() {
		return engine;
	}

	public void setEngine(GameEngine engine) {
		this.engine = engine;
	}
	

	public MyGroup getGroup() {
		return myGroup;
	}


	public MySkin getSkin() {
		return skin;
	}


	public void showDialog(MyDialog deathDialog) {
		if(dialogShown == true)
			return;
		deathDialog.show(this);
		deathDialog.toFront();
		dialogShown = true;
		centerLabel.remove();
	}

	public void showMessage(String msg, Body detectBody){
		this.msgLabel.setText(msg);
		float width = Gdx.graphics.getWidth();
		msgLabel.setWrap(true);
		msgLabel.setWidth(width*0.75f);
		this.msgBody = detectBody;
	}
	
	public void hideDialog(MyDialog dialog){
		dialog.hide();
		dialogShown = false;
	}


	public Vector2 getScale() {
		return new Vector2(scaleX, scaleY);
	}
	
	@Override
	public void dispose() {
		super.dispose();
		engine.getWorld().dispose();

	}

	public void addResidue(Vector2 normal, Vector2 position) {
		Residue residue = new Residue(normal, position);
		getGroup().addActor(residue);
		residues.add(residue);
	}

	public ArrayList<Residue> getResidues() {
		return residues;
	}

	public void setDeathEntities(MyBoxActor goodOne, Object enemy) {
		this.goodOne = goodOne;
		this.enemy = enemy;
	}
	
}
