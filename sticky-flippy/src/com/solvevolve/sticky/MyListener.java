package com.solvevolve.sticky;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.solvevolve.actors.Gum;

public class MyListener extends ClickListener{

	static class VelocityTracker {
		int sampleSize = 10;
		float lastX, lastY;
		float deltaX, deltaY;
		long lastTime;
		int numSamples;
		float[] meanX = new float[sampleSize];
		float[] meanY = new float[sampleSize];
		long[] meanTime = new long[sampleSize];

		public void start (float x, float y, long timeStamp) {
			lastX = x;
			lastY = y;
			deltaX = 0;
			deltaY = 0;
			numSamples = 0;
			for (int i = 0; i < sampleSize; i++) {
				meanX[i] = 0;
				meanY[i] = 0;
				meanTime[i] = 0;
			}
			lastTime = timeStamp;
		}

		public void update (float x, float y, long timeStamp) {
			long currTime = timeStamp;
			deltaX = x - lastX;
			deltaY = y - lastY;
			lastX = x;
			lastY = y;
			long deltaTime = currTime - lastTime;
			lastTime = currTime;
			int index = numSamples % sampleSize;
			meanX[index] = deltaX;
			meanY[index] = deltaY;
			meanTime[index] = deltaTime;
			numSamples++;
		}

		public float getVelocityX () {
			float meanX = getAverage(this.meanX, numSamples);
			float meanTime = getAverage(this.meanTime, numSamples) / 1000000000.0f;
			if (meanTime == 0) return 0;
			return meanX / meanTime;
		}

		public float getVelocityY () {
			float meanY = getAverage(this.meanY, numSamples);
			float meanTime = getAverage(this.meanTime, numSamples) / 1000000000.0f;
			if (meanTime == 0) return 0;
			return meanY / meanTime;
		}

		private float getAverage (float[] values, int numSamples) {
			numSamples = Math.min(sampleSize, numSamples);
			float sum = 0;
			for (int i = 0; i < numSamples; i++) {
				sum += values[i];
			}
			return sum / numSamples;
		}

		private long getAverage (long[] values, int numSamples) {
			numSamples = Math.min(sampleSize, numSamples);
			long sum = 0;
			for (int i = 0; i < numSamples; i++) {
				sum += values[i];
			}
			if (numSamples == 0) return 0;
			return sum / numSamples;
		}

		private float getSum (float[] values, int numSamples) {
			numSamples = Math.min(sampleSize, numSamples);
			float sum = 0;
			for (int i = 0; i < numSamples; i++) {
				sum += values[i];
			}
			if (numSamples == 0) return 0;
			return sum;
		}
	}
	
	private MyBoxStage stage;
	private Vector2 down;
	private Vector2 drag;
	private VelocityTracker tracker;
	private boolean doubleTouch;
	private Vector2 scale;

	public MyListener(MyBoxStage myBoxStage) {
		this.stage = myBoxStage;
		tracker = new VelocityTracker();
		scale = stage.getScale();
	}
	
	@Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer,
			int button) {
		if(super.touchDown(event, x, y, pointer, button)){
			doubleTouch = false;
			stage.setTapDone();
			isSingleTouch();
			down = new Vector2(x/scale.x, y/scale.y);
			tracker.start(down.x, down.y, Gdx.input.getCurrentEventTime());
			return true;
		}
		return false;
	}
	
	@Override
	public void touchDragged(InputEvent event, float x, float y, int pointer) {
		super.touchDragged(event, x, y, pointer);
		isSingleTouch();
		drag = new Vector2(x/scale.x, y/scale.y);
		tracker.update(drag.x, drag.y, Gdx.input.getCurrentEventTime());
	}
	
	@Override
	public void touchUp(InputEvent event, float x, float y, int pointer,
			int button) {
		super.touchUp(event, x, y, pointer, button);
		isSingleTouch();
		drag = new Vector2(x/scale.x, y/scale.y);
		tracker.update(drag.x, drag.y, Gdx.input.getCurrentEventTime());
		drag.sub(down);
		if(tracker.numSamples >= 4 && drag.len() > 0.2f){
			Vector2 flingVelocity = new Vector2(tracker.getVelocityX(), tracker.getVelocityY());
			flingVelocity.clamp(Gum.MIN_SPEED, Math.min(Gum.MAX_FLING_SPEED, Gum.MAX_LOW_FLING_SPEED*(drag.len()/Gum.LOW_FLING_DISTANCE)));
			stage.getEngine().fling(flingVelocity);
		}
		else {
			stage.getEngine().getGum().stop();
		}
		
	}
	
	public void isSingleTouch(){
		int count = 0;
		for (int i = 0; i < 20; i++) { // 20 is max number of touch points
			   if (Gdx.input.isTouched(i)) {
				   count++;
			   }
		}
		if(count > 1 || (count==1 && Gdx.input.isKeyPressed(Keys.SHIFT_LEFT))){
			this.doubleTouch = true;
		}
	}
	
}
