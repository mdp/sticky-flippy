package com.solvevolve.sticky;

import com.solvevolve.actors.MyBoxActor;

public interface Analytics {

	public void logSucess(Level level, float time, boolean foundStar, boolean beatTime);
	public void logFail(Level level, float time,MyBoxActor goodOne);
}
