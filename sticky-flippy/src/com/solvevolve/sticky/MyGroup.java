package com.solvevolve.sticky;

import com.badlogic.gdx.scenes.scene2d.Group;

public class MyGroup extends Group{
	private MyBoxStage stage;

	public MyGroup(MyBoxStage stage) {
		this.stage = stage;
	}
	
	public MyBoxStage getStage(){
		return this.stage;
	}
	
	@Override
	public void act(float delta) {
		stage.tester.start("mygroup");
		super.act(delta);
		stage.tester.stop("mygroup");
	}
}
