package com.solvevolve.sticky;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.solvevolve.actors.CollideNotify;
import com.solvevolve.actors.Enemy;
import com.solvevolve.actors.Gum;
import com.solvevolve.actors.Plate;

public class MyCollideDetector implements ContactListener {

	private GameEngine engine;

	public MyCollideDetector(GameEngine gameEngine) {
		this.engine = gameEngine;
	}

	@Override
	public void beginContact(Contact contact) {
		Gum gum = null;
		Body detectBody = null;
		Fixture radialFixture = null;
		boolean avoidGum = false;
		
		
		if( contact.getFixtureA().getBody().getUserData() instanceof CollideNotify){
			CollideNotify notify = (CollideNotify) contact.getFixtureA().getBody().getUserData();
			avoidGum = notify.collideWith(contact.getFixtureB().getBody(),contact.getFixtureA());
			
		}
		if( contact.getFixtureB().getBody().getUserData() instanceof CollideNotify){
			CollideNotify notify = (CollideNotify) contact.getFixtureB().getBody().getUserData();
			avoidGum = notify.collideWith(contact.getFixtureA().getBody(),contact.getFixtureB());
			
		}
		
		if(!avoidGum){
			if( contact.getFixtureA().getBody().getUserData() instanceof Gum){
				gum = (Gum)  contact.getFixtureA().getBody().getUserData();
				detectBody = contact.getFixtureB().getBody();
				radialFixture = contact.getFixtureA();
			}
			else if( contact.getFixtureB().getBody().getUserData() instanceof Gum){
				gum = (Gum)  contact.getFixtureB().getBody().getUserData();
				detectBody = contact.getFixtureA().getBody();
				radialFixture = contact.getFixtureB();
				
			}
			if(gum != null && radialFixture.getBody() != gum.body){
				gum.stick(radialFixture,detectBody,contact);
			}

		}
		
		
	}

	@Override
	public void endContact(Contact contact) {
		if(contact.getFixtureB() == null || contact.getFixtureA() == null){
			return;
		}
		if( contact.getFixtureA().getBody().getUserData() instanceof CollideNotify ){
			CollideNotify notify = (CollideNotify) contact.getFixtureA().getBody().getUserData();
			notify.collideEnd(contact.getFixtureB().getBody(),contact.getFixtureA());
			
		}
		if( contact.getFixtureB().getBody().getUserData() instanceof CollideNotify ){
			CollideNotify notify = (CollideNotify) contact.getFixtureB().getBody().getUserData();
			notify.collideEnd(contact.getFixtureA().getBody(),contact.getFixtureB());
			
		}
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {

	}

}
