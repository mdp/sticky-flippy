package com.solvevolve.sticky;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Json;
import com.solvevolve.actors.UIStar;
import com.sun.awt.SecurityWarning;


public class UserStats {

	private float[][] worldTimes = new float[3][12];
	private boolean[][] starsFound = new boolean[3][12];
	private static float[][] worldMinTimes = new float[3][12];
	public boolean music;
	public boolean sound;
	private boolean firstTime;
	
	
	private void setMinTimes(){
		worldMinTimes[0]= new float[]{
				30f,25f,35f,	40f,65f,110f,
				65f,50f,35f,	70f,110f,60f
		};
		worldMinTimes[1]= new float[]{
				25f,70f,100f,	60f,100f,115f,
				180f,95f,140f,	85f,70f,130f
		};
		worldMinTimes[2]= new float[]{
				22f,125f,90f,	75f,150f,160f,
				200f,50f,125f,	175f,250f,140f
		};
		
	}
	
	public UserStats() {
		firstTime = true;
		setMinTimes();
	}
	
	public void setTime(int worldNo, int lvlNo, float time, boolean starFound){
		time = ((int)Math.ceil(time*100))/100f;
		boolean save = false;
		if(starFound){
			starsFound[worldNo-1][lvlNo-1] = starFound;
			save = true;
		}
		
		float prevTimeTaken = worldTimes[worldNo-1][lvlNo-1];
		if(prevTimeTaken == 0 || time < prevTimeTaken){
			worldTimes[worldNo-1][lvlNo-1] = time;
			save = true;
		}
		if(save)
			save();
		
	}
	
	
	public static UserStats load(){
		Preferences preferences = Gdx.app.getPreferences("stickyCandy");
		String str = preferences.getString("json", "{}");
		Json json = new Json();
		UserStats userStats = json.fromJson(UserStats.class, str);
		if(userStats.firstTime){
			userStats.music = true;
			userStats.sound = true;
		}
		return userStats;
	}
	
	public void save(){
		Preferences preferences = Gdx.app.getPreferences("stickyCandy");
		Json json = new Json();
		firstTime = false;
		preferences.putString("json",json.toJson(this));
		preferences.flush();
	}

	public float getTime(int worldNo, int lvlNo) {
		return worldTimes[worldNo-1][lvlNo-1];
	}
	
	public HashMap<String,Boolean> getStarStatus(int worldNo, int lvlNo){

		HashMap<String, Boolean> stars = new HashMap<String, Boolean>();
		
		stars.put(UIStar.StarType.BRONZE.toString(), false);
		stars.put(UIStar.StarType.SILVER.toString(), false);
		stars.put(UIStar.StarType.GOLD.toString(), false);
		float time = getTime(worldNo, lvlNo);
		if(time > 0){
			stars.put(UIStar.StarType.BRONZE.toString(), true);
		}
		if(time > 0 && time-1 <= worldMinTimes[worldNo-1][lvlNo-1]){
			stars.put(UIStar.StarType.SILVER.toString(), true);
		}
		if(starsFound[worldNo-1][lvlNo-1]){
			stars.put(UIStar.StarType.GOLD.toString(), true);
		}
		return stars;
	}

	public String getSilverStarTime(int worldNo, int lvlNo) {
		return getTimeFormat(this.worldMinTimes[worldNo-1][lvlNo-1]);
	}
	
	public static String getTimeFormat(float time){
		int sec = MathUtils.floor(time%60);
		String secString = ""+sec;
		if(sec < 10){
			secString = "0"+sec;
		}
		return ""+MathUtils.floor(time/60)+":"+secString;
	}
}
