package com.solvevolve.sticky;

import java.util.ArrayList;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.MathUtils;
import com.solvevolve.actors.Enemy;
import com.solvevolve.actors.Gum;
import com.solvevolve.actors.LaserButton;
import com.solvevolve.actors.MyBoxActor;

public class Tips {

	public ArrayList<String> tipMessages = new ArrayList<String>();

	public Tips() {
		initTips();
	}

	private void initTips() {
		tipMessages.add("Flick your finger in a direction to jump.");
		tipMessages
				.add("Rubber Boy, the flying rubber is prone to sticking to surfaces.");
		tipMessages.add("Tap to loose all speed and come to stop.");
		tipMessages.add("Tap to get Rubber Boy unstuck and free.");
		tipMessages.add("Flick in mid air to perform a double jump");
		tipMessages
				.add("Rubber Boy can only do a double jump, so make the second jump count");
		tipMessages
				.add("Tap to stop helps you land a jump in the exact position wanted");
		tipMessages.add("Try using different thumbs for tap-to-stop and flick");
		tipMessages
				.add("Practise double jump and tap-to-stop for precision control.");

		tipMessages.add("Completing a level earns a bronze star");
		tipMessages
				.add("Completing a level with in timelimit earns a silver star");
		tipMessages
				.add("Find the golden star and complete the level to earn a golden star");

		tipMessages
				.add("Switch camera to get a proper glimpse of the path ahead.");
		tipMessages.add("Switching camera reveals many secrets");

		tipMessages.add("Try out other levels, if you are stuck.");

		tipMessages
				.add("Please review the game to support new worlds and levels");

	}

	public String getRandomTip() {
		return tipMessages.get(MathUtils.random(tipMessages.size() - 1));
//		return "This is such a long string that i don't even know what to type here, put let me try it out to see line wrap, wtf will happen if i make this even longer is to be seen";
	}

	
	public String getRandomWinMsg() {
		ArrayList<String> win = new ArrayList<String>();

		win.add("Through the doors of success");
		win.add("You win, you win, you win");
		win.add("Aah the smell of Victory");
		win.add("You did it.");
		win.add("Finish a level to earn the bronze star");
		win.add("Complete a level within the time limit for a silver star");
		win.add("Get the hidden Golden Star in each level");
		win.add("Support further updates by reviewing the game");
		win.add("Like on Facebook, to access level editor");
		return win
				.get(MathUtils.random(win.size() - 1));

	}

	

	public String getRandomDeathMsg(MyBoxActor goodOne, Object enemy) {
		ArrayList<String> death = new ArrayList<String>();
		if (goodOne instanceof Gum) {
			if(enemy instanceof Enemy){
				switch (((Enemy)enemy).type) {
				case BAT:
					death.add("\"I too am afraid of bats\"-Batman");
					death.add("Not many die due to bats, you are an exception");
					break;
				case BEE:
					death.add("\"No honey for you\"-Queen Bee");
					death.add("Bee stings can become quite deadly");
					break;
				case FLY:
					death.add("\"I ain't an ordinary house fly\"-HouseFly");
					death.add("Were you hoping to fly using a housefly?");
					break;
				case LASER:
					death.add("FYI:Lasers can be quite lethal");
					death.add("\"I can see right through you\"-Laser");
					break;
				case RAT:
					death.add("\"You sure do taste better than cheese\"-Rat");
					death.add("And Rubber Boy volunteers for a mouse trap");
					break;
				case SLIME:
					death.add("\"qaqIHneS >sup, qaqIHmo' jIQuch \"-Slime");
					death.add("Slimes not bright, but effective killers");
					break;
				case SPIDER:
					death.add("I am going to savour you for a long time\"-Spider");
					death.add("Lookout here comes the 'Spiderman minus man'");
					break;
				case SPINNER:

				case SPINNER_HALF:
					death.add("Rotating sharp blades vs Rubber Boy, guess who wins");
					death.add("YOLO but throwing oneself on sharp blabes?");
					death.add("Good job at getting cut into tiny bits");
					break;
				default:
					break;

				}
			}
			else {
				if(enemy instanceof LaserButton){
					death.add("FYI:Lasers can be quite lethal");
					death.add("\"I can see right through you\"-Laser");
				}
				else {
					death.add("Bathing in lava is deadly");
					death.add("Hot molten lava doesn't make for a good swimming pool");
				}	
			}
			
		}
		else{
			death.add("You shouldn't destroy the KEYS to success");
			death.add("You will never open the gates without the KEYS");
			death.add("KEYS are meant to open doors, not toys for you to torture");
			return death
					.get(MathUtils.random(death.size() - 1));
		}
		
		if(MathUtils.randomBoolean()){
			return death
					.get(MathUtils.random(death.size() - 1));
		}
		
		death.add("\"There is no shame in quitting\"- You-don't-Know-Who");
		death.add("\"Death makes us all a bit more clever\"- Evolution");
		death.add("\"That which does kill us makes us stronger\"- Anti Nietzsche");
		death.add("Try out other levels if you are stuck");
		death.add("Practice till you learn the tricks");
		death.add("Try using one thumb to jump and one thumb to tap&stop");
		death.add("Remember you can flick anywhere, so avoid blocking view with fingers");
		death.add("Make extensive use of the double jump, so land correctly");
		death.add("Tap&Stop to control the jump");
		

		return death
				.get(MathUtils.random(death.size() - 1));

	}
}
