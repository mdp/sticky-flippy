package com.solvevolve.sticky;

import java.util.HashMap;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.TmxMapLoader.Parameters;
import com.badlogic.gdx.math.Vector2;
import com.solvevolve.actors.BackGround.BGType;
import com.solvevolve.actors.Spikes;
import com.solvevolve.sticky.Level.CamLocation;

public class Level {

	public String name;
	public int worldNo;
	public int lvlNo;
	private TiledMap map;
	private GameEngine engine;
	private float spikeSpeed;
	public enum CAM_TYPE{
		GUM,SPIKEX,SPIKEY,STATIC;
	}
	private CAM_TYPE currType;
	private BGType bgType;
	private String[] messages;
	private Integer bgOffset; 
	private Vector2 mapSize;
	public Vector2 getMapSize() {
		return mapSize;
	}

	private Vector2 camCover;
	private int lvlHeight;
	private CamLocation camLocation;
	private String displayName;
	
	public enum CamLocation{
		LEFT, RIGHT, CENTER;
	}
	
	public static Level loadLevel(GameEngine engine, int world, int lvlNo ){
		Level lvl = new Level();
		lvl.name = "wld-"+world+"/"+getLevelName(world, lvlNo);
		lvl.engine = engine;
		lvl.worldNo = world;
		lvl.lvlNo = lvlNo;
		lvl.setDisplayName(lvl.getLevelDisplayName("tilemaps/wld-"+world+"/names.txt"));
		TmxMapLoader loader = new TmxMapLoader();
		
		TmxMapLoader.Parameters parameters = new Parameters();
		parameters.textureMagFilter = TextureFilter.Linear;
		parameters.textureMinFilter = TextureFilter.Nearest;
		lvl.setMap(loader.load("tilemaps/"+lvl.name, parameters));
		String camType = lvl.map.getProperties().get("cam", "GUM", String.class);
		String backGround = lvl.map.getProperties().get("bg", "GRASSLAND", String.class);
		lvl.currType = CAM_TYPE.valueOf(camType);
		lvl.bgType = BGType.valueOf(backGround);
		if(lvl.worldNo == 1){
			lvl.bgType = BGType.GRASSLAND;
		}
		if(lvl.worldNo == 2){
			lvl.bgType = BGType.MUSHROOM;
		}
		if(lvl.worldNo == 3){
			lvl.bgType = BGType.DESERT;
		}
		lvl.bgOffset = Integer.valueOf(lvl.map.getProperties().get("bgOff", "0",String.class));
		lvl.spikeSpeed = Float.valueOf(lvl.map.getProperties().get("speed", "0.5",String.class));
		lvl.camLocation = CamLocation.CENTER;
		if(lvl.lvlNo == 12 && lvl.worldNo==3){
			lvl.camLocation = CamLocation.RIGHT;
		}
		FileHandle messageHandle = Gdx.files.internal("tilemaps/"+lvl.name.replaceAll("tmx", "txt"));
		if(messageHandle != null && messageHandle.exists()) {
			String msgs = messageHandle.readString();
			lvl.messages = msgs.split("\n");  
		}
		
		TiledMapTileLayer tiledLayer = (TiledMapTileLayer) lvl.map.getLayers().get("platform");
		
		lvl.mapSize = new Vector2(tiledLayer.getWidth(), tiledLayer.getHeight()); 
		lvl.camCover = GameEngine.SCREEN_GRID;
		lvl.lvlHeight = (int) lvl.mapSize.y;
		return lvl;
	}
	
	public void initLevel(){
		if(currType == CAM_TYPE.SPIKEX){
			Spikes spikes = new Spikes();
			spikes.spawn(engine.getStage(), Direction.RIGHT);
			spikes.setSpeed(spikeSpeed);
			engine.setSpikes(spikes);
		}
		if(currType == CAM_TYPE.SPIKEY){
			Spikes spikes = new Spikes();
			spikes.spawn(engine.getStage(), Direction.UP);
			spikes.setSpeed(spikeSpeed);
			engine.setSpikes(spikes);
		}
		engine.setBackground(bgType, bgOffset, lvlHeight);

	}

	public TiledMap getMap() {
		return map;
	}

	public void setMap(TiledMap map) {
		this.map = map;
	}

	public String getName() {
		return this.name;
	}

	public Vector2 getCamPosition(String object) {
		Vector2 pos = new Vector2();
		if(object.equalsIgnoreCase("GUM")){
			pos = engine.getGum().getCameraPos();
			if(camLocation == CamLocation.LEFT){
				pos.x -= GameEngine.SCREEN_GRID.x / 2-4;
			}
			if(camLocation == CamLocation.RIGHT){
				pos.x += GameEngine.SCREEN_GRID.x / 2-4;
			}
		}	
		if(object.equalsIgnoreCase("EXIT"))
			pos = engine.getExitPos();
		
		
		
		return getCamifiedPos(pos);
		
		
	}
	
	public Vector2 getCamifiedPos(Vector2 pos){
		pos.x = pos.x - (1)*(GameEngine.SCREEN_GRID.x / 2);
		pos.y = pos.y - (1)* (GameEngine.SCREEN_GRID.y / 2);
		
		if(pos.x < 0){
			pos.x =0;
		}
		if(pos.x > mapSize.x - camCover.x  ){
			pos.x = mapSize.x -camCover.x;
		}
		pos.y -=1;
		if(pos.y < 0){
			pos.y =0;
		}
		else if(pos.y > mapSize.y - camCover.y){
			pos.y = mapSize.y -camCover.y;
		}
		return pos;
	}

	public String getMessage(int signId) {
		if(this.messages != null && signId < this.messages.length){
			return this.messages[signId];
		}
		return null;
	}

	public Vector2 getCamPosition() {
		return getCamPosition("GUM");
	}

	public CamLocation getCamLocation() {
		return camLocation;
	}

	public String getSilverStarTime() {
		Sticky sticky = (Sticky) Gdx.app.getApplicationListener();
		UserStats stats = sticky.getUserStats();
		return stats.getSilverStarTime(worldNo, lvlNo)+"";
	}
	
	public HashMap<String,Boolean> getStarStatus(){
		Sticky sticky = (Sticky) Gdx.app.getApplicationListener();
		UserStats stats = sticky.getUserStats();
		return stats.getStarStatus(worldNo, lvlNo);
	}
	
	public static String getLevelName(int worldNo, int lvlNo){
		FileHandle maps = getMapsHandle(worldNo);
		return getLevels(maps)[lvlNo-1].name();
		
	}

	public static FileHandle getMapsHandle(int worldNo) {
		FileHandle maps;
		if (Gdx.app.getType() == ApplicationType.Android) {
			maps = Gdx.files.internal("tilemaps/"+"wld-"+worldNo);
		} else {
			String fileDir = "./bin/tilemaps/"+"wld-"+worldNo;
			maps = Gdx.files.local(fileDir);
			if(maps == null || !maps.isDirectory() || maps.list().length==0 ){
				fileDir = "tilemaps/"+"wld-"+worldNo;
				maps = Gdx.files.internal(fileDir);
			}
		}
		return maps;
	}

	public static FileHandle[] getLevels(FileHandle maps) {
		FileHandle fileList = maps.child("lvls.txt");
		String files[] = fileList.readString().split("\\n");
		FileHandle[] fh = new FileHandle[12];
		int i=0;
		for (String filename: files) {
		   fh[i]= maps.child(filename.trim());
		   i++;
		}
		return fh;
	}

	public String getLevelDisplayName(String namesFile) {
		FileHandle fileList = Gdx.files.internal(namesFile);
		String names[] = fileList.readString().split("\\n");
		return names[lvlNo-1].trim();
	}
	
	public void setCamLocation(CamLocation camLocation) {
		this.camLocation = camLocation;
	}

	public static String[] getWorldNames() {
		String[] names = new String[3];
		names[0] = "Candy Town";
		names[1] = "Wild Forest";
		names[2] = "Bouncy Village";
		return names;
	}
	
	public String getLevelIndentity(){
		return worldNo+"-"+lvlNo;
	}

	public boolean playChase() {
		if(lvlNo == 9 && worldNo == 3)
			return true;
		if(lvlNo == 9 && worldNo == 2)
			return true;
		
		return false;
	}

	public boolean playSneak() {
		if(lvlNo == 6 && worldNo == 1)
			return true;
		if(lvlNo == 7 && worldNo == 2)
			return true;
		if(lvlNo == 2 && worldNo == 3)
			return true;
		
		return false;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
}
