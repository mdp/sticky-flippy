package com.solvevolve.sticky;

import java.awt.Menu;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.solvevolve.screen.MenuScreen;
import com.solvevolve.screen.MySkin;
import com.solvevolve.screen.SplashScreen;
import com.solvevolve.screen.WorldScreen;

public class Sticky extends Game{

	private Textures textues;
	private MySkin skin;
	private UserStats userStats;
	private Sounds sounds;
	private DeviceCustom custom;
	private Analytics analytics;
	
	
	public Analytics getAnalytics() {
		return analytics;
	}

	public Sticky(DeviceCustom custom, Analytics analytics) {
		this.analytics = analytics;
		this.custom = custom;
	}
	
	@Override
	public void create() {
		Gdx.input.setCatchBackKey(true);
		setScreen(new SplashScreen());
		
//		setScreen(new LevelScreen());
//		setScreen(new LevelsScreen("tut"));
	}

	@Override
	public void render() {
		float ambient = 0.f;
		Gdx.gl.glClearColor(ambient,ambient,ambient,1f);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		super.render();
	}
	
	public Textures getTextues() {
		if(textues == null){
			textues = new Textures();
		}
		return textues;
	}
	
	public UserStats getUserStats(){
		if(userStats == null){
			userStats = UserStats.load();
		}
		return userStats;
	}

	public MySkin getSkin(){
		if(skin == null){
			skin = new MySkin(); 
		}
		return skin;
	}
	
	public Sounds getSounds() {
		if(sounds == null){
			sounds = new Sounds(getUserStats());
		}
		return sounds;
	}
	
	public void setTextues(Textures textues) {
		this.textues = textues;
	}
	
	@Override
	public void setScreen(Screen screen) {
		Screen oldScreen = getScreen();
		if(oldScreen != null)
			oldScreen.dispose();
		super.setScreen(screen);
				
	}
	
	public void dispose(){
		disposeSingletons();
		getScreen().dispose();
	}
	
	public void reload() {
		getSkin();
		getTextues();
		getSounds();
	}
	

	public DeviceCustom getCustom() {
		return custom;
	}

	public void setCustom(DeviceCustom custom) {
		this.custom = custom;
	}

	public void disposeSingletons() {
		Gdx.app.log("disposing resources","");
		if(textues != null){
			textues.getAtlas().dispose();
			textues = null;
		}
		
		if(skin != null){
			skin.dispose();
			skin = null;
		}
		
		if(sounds != null){
			sounds.dispose();
			sounds = null;
		}
	}
	
	
	
}
