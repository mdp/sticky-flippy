package com.solvevolve.sticky;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.omg.PortableInterceptor.ACTIVE;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.maps.Map;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.TimeUtils;
import com.esotericsoftware.tablelayout.BaseTableLayout.Debug;

public class FPSTester {

	float gapTime = 0;
	private long start;
	private long stop;
	HashMap<String, Long> counters = new HashMap<String, Long>();
	HashMap<String, Long> startTimes = new HashMap<String, Long>();
	private boolean active = false;
	public void start(){
		if(active)
			start = TimeUtils.nanoTime();
	}
	
	public void start(String str){
		if(active)
			startTimes.put(str, TimeUtils.nanoTime());
	}
	
	public void stop(String str){
		if(active){
			Long countTime = startTimes.get(str);
			Long already = counters.get(str);
			if(already == null){
				already = (long) 0;
			}
			already = (long) 0;
			counters.put(str, already+TimeUtils.nanoTime()-countTime);
		}
		
	}
	
	
	public void stop(MyBoxStage stage){
		if(active){
			stop = TimeUtils.nanoTime();
			gapTime = (stop - start);
			if(gapTime > (1/20f)*1000000000.0){
				Set<String> diff = counters.keySet();
				Iterator<String> it = diff.iterator();
				boolean debug = true;
				
				if(debug){
//					Gdx.app.log("GapTime:", 1f/(gapTime/1000000000.0)+"");
					it = diff.iterator();
					while(it.hasNext()){
						String typ = it.next();
//						Gdx.app.log(typ+":", 100*counters.get(typ)/gapTime+"");
					}
					int bodies = stage.getEngine().getWorld().getBodyCount();
//					Gdx.app.log("engine:", bodies+"");
				}
				
			}
			gapTime = 0;
			counters = new HashMap<String, Long>();
			startTimes = new HashMap<String, Long>();
		}
		
	}
}
