package com.solvevolve.minis;

import java.util.Properties;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.solvevolve.actors.ActorCell;
import com.solvevolve.actors.Coin;
import com.solvevolve.sticky.GameEngine;

public class CoinsGame extends LevelCustomizer{

	public final float maxGap = 0.5f;
	private float gap=0;
	private Vector2 mapSize;
	private int score = 0;
	
	public CoinsGame(GameEngine engine) {
		super(engine);
		mapSize = engine.getLevel().getMapSize();
		engine.setTimer(60);
	}

	@Override
	public void detectedActorCell(ActorCell actorCell) {
		if(actorCell instanceof Coin){
			Coin coin = (Coin) actorCell;
			score += coin.getValue();
		}
	}

	@Override
	public void act(float delta) {
		gap += delta;
		if(gap > maxGap){
			MapProperties properties = new MapProperties();
			properties.put("detect", "COIN_GOLD");
			ActorCell cell = new Coin(properties);
			cell.spawn(engine.getStage(), MathUtils.random(1, mapSize.x-1), MathUtils.random(mapSize.y-3,mapSize.y-1));
			gap = 0;
		}
		engine.setTimer(engine.getTimer()-delta);
		if(engine.getTimer() < 0){
			engine.levelDone();
		}
		engine.setMiddleText(score+"");
	}

}
