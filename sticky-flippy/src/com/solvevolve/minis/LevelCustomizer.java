package com.solvevolve.minis;

import com.badlogic.gdx.maps.Map;
import com.solvevolve.actors.ActorCell;
import com.solvevolve.sticky.GameEngine;


public abstract class LevelCustomizer {

	protected GameEngine engine;

	public LevelCustomizer(GameEngine engine) {
		this.engine = engine;
		engine.setLevelCustomizer(this);		
	}

	public abstract void detectedActorCell(ActorCell actorCell);

	public abstract void act(float delta);
	
	
	public static LevelCustomizer getLevelCustomizer(Map map, GameEngine engine){
		String custom = map.getProperties().get("custom", "", String.class);
		if(custom.equals("CoinsGame")){
			return new CoinsGame(engine);
		}
		
		return new GenericGame(engine);
	}
}
