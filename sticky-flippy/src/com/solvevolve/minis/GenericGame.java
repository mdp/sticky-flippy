package com.solvevolve.minis;

import java.util.Properties;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.solvevolve.actors.ActorCell;
import com.solvevolve.actors.Coin;
import com.solvevolve.actors.Enemy;
import com.solvevolve.sticky.GameEngine;

public class GenericGame extends LevelCustomizer{

	public final float maxGap = 0.5f;
	private float gap=0;
	private Vector2 mapSize;
	
	public GenericGame(GameEngine engine) {
		super(engine);
		mapSize = engine.getLevel().getMapSize();
		engine.setTimer(0);
	}

	@Override
	public void detectedActorCell(ActorCell actorCell) {
		
	}

	@Override
	public void act(float delta) {
		engine.setTimer(engine.getTimer()+delta);
	}

}
