package com.solvevolve.screen;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.solvevolve.actors.Effects;
import com.solvevolve.sticky.Level;

public class CreditsScreen extends AbstractMenuScreen{

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		
		
		Table credits = new Table(skin);
		credits.setSize(width, height);
		credits.defaults().padRight(30);
		credits.add("Thank you for playing Rubber Boy","creditLarge").padBottom(50).row();
		
		credits.add("Code & Design","creditTitleMediumLarge").row();
		credits.add("Durga Prasad","creditLarge").padBottom(40).row();
		
		credits.add("Game Art:","creditTitleMediumLarge").row();
		credits.add("Kenny.nl","creditMediumLarge").padBottom(20).row();
		credits.add("UI Art:","creditTitleMediumLarge").row();
		credits.add("graphicburger.com","creditMediumLarge").padBottom(20).row();
		
		String musicAtt = "-Kevin MacLeod incompetech.com";
		credits.add("Music","creditTitleMediumLarge").row();
		credits.add("Easy Lemon"+musicAtt,"creditMedium").row();
		credits.add("Digya"+musicAtt,"creditMedium").row();
		credits.add("Bassa Island"+musicAtt,"creditMedium").row();
		credits.add("Call to Adventure"+musicAtt,"creditMedium").row();
		credits.add("Monkey Spinning Monkeys"+musicAtt,"creditMedium").row();
		credits.add("Sneeky Snitch"+musicAtt,"creditMedium").row();
		
		
		credits.add("Sound","creditTitleMediumLarge").padTop(30).row();
		credits.add("From freesound.org","creditMedium").row();
		String sound = "kastenfrosch__gewonnen.mp3#zedkah__bee-beat.wav#randomationpictures__powerup-3.wav#zabuhailo__ratsqueak.wav#fins__menu-click.wav#wubitog__slime-attack-or-movement.wav#halleck__jacobsladdersingle1.flac#dheming__spider-killed-foley-01.wav#paulw2k__hand-sawing.wav#spookymodem__spider-chattering.wav#acclivity__circularsaw.mp3#morgantj__scraping.mp3#dobroide__fly-00.wav#sofie__bats-2.aif#loljames__bolt-on-gate-open-close.wav";
		String sounds[] = sound.split("#");
		for(int i=0; i<sounds.length;i++){
			credits.add(sounds[i],"creditMedium").row();
		
		}
		
		credits.add("Font: CarterOne","creditMedium").padTop(40).row();
		credits.add("Made using libGDX","creditMediumLarge").padTop(40).row();
		
//		credits.setWidth(width*1f);
		
		ScrollPane scrollPane = new ScrollPane(credits, skin.getScrollStyle());
//		scrollPane.setScrollingDisabled(true, false);
		scrollPane.setSize(width, height);
		scrollPane.setOverscroll(false, true);
		
		
		menuTable.add(scrollPane);
		
		addBackButton(new MenuScreen());
		
		addTitle(height);
		
	}

	private void addTitle(float height) {
		Table floatTable = new Table();
		Label heading = new Label("Credits", skin,"heading");
//		Effects effects = new Effects();
//		stage.addActor(effects);
		stage.addActor(floatTable);
		
		floatTable.setFillParent(true);
		floatTable.add(heading).top().row();
		floatTable.add().height(height*0.92f).bottom().fill().row();
	}
}
