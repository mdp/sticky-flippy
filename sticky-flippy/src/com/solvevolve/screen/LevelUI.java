package com.solvevolve.screen;

import java.util.HashMap;

import org.omg.CORBA.Bounds;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.solvevolve.actors.ActorCell;
import com.solvevolve.actors.UIStar;
import com.solvevolve.actors.UIStar.StarType;
import com.solvevolve.sticky.Level;
import com.solvevolve.sticky.MyBoxStage;
import com.solvevolve.sticky.Sticky;
import com.solvevolve.sticky.UserStats;
import com.sun.java.swing.plaf.windows.resources.windows;

public class LevelUI {

	
	public class LevelSelectListener extends SClickListener{
		private String world;
		private String lvl;
		private int worldNo;
		private int lvlNo;

		public LevelSelectListener(String world, String lvl, int worldNo, int lvlNo) {
			this.world = world;
			this.lvl = lvl;
			this.worldNo = worldNo;
			this.lvlNo = lvlNo;
		}
		
		@Override
		public void clicked(InputEvent event, float x, float y) {
			super.clicked(event, x, y);
			Sticky sticky = ((Sticky)Gdx.app.getApplicationListener());
			sticky.setScreen(new PlayScreen(worldNo, lvlNo,null,true));
		}
	}

	public enum PauseMode{
		START, PAUSE, DEATH, SUCCESS;
		
		public String getName(){
			String name = this.toString();
			return Character.toUpperCase(name.charAt(0)) + name.substring(1).toLowerCase();
		}
	}
	
	private MySkin skin;
	private Sticky sticky;
	private UserStats userStats;
	
	public LevelUI(MySkin skin) {
		this.skin = skin;
		
		this.sticky = ((Sticky)Gdx.app.getApplicationListener());
		this.userStats =  sticky.getUserStats();
		
	}

	public MyDialog getPauseDialog(final MyBoxStage stage, final PauseMode mode, final Level level, String message){
		final MyDialog dialog = new MyDialog(mode.getName(), skin);
		
		float width = Gdx.graphics.getWidth();
		float height = Gdx.graphics.getHeight();
		Table titleTable = new Table(skin);
		Label titleLabel = new Label(mode.getName(),skin,"heading");
		titleTable.setFillParent(true);
		titleTable.add(titleLabel).padBottom(height*0.77f).row();
		
		dialog.getContentTable().addActor(titleTable);
		
		
		Table dialogTable = new Table();
		Table starTable = new Table(skin);
		

		String name = level.getDisplayName();
		String[] levelName = name.split("-");
		starTable.add(level.getLevelIndentity()+" "+levelName[0],"pauseLvlName").colspan(3).center().row();
		
		UIStar bronze = new UIStar(StarType.BRONZE,level);
		UIStar silver = new UIStar(StarType.SILVER,level);
		UIStar gold = new UIStar(StarType.GOLD,level);
		float starWidth = width/6f;
		starTable.add(bronze.getActor()).size(starWidth).right().align(Align.center);
		starTable.add(gold.getActor()).size(starWidth);
		starTable.add(silver.getActor()).size(starWidth).center().row();
		
		
		
		String msgText = levelName[1];
		switch (mode) {
		case DEATH:
		case SUCCESS:
		case PAUSE:
			msgText=message;
			break;
		case START:
			break;
		default:
			break;

		}
		Label msg = new Label("", skin,"pauseLvlText");
		msg.setText(msgText);
		
		
		if(msg.getTextBounds().width > dialog.getPrefWidth()){
			msg.setWrap(true);
			float lineCount = MathUtils.ceil(msg.getTextBounds().width/(dialog.getPrefWidth()*0.8f));
			float lineHeight = msg.getTextBounds().height;
			starTable.add(msg).colspan(3).left().padLeft(dialog.getPrefWidth()*0.1f).width(dialog.getPrefWidth()*0.9f).height(lineHeight*lineCount).padTop(30).padBottom(30).row();
		}	
		else{
			starTable.add(msg).colspan(3).center().padTop(30).padBottom(30).row();
		}
		
		
		dialogTable.add(starTable).padTop(height/40f).row();
		
		Table btnTable = new Table();
		Table btnTopTable = new Table();
		ClickListener restartListener = new SClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				((Sticky)Gdx.app.getApplicationListener()).setScreen(new PlayScreen( level.worldNo, level.lvlNo, stage.getResidues(),false));
			}
		};
		ClickListener resumeListener = new SClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				stage.hideDialog(dialog);
			}
		};
		ClickListener nextListener = new SClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				if(level.lvlNo <= 11)
					sticky.setScreen(new PlayScreen(level.worldNo, level.lvlNo+1, null,true));
				else {
					if(level.worldNo == 3){
						sticky.setScreen(new CreditsScreen());
					}
					else {
						sticky.setScreen(new WorldScreen());
					}	
				}
			}
		};
		ClickListener menuListener = new SClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				((Sticky)Gdx.app.getApplicationListener()).setScreen(new WorldScreen());
			}
		};
		

		
		Actor restart = dialog.getButton(skin.getRestartButtonStyle(), restartListener);
		Actor resume = dialog.getButton(skin.getPlayButtonStyle(), resumeListener);
		Actor menu = dialog.getButton(skin.getMenuButtonStyle(), menuListener);
		
		btnTable.defaults().maxSize(width/10).padRight(width/20);
		btnTable.add(menu).left();
		btnTable.add(restart);
		if(mode == PauseMode.SUCCESS){
			Actor next = dialog.getButton(skin.getNextButtonStyle(), nextListener);
			btnTable.add(next).size(width/12).padRight(0);
		}
		else {
			if(mode == PauseMode.DEATH){
				resume = dialog.getButton(skin.getPlayButtonStyle(), restartListener);
			}
			btnTable.add(resume).padRight(0);
		}
		
		
		
		
		Group groupHolder = new Group();
		Table btnWhole = new Table();
		btnWhole.add(btnTopTable).height(height*0.7f).row();
		btnWhole.add(btnTable).maxWidth(0.75f*width/2).row();
		
		
		groupHolder.addActor(btnWhole);
		groupHolder.addActor(dialogTable);
		
		dialog.getContentTable().add(groupHolder).row();
		return dialog;
	}
	
	private ImageButtonStyle getLevelStarStyle(ImageButtonStyle style){
		style.up = new AlphaDrawable(((TextureRegionDrawable) style.checked));
		return style;
	}
	
	public Actor getLevelBtn(String world, String level, int worldNo, int lvlNo, float width, float height){
		
		Table lvlTable = new Table();
		Table starTable = new Table();
		Group lvlGroup = new Group();
		Table tb = new Table();
		
		lvlGroup.addActor(lvlTable);
		lvlGroup.addActor(starTable);
		String btnText = level.substring(0,2);//+"\n"+userStats.getTime(worldNo,lvlNo);
		TextButton btn = new TextButton(btnText, skin.getLevelButton());
		LevelSelectListener lvlListener = new LevelSelectListener(world, level,worldNo,lvlNo);
		btn.addListener(lvlListener);
		
		lvlTable.add(btn).size(width,height).row().padBottom(width/2f);
		
		HashMap<String, Boolean> startStatus = userStats.getStarStatus(worldNo, lvlNo);
		
		ImageButton starFinish = new ImageButton(getLevelStarStyle(getStarStyle(StarType.BRONZE))), 
			starTime = new ImageButton(getLevelStarStyle(getStarStyle(StarType.SILVER))), 
			starHidden = new ImageButton(getLevelStarStyle(getStarStyle(StarType.GOLD)));
		
		
		
		starFinish.addListener(lvlListener);
		starTime.addListener(lvlListener);
		starHidden.addListener(lvlListener);
		
		starFinish.setDisabled(true);
		starTime.setDisabled(true);
		starHidden.setDisabled(true);
		
		if(startStatus.get(StarType.BRONZE.toString())){
			starFinish.setChecked(true);
		}
		if(startStatus.get(StarType.SILVER.toString())){
			starTime.setChecked(true);
		}
		if(startStatus.get(StarType.GOLD.toString())){
			starHidden.setChecked(true);
		}
		float starSize = 0.8f*width/3f;
		
		starTable.columnDefaults(0).padLeft(0.1f*width);
		starTable.columnDefaults(2).padRight(0.1f*width);
		
		starTable.add().colspan(3).width(width).height(height- 2f*starSize).row();
		starTable.add(starFinish).size(starSize);
		starTable.add(starTime).size(starSize);
		starTable.add(starHidden).size(starSize).row();
		
//		starTable.debug();
		tb.add(lvlGroup);
		
		return tb;
	}
	
	private ImageButtonStyle getStarStyle(StarType  type){
		Sticky game = (Sticky) Gdx.app.getApplicationListener();
		TextureRegionDrawable star = new TextureRegionDrawable(game.getTextues().getAtlas().findRegion("star"+ type.getName()));
		TextureRegionDrawable starHold = new TextureRegionDrawable(game.getTextues().getAtlas().findRegion("star"+ type.getName()+"Hold"));
		
		ImageButtonStyle style = new ImageButtonStyle(new ImageButtonStyle(starHold, star, star, null, null, null));
		return style;
	}
	
	public class AlphaDrawable extends TextureRegionDrawable{
		
		public AlphaDrawable(TextureRegionDrawable up) {
			super(up);
		}

		@Override
		public void draw(SpriteBatch batch, float x, float y, float width,
				float height) {
			Color color = batch.getColor();
			batch.setColor(color.r, color.g, color.b, 0.25f);
			super.draw(batch, x, y, width, height);
			batch.setColor(color);
			
		}
	}
}
