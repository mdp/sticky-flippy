package com.solvevolve.screen;

import java.util.ArrayList;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.solvevolve.actors.Residue;
import com.solvevolve.sticky.MyBoxStage;
import com.solvevolve.sticky.Sticky;
import com.solvevolve.sticky.UserStats;

public class PlayScreen implements Screen{

	private MyBoxStage stage;
	private FPSLogger fpsLogger = new FPSLogger();
	private int worldNo;
	private int lvlNo;
	private ArrayList<Residue> residues;
	private boolean viewExit;
	private UserStats userStats;
	private Sticky sticky;
	
	
	
	public PlayScreen(int worldNo, int lvlNo, ArrayList<Residue> arrayList, boolean viewExit) {
		this.worldNo = worldNo;
		this.lvlNo = lvlNo;
		this.residues = arrayList;
		this.viewExit = viewExit;
		this.sticky = ((Sticky)Gdx.app.getApplicationListener());
		this.userStats = ((Sticky)Gdx.app.getApplicationListener()).getUserStats();
		
	}

	@Override
	public void render(float delta) {
		stage.tester.start();
		stage.act(delta);
		
		stage.tester.start("draw");
		stage.draw();
		stage.tester.stop("draw");
		stage.tester.stop(stage);
		Table.drawDebug(stage);
		sticky.getSounds().act(delta);
	}
	
	private long diff, start = System.currentTimeMillis();

	public void sleep(int fps) {
	    if(fps>0){
	      diff = System.currentTimeMillis() - start;
	      long targetDelay = 1000/fps;
	      if (diff < targetDelay) {
	        try{
	            Thread.sleep(targetDelay - diff);
	          } catch (InterruptedException e) {}
	        }   
	      start = System.currentTimeMillis();
	    }
	}

	@Override
	public void resize(int width, int height) {
		
		stage.resize(width, height);	
	}

	@Override
	public void show() {
		stage = new MyBoxStage(worldNo, lvlNo, residues, viewExit);
		
	}

	@Override
	public void hide() {
//		sticky.disposeSingletons();
		sticky.getSounds().stopSounds();
		sticky.getSounds().stopLoop();
	}

	@Override
	public void pause() {
		stage.getEngine().pause();
	}

	@Override
	public void resume() {
		sticky.reload();
	}

	@Override
	public void dispose() {
		stage.dispose();
		
	}

}
