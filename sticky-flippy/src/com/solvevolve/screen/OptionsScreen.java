package com.solvevolve.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.solvevolve.sticky.Sticky;
import com.solvevolve.sticky.UserStats;

public class OptionsScreen extends AbstractMenuScreen {

	private UserStats stats;
	private ImageButton music;
	private ImageButton sound;
	private CheckBox normal;
	private CheckBox fast;
	private CheckBox low;
	private CheckBox high;

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		

		music = new ImageButton(skin.getMusicButtonStyle());
		sound = new ImageButton(skin.getSoundButtonStyle());
		
		normal = new CheckBox("Normal", skin, "radio");
		fast = new CheckBox("Fast", skin, "radio");
		
		low = new CheckBox("Low", skin, "radio");
		high = new CheckBox("High", skin, "radio");
		
		Sticky sticky = (Sticky) Gdx.app.getApplicationListener();
		stats = sticky.getUserStats();
		
		menuTable.defaults().pad(10);
		menuTable.columnDefaults(0).padRight(width/20);
		menuTable.add("Sound","menu");
		menuTable.add(music).size(width/12);
		menuTable.add(sound).size(width/12);
		menuTable.row();
		menuTable.add("Speed","menu");
		menuTable.add(normal);
		menuTable.add(fast);
		menuTable.row();
		menuTable.add("FrameRate","menu");
		menuTable.add(high);
		menuTable.add(low);
		menuTable.row();
		
		music.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				stats.music = !stats.music;
				updateScreen(true);
			}
		});
		
		sound.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				stats.sound = !stats.sound;
				updateScreen(true);
			}
		});
		


		addBackButton(new MenuScreen());
		updateScreen(false);
		
	}
	
	private void updateScreen(boolean save){
		music.setChecked(!stats.music);
		sound.setChecked(!stats.sound);

		if(save)
			stats.save();
	}
}
