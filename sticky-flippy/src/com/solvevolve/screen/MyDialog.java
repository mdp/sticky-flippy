package com.solvevolve.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class MyDialog extends Dialog {

	private Skin skin;

	public MyDialog(String title, MySkin skin) {
		super("", skin.getWindowStyle());
		initialize();
		this.skin = skin;
	}

    private void initialize() {  
//        padTop(40); // set padding on top of the dialog title  
        setModal(true);  
        setMovable(false);  
        setKeepWithinStage(false);
    }
    
    @Override  
    public MyDialog text(String text) {  
        super.text(new Label(text, skin, "timer"));  
        return this;  
    }  
      
    /**  
     * Adds a text button to the button table.  
     * @param listener the input listener that will be attached to the button.  
     */  
    public MyDialog button(String buttonText, InputListener listener) {  
    	TextButton button = new TextButton(buttonText, skin);  
        button.addListener(listener);  
        button(button);  
        return this;  
    }
    
    public Actor getButton(ImageButtonStyle style, InputListener listener ) {  
        ImageButton button = new ImageButton(style);  
        button.addListener(listener);  
        return button;  
    }
  
    @Override  
    public float getPrefWidth() {  
        // force dialog width  
        return Gdx.graphics.getWidth()/2;  
    }  
  
    @Override  
    public float getPrefHeight() {  
        // force dialog height  
    	return 4*Gdx.graphics.getHeight()/5;  
    }
    
    @Override
    public void draw(SpriteBatch batch, float parentAlpha) {
    	setKeepWithinStage(false);
    	setClip(false);
    	super.draw(batch, parentAlpha);
    }
}
