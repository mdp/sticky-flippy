package com.solvevolve.screen;

import javax.jws.soap.SOAPBinding.Style;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Window.WindowStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class MySkin extends Skin{

	private TextureAtlas atlas;
	public TextureAtlas getAtlas() {
		return atlas;
	}


	public BitmapFont fontGaint;
	public BitmapFont fontLarge;
	public BitmapFont fontMediumLarge;
	public BitmapFont fontMedium;
	public BitmapFont fontSmall;
	public int small = 12;
	public int medium = 15;
	public int mediumLarge = 22;
	public int large = 25;
	public int gaint = 35;
	
	

	public MySkin() {
		super();
		FileHandle skinFile = Gdx.files.internal("skin/newskin.json");
		generateFonts("CarterOne");
		FileHandle atlasFile = skinFile.sibling(skinFile.nameWithoutExtension() + ".atlas");
		if (atlasFile.exists()) {
			atlas = new TextureAtlas(atlasFile);
			addRegions(atlas);
		}
		load(skinFile);
		
	}
	
	public ScrollPaneStyle getScrollStyle(){
		return new ScrollPaneStyle(getNine("levelBack"), null,null,getNine("scrollBar"),getNine("scrollHandle"));
	}
	
	public Drawable getNine(String name){
		return new NinePatchDrawable(atlas.createPatch(name));
	}
	
	public TextureRegionDrawable getReg(String name){
		return new TextureRegionDrawable(atlas.findRegion(name));
	}
	
	public TextButtonStyle getTextStyle(String up, BitmapFont font){
		TextButtonStyle style = new TextButtonStyle(getNine(up), getNine(up+"Down"),null, font);
//		float amb =1- 0.05f;
		style.fontColor = Color.WHITE;
		return style;
	}
	
	public ImageButtonStyle getImageStyle(String up, String down){
		return new ImageButtonStyle(getNine(up), getNine(down), getNine(down), null, null, null);
	}
	
	private ImageButtonStyle getImageStyle(String string) {
		return getImageStyle(string, string+"Down");
	}
	
	
	public TextButtonStyle getMenuStyle(){
		return getTextStyle("greenBtn", fontLarge);
	}

	public TextButtonStyle getGreenStyle(){
		return getTextStyle("greenBtn", fontMediumLarge);
	}
	
	public TextButtonStyle getYellowStyle(){
		return getTextStyle("yellowBtn", fontMediumLarge);
	}
	
	public TextButtonStyle getRedStyle(){
		return getTextStyle("redBtn", fontMediumLarge);
	}
	
	public ImageButtonStyle getPauseButtonStyle(){
		return getImageStyle("pauseBtn");
	}


	public ImageButtonStyle getPlayButtonStyle(){
		return getImageStyle("playBtn");
	}
	
	public ImageButtonStyle getRestartButtonStyle(){
		return getImageStyle("reloadBtn");
	}
	
	public ImageButtonStyle getMenuButtonStyle(){
		return getImageStyle("menuBtn");
	}
	
	public ImageButtonStyle getNextButtonStyle(){
		return getImageStyle("nextBtn");
	}
	
	public ImageButtonStyle getStarButtonStyle(){
		TextureRegionDrawable down = new TextureRegionDrawable(atlas.findRegion("starGold"));
		TextureRegionDrawable up = new TextureRegionDrawable(atlas.findRegion("starGoldHold"));
		ImageButtonStyle btnStyle = new ImageButtonStyle(new ButtonStyle(up, up, down));
		return btnStyle;
	}
	
	
	public ImageButtonStyle getCenterButtonStyle(){
		return getImageStyle("centerLight","centerDark");
	}
	
	public ImageButtonStyle getLeftButtonStyle(){
		return getImageStyle("leftLight","leftDark");
	}
	
	public ImageButtonStyle getRightButtonStyle(){
		return getImageStyle("rightLight","rightDark");
	}
	
	
	public ImageButtonStyle getMusicButtonStyle(){
		return getImageStyle("musicBtn","musicBtnDown");
	}
	
	public ImageButtonStyle getSoundButtonStyle(){
		return getImageStyle("soundBtn","soundBtnDown");
	}
	

	
	
	@Override
	public void dispose() {
		super.dispose();
		atlas.dispose();
	}
	
	private void generateFonts(String fontName){
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/"+fontName+".ttf"));
		float density = Gdx.graphics.getDensity();
		float factor = 1;
		if(Gdx.app.getType() == ApplicationType.Desktop){
			float width = Gdx.graphics.getWidth();
			factor = 3*(width/1280);
		}
		small = (int) Math.ceil(small*density*factor);
		medium = (int) Math.ceil(medium*density*factor);
		mediumLarge = (int) Math.ceil(mediumLarge*density*factor);
		large = (int) Math.ceil(large*density*factor);
		gaint = (int) Math.ceil(gaint*density*factor);
		try {
		fontSmall = generator.generateFont(small); // font size 25 pixels		
		fontMedium = generator.generateFont(medium); // font size 25 pixels
		fontMediumLarge = generator.generateFont(mediumLarge); // font size 25 pixels
		fontLarge = generator.generateFont(large); // font size 25 pixels
		fontGaint = generator.generateFont(gaint); // font size 25 pixels
		}
		catch(Exception exception){
			Gdx.app.error("font genration", exception.getMessage());
		}
		this.add("small", fontSmall, BitmapFont.class);
		this.add("medium", fontMedium, BitmapFont.class);
		this.add("mediumLarge", fontMediumLarge, BitmapFont.class);
		this.add("large", fontLarge, BitmapFont.class);
		this.add("gaint", fontGaint, BitmapFont.class);
		generator.dispose();
	}


	public ImageButtonStyle getBackStyle() {
		return getImageStyle("backBtn");
	}


	public TextButtonStyle getLevelButton() {
		TextButtonStyle style = getTextStyle("levelBtn", fontLarge);
		style.fontColor = Color.WHITE;
		return style;
	}


	public WindowStyle getWindowStyle() {
		WindowStyle style = new WindowStyle(fontMediumLarge, Color.WHITE, getNine("dialogBack"));
		return style;
	}
	
	public WindowStyle getLevelWindowStyle(){
		WindowStyle style = new WindowStyle(fontMediumLarge, Color.WHITE, getNine("levelBack"));
		return style;
	}


	public ImageButtonStyle getLikeButton() {
		ImageButtonStyle likeBtn = getImageStyle("fbBtn");
		likeBtn.imageChecked = null;
		likeBtn.checked = null;
		return likeBtn;
	}

}
