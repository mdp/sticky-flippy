package com.solvevolve.screen;

import java.util.HashMap;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.solvevolve.sticky.Level;
import com.sun.corba.se.impl.naming.pcosnaming.NameServer;

public class WorldScreen extends AbstractMenuScreen{

	public class WorldSelectListener extends SClickListener{
		private String world;
		private int worldNo;
		private String displayName;

		public WorldSelectListener(String world, String displayName, int worldNo) {
			this.world = world;
			this.worldNo = worldNo;
			this.displayName = displayName;
		}
		
		@Override
		public void clicked(InputEvent event, float x, float y) {
			super.clicked(event, x, y);
			sticky.setScreen(new LevelsScreen(world,displayName, worldNo));
		}
	}
	
	
	@Override
	public void show() {
		super.show();
		
	}
	
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		HashMap<String, String> worlds = new HashMap<String, String>();
		
		menuTable.clear();
		String[] names = Level.getWorldNames();
		
		worlds.put(names[0], "wld-1");
		worlds.put(names[1], "wld-2");
		worlds.put(names[2], "wld-3");
		for(int i=0; i<names.length; i++){
			TextButton btn = new TextButton(names[i], skin.getMenuStyle());
			btn.addCaptureListener(new WorldSelectListener(worlds.get(names[i]),names[i], i+1));
			menuTable.add(btn).width(Gdx.graphics.getWidth()/3).padTop(height/20).padBottom(height/20).row();
		}
		
		addBackButton(new MenuScreen());
	}
}
