package com.solvevolve.screen;

import com.badlogic.gdx.graphics.Color;

public class SplashScreen extends AbstractMenuScreen{

	private static float maxTime = 1f;
	private float countTime = 0;
	
	public void resize(int width, int height) {
		super.resize(width, height);
		menuTable.add("Rubber Boy","gameTitle").row();
		menuTable.add("by","medium",Color.WHITE).row();
		menuTable.add("Durga Prasad","medium",Color.WHITE).row();
	};

	@Override
	public void render(float delta) {
		stage.draw();
		stage.act(delta);
		countTime+= delta;
		if(countTime > maxTime){
			sticky.setScreen(new MenuScreen());
		}
	}
}
