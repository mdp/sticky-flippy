package com.solvevolve.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.TimeScaleAction;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.esotericsoftware.tablelayout.BaseTableLayout;
import com.solvevolve.actors.Effects;
import com.solvevolve.sticky.Level;
import com.solvevolve.sticky.MyBoxStage;
import com.solvevolve.sticky.Sticky;
import com.solvevolve.sticky.UserStats;

public class LevelsScreen extends AbstractMenuScreen {
	
	private String world;
	private FileHandle maps;
	private int worldNo;
	private String fileDir;
	private Drawable backTexture2;
	private Label heading;
	private String displayName;
	public LevelsScreen(String world, String displayName, int worldNo) {
		super();
		this.world = world;
		this.worldNo = worldNo;
		this.displayName = displayName;
		Sticky sticky = (Sticky) Gdx.app.getApplicationListener();
		backTexture2 = sticky.getSkin().getNine("levelBack");
		
		
	}

	@Override
	public void render(float delta) {
		float pad = 0;
		SpriteBatch batch = stage.getSpriteBatch();
		batch.begin();
		batch.draw(backTexture, 0,0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		backTexture2.draw(batch, pad, 0, Gdx.graphics.getWidth()-pad, Gdx.graphics.getHeight());
		batch.end();
		stage.draw();
		stage.act(delta);
		Table.drawDebug(stage);
		if (Gdx.input.isKeyPressed(Keys.BACK) && backScreen != null){
			sticky.setScreen(backScreen);
		}
	}
	
	
	
	private FileHandle[] getLevels(){
		return Level.getLevels(maps);
	}
	
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		Table floatTable = new Table();
		maps = Level.getMapsHandle(worldNo);
		heading = new Label(displayName, skin,"heading");
//		Effects effects = new Effects();
//		stage.addActor(effects);
		stage.addActor(floatTable);
		menuTable.clear();
		
		Table lvlTable = menuTable;
		
		lvlTable.setFillParent(true);
		
		floatTable.setFillParent(true);
		floatTable.add(heading).top().row();
		floatTable.add().height(height*0.92f).bottom().fill().row();
		TextButton btn;
		int i = 0;
		float pad = 50;
		float size = (width-pad*5)/4;
		size = Math.min(size, (height-pad*4)/3);
		FileHandle[] mapsList = this.getLevels();
		LevelUI lvlUI = new LevelUI(skin);
		lvlTable.add().padBottom(height*0.26f).row();
		lvlTable.defaults().size(width/8f, 5*height/20f).padBottom(height/10).padLeft(width/40);//.padRight(width/40);
		while(i < mapsList.length && i < 12){
			Actor lvlBtn = lvlUI.getLevelBtn(world, mapsList[i].name(), worldNo, i+1,width/7.5f,height/4f);
			lvlTable.add(lvlBtn).fill();
			i++;
			if(i%6==0)
				lvlTable.row();
		}
		
		
		addBackButton(new WorldScreen());
		
		
	}
	
}
