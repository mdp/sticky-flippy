package com.solvevolve.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.renderers.BatchTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.solvevolve.actors.BackGround;
import com.solvevolve.sticky.Sticky;

public class AbstractMenuScreen implements Screen{

	protected Stage stage;
	protected Table menuTable;
	protected MySkin skin;
	protected Sticky sticky;
	protected Texture backTexture;
	protected ImageButton back;
	protected AbstractMenuScreen backScreen;
	
	
	
	@Override
	public void render(float delta) {
		SpriteBatch batch = stage.getSpriteBatch();
		batch.begin();
		batch.draw(backTexture, 0,0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		batch.end();
		stage.draw();
		stage.act(delta);
		Table.drawDebug(stage);
		if (Gdx.input.isKeyPressed(Keys.BACK) && backScreen != null){
			sticky.setScreen(backScreen);
		}
	}

	protected void addBackButton(final AbstractMenuScreen screen){
		int width = Gdx.graphics.getWidth();
		int height = Gdx.graphics.getHeight();
		
//		back = new TextButton("Back", skin.getRedStyle());
		back = new ImageButton(skin.getBackStyle());
		stage.addActor(back);
		back.addListener(new SClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				sticky.setScreen(screen);
			}
		});
		float backHeight = Math.min(back.getHeight(), width/10);
		
//		back.setBounds(width - 2*backWidth, height/20, backWidth, height/10);
//		back.setX(1.f*backWidth);
		back.setY(height - backHeight);
		back.setX(0);
		backScreen = screen;
	}
	
	@Override
	public void resize(int width, int height) {
		sticky = ((Sticky)Gdx.app.getApplicationListener());
		skin = sticky.getSkin();
		stage = new Stage();
		menuTable = new Table(skin);
		stage.addActor(menuTable);
		
		Gdx.input.setInputProcessor(stage);
		menuTable.defaults();
		
		
		menuTable.setSize(width, height);
		backTexture = new Texture(Gdx.files.internal("tiles/justBackground/"+BackGround.BGType.MUSHROOM.getImage()));
	}

	@Override
	public void show() {
		sticky = ((Sticky)Gdx.app.getApplicationListener());
		sticky.getSounds().playLoop(null);
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		if(stage != null)
			this.stage.dispose();
		
	}

}
