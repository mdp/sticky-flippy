package com.solvevolve.screen;

import oracle.jrockit.jfr.Options;
import sun.org.mozilla.javascript.internal.ast.Loop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.solvevolve.sticky.UserStats;

public class MenuScreen extends AbstractMenuScreen{

	TextButton playButton;
	private TextButton testButton;
	private TextButton options;
	private TextButton review;
	private TextButton credits;
	private TextButton exit;
	private UserStats stats;
	
	
	@Override
	public void show() {
		super.show();
//		sticky.setScreen(new CreditsScreen());
	}
	
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		TextButtonStyle style = skin.getYellowStyle();
		style.font = skin.fontLarge;
		
		TextButtonStyle playButtonStyle = skin.getGreenStyle();
		playButtonStyle.font = skin.fontLarge;
		
		TextButtonStyle exitButtonStyle = skin.getRedStyle();
		exitButtonStyle.font = skin.fontLarge;
		
		
		playButton = new TextButton("Play",playButtonStyle);
		playButton.addListener(new SClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				sticky.setScreen(new WorldScreen());
			}
		});
		
		options = new TextButton("Options", style);
		options.addListener(new SClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				sticky.setScreen(new OptionsScreen());
			}
		});
		review = new TextButton("Review", style);
		review.addListener(new SClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				sticky.getCustom().showReviewScreen();
			}
		});
		credits = new TextButton("Credits",style);
		credits.addListener(new SClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				sticky.setScreen(new CreditsScreen());
			}
		});
		exit = new TextButton("Exit", exitButtonStyle);
		exit.addListener(new SClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				Gdx.app.exit();
			}
		});
		menuTable.padTop(width/45).padBottom(width/45);
		menuTable.defaults().maxWidth(width/5).spaceTop(height/40);
		menuTable.add(playButton).width(width/5 + 80).row();
		menuTable.add(review).row();
		menuTable.add(credits).row();
//		menuTable.debug();
		this.stats = sticky.getUserStats();
		
		final ImageButton sound = new ImageButton(skin.getSoundButtonStyle());
		sound.setChecked(!stats.sound);
		sound.addListener(new SClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				stats.sound = !stats.sound;
				sound.setChecked(!stats.sound);
				stats.save();
			}
		});
		
		final ImageButton music = new ImageButton(skin.getMusicButtonStyle());
		music.setChecked(!stats.music);
		music.addListener(new SClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				stats.music = !stats.music;
				music.setChecked(!stats.music);
				if(!stats.music){
					sticky.getSounds().stopLoop();
				}	
				else {
					sticky.getSounds().playLoop(null);
				}
				stats.save();
			}
		});
		
		ImageButton fbButton = new ImageButton(skin.getLikeButton());
		fbButton.addListener(new SClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				sticky.getCustom().showFBlikeScreen();
			}
		});
		
		Table soundTable = new Table();
		soundTable.defaults().maxSize(width/14f).padRight(width/50f).padLeft(width/50f);
		soundTable.add(sound);
		soundTable.add(music);
		soundTable.add(fbButton);
		
//		soundTable.debug();
//		menuTable.debug();
		
		menuTable.add(exit).row();
		menuTable.add(soundTable).row();
	}
	
}
