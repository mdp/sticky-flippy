package com.solvevolve.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.solvevolve.sticky.Sticky;

public class SClickListener extends ClickListener {

	@Override
	public void clicked(InputEvent event, float x, float y) {
		super.clicked(event, x, y);
		Sticky sticky = ((Sticky)Gdx.app.getApplicationListener());
		sticky.getSounds().btnPressed();
	}
	
}
