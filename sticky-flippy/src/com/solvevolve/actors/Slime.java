package com.solvevolve.actors;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;

public class Slime extends Enemy{

	private TextureRegion walk2;
	private TextureRegion walk1;
	
	public Slime() {
		super("slime"+(new String[]{"","Blue"})[MathUtils.random(1)], 57/70f,(34/70f),MyBoxActor.Type.SLIME);
		this.centered = true;
		setUpWallAnimation();
	}

	protected void setUpWallAnimation() {
		this.walk1 = new TextureRegion(getRegion(name));
		this.walk2 = new TextureRegion(getRegion(name+"_walk"));
		this.walk = new Animation(speed/12, new TextureRegion[]{
				walk1,walk2
		});
	}
	
	protected void flipAnimation(boolean flipX, boolean flipY) {
		walk1.flip(flipX, flipY);
		walk2.flip(flipX, flipY);
	}
	
	protected Shape getShape(){
		PolygonShape shape = new PolygonShape(); 
		if (centered)
			shape.setAsBox((width-GAP*width) / 2, (height-GAP*height) / 2);
		else
			shape.setAsBox((width-GAP*width) / 2, (height-GAP*height) / 2, new Vector2((width-GAP*width) / 2,
					(height-GAP*height) / 2), 0);
		return shape;
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
	}
	
}
