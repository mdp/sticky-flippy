package com.solvevolve.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.solvevolve.sticky.MyBoxStage;

public abstract class ActorCell extends MyBoxActor implements CollideNotify{

	private MapProperties properties;
	public static float maxDropVel = 8;
	protected DetectType type;
	
	public ActorCell(MapProperties properties) {
		super(properties.get("detect","",String.class).toLowerCase(), 1f, 1f);
		this.properties = properties;
		type = DetectType.valueOf(properties.get("detect","",String.class));
		
	}
	
	public ActorCell(MapProperties properties,float size) {
		super(properties.get("detect","",String.class).toLowerCase(), size, size);
		this.properties = properties;
		type = DetectType.valueOf(properties.get("detect","",String.class));
		
	}
	
	@Override
	public void act(float delta) {
		super.act(delta);
		if(body.getLinearVelocity().len2() > maxDropVel*maxDropVel){
			body.setLinearVelocity(body.getLinearVelocity().nor().scl(maxDropVel));
		}
	}

	@Override
	public void spawn(MyBoxStage stage, float x, float y) {
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.friction = 1;
		fixtureDef.density = 0.001f;
		this.setWorld(stage.getEngine().getWorld(), fixtureDef, BodyType.DynamicBody, x+0.5f, y+0.5f, true);
		stage.getGroup().addActor(this);	
		
	}
	
	public void setWorld(World world, FixtureDef fixtureDef, BodyType bodyType,
			float x, float y, boolean centered) {
		
		BodyDef bodyDef = new BodyDef();
		bodyDef.angularDamping = 0.3f;
		bodyDef.type = bodyType;
		bodyDef.position.set(x, y);
		this.centered = centered;
		float gap = getGap();
		body = world.createBody(bodyDef);
		if(type.toString().toLowerCase().indexOf("coin") != -1){
			CircleShape shape = new CircleShape();
			shape.setRadius(0.25f);
			fixtureDef.shape = shape;
			body.createFixture(fixtureDef);
			shape.dispose();
		}
		else {
			PolygonShape shape = new PolygonShape();
			shape.setAsBox((width-gap) / 2, (height-gap) / 2);
			fixtureDef.shape = shape;
			body.createFixture(fixtureDef);
			shape.dispose();
		}
		
		
		body.setUserData(this);
		body.setSleepingAllowed(false);
		setPosition(x, y);
	}



	protected float getGap() {
		// TODO Auto-generated method stub
		return 0.2f;
	}

	protected boolean isDeadly(Body body){
		Object userData = body.getUserData();
		if(userData == null) return false;
		if(userData instanceof SpinnerHalf || userData instanceof Spinner || LaserButton.isLaser(body)){
			return true;
		}
		if (userData instanceof Cell) {
			Cell cell = (Cell) userData;
			if (cell.getTile() != null && cell.getTile().getProperties().get("detect", "", String.class).equals("LAVA")) {
				return true;
			}
		}	
		return false;
		
	}
	
	public MapProperties getProperties() {
		return properties;
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		boolean isSensor = body.getFixtureList().get(0).isSensor();
		if(isSensor==false){
			super.draw(batch, parentAlpha);
		}
	}

	public void collideEnd(Body body, Fixture bodyFixture){
		
	}

	public static ActorCell getActorCell(MapProperties properties2) {
		String detect = properties2.get("detect", "", String.class);
		if(detect.contains("KEY")){
			return new Key(properties2);
		}
		if(detect.contains("STAR")){
			return new Star(properties2);
		}
		
		return null;
	}
	
}
