package com.solvevolve.actors;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;

public class Coin extends ActorCell{

	private final float remainSilver = 5f;
	private final float disappear = 7f;
	private float counter = 0;
	
	public Coin(MapProperties properties) {
		super(properties);
	}
	
	@Override
	public void act(float delta) {
		super.act(delta);
		if(type == DetectType.COIN_SILVER){
			counter += delta;
			if(counter > remainSilver){
				type = DetectType.COIN_BRONZE;
				setTextureRegion(type.toString().toLowerCase());
				counter = 0;
			}
		}
		if(type == DetectType.COIN_BRONZE){
			counter +=delta;
			if(counter > disappear){
				destroy(getMyBoxStage().getEngine());
			}
		}
	}
	
	public int getValue(){
		switch(type){
		case COIN_BRONZE:
			return 2;
		case COIN_GOLD:
			return 10;
		case COIN_SILVER:
			return 5;
		default:
			return 0;
		}
	}
	
	@Override
	public boolean collideWith(Body body,Fixture fix) {
		

		if(!isDeadly(body)){
			touchDown();
		}
		return true;
	}
	
	protected void touchDown() {
		if(type == DetectType.COIN_GOLD){
			type = DetectType.COIN_SILVER;
			setTextureRegion(type.toString().toLowerCase());
			counter = 0;
		}
	}
	

}
