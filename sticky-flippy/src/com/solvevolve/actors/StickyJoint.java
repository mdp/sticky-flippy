package com.solvevolve.actors;

import java.util.ArrayList;
import java.util.Arrays;

import javax.print.attribute.Size2DSyntax;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.JointDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.DistanceJoint;
import com.badlogic.gdx.physics.box2d.joints.DistanceJointDef;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class StickyJoint {

	public static float[] IMPACT_SCALE = new float[]{Gum.MIN_SPEED,Gum.MAX_FLING_SPEED};
	public static float MAX_STICK_TIME = 3.5f;
	public static float MIN_STICK_TIME = 0.4f;
	
	private JointDef jointDef;
	private Joint joint;
	private float maxStickyTime;
	private float stickyTime;
	private World world;
	private Vector2 reflectVel;
	private int radialIndex;
	
	
	public StickyJoint(World world, JointDef jointDef, Vector2 normal, int radialIndex) {
		this.jointDef = jointDef;
		this.world = world;
		
		Vector2 velocity = new Vector2(jointDef.bodyA.getLinearVelocity());
		Gum gum = (Gum) jointDef.bodyA.getUserData();
		this.reflectVel = new Vector2(gum.body.getLinearVelocity()).add(new Vector2(normal).scl(normal.dot(velocity)*-2f));
		velocity = velocity.clamp(IMPACT_SCALE[0], IMPACT_SCALE[1]);
		float impactVelocity = Math.abs(velocity.dot(normal));
		float currMaxStickTime = MAX_STICK_TIME*(impactVelocity/IMPACT_SCALE[1]);
		if(currMaxStickTime < MIN_STICK_TIME){
			currMaxStickTime = MIN_STICK_TIME;
		}
		this.maxStickyTime = currMaxStickTime;
		this.radialIndex = radialIndex;
	}
	
	public boolean act(float delta, boolean isAttached){
		if(joint == null){
			joint = world.createJoint(jointDef);
			stickyTime = maxStickyTime;
		}
		else if(!isAttached){
			if(joint.getBodyB() == null){
				return true;
			}
			Object bodyBObject = joint.getBodyB().getUserData();
			if(bodyBObject instanceof LaserButton){
				
			}
			else {
				stickyTime -= delta;
				if(stickyTime < 0){
					return true;
				}
			}
			
		}
		return false;
	}
	
	public static Vector2 removeAllJoints(Gum gum, ArrayList<StickyJoint> list){
		Vector2 normal = getNormal(gum, list);
		StickyJoint stickyJoint;
		
		for(int i=list.size()-1; i>=0; i--){
			stickyJoint = list.get(i);
			stickyJoint.destroy();
			list.remove(i);
		}
		
		
		return normal;
	}
	
	
	
	private void addResidue() {
		
		if(joint != null && !(joint.getBodyB().getUserData()  instanceof Breakable) && !(joint.getBodyB().getUserData() instanceof Plate))
		{
//			Vector2 nor = new Vector2(gum.body.getPosition()).sub(joint.getAnchorA());
//			gum.getMyBoxStage().addResidue(normal,joint.getAnchorB());
		}
	}

	public static Vector2 getNormal(Gum gum, ArrayList<StickyJoint> list){
		if(list.size() == 0) return null;
		int[] stickingIndices = new int[list.size()];
		for(int i=0; i<list.size(); i++){
			stickingIndices[i] = list.get(i).radialIndex;
		}
 		
 		Arrays.sort(stickingIndices);
 		int start = stickingIndices[0];
 		int end = stickingIndices[list.size()-1];
 		
 		if(end == gum.radialBodyCount){
 			for(int i=1; i<stickingIndices.length; i++){
 				if(stickingIndices[i] == ++start){
 					continue;
 				}
 				else {
 					start = stickingIndices[i-1];
 					end = stickingIndices[i];
 					break;
 				}
 			}
 		}
 		Body[] radialBodies = gum.getRadialBodies();
 		Vector2 midPoint = new Vector2(radialBodies[start-1].getPosition()).add(radialBodies[end-1].getPosition()).scl(0.5f);
 		Vector2 normal = new Vector2(gum.body.getPosition()).sub(midPoint).nor();
		return normal;
		
	}
	
	public static Vector2 getAvgReflectVelocity(Gum gum, ArrayList<StickyJoint> list){
		
		StickyJoint stickyJoint;
		Vector2 avgVel = new Vector2();
		float bodiesWithVel=0;
		for(int i=list.size()-1; i>=0; i--){
			stickyJoint = list.get(i);
			if(!Float.isNaN(stickyJoint.reflectVel.x) && !Float.isNaN(stickyJoint.reflectVel.y)){
				avgVel.add(stickyJoint.reflectVel);
				bodiesWithVel++;
			}
			
		}
		if(bodiesWithVel == 0){
			return null;
		}
		return avgVel.scl(1f/bodiesWithVel).scl(1f);
	}
	

	private void destroy() {
		if(joint !=null){
			addResidue();
			world.destroyJoint(joint);
		}	
	}

	public static void update(ArrayList<StickyJoint> stickyJoints, float delta, boolean isAttached) {
		StickyJoint stickyJoint;
		
		for(int i= stickyJoints.size()-1; i>=0; i--){
			stickyJoint = stickyJoints.get(i);
			if(stickyJoint.act(delta,isAttached)){
				stickyJoint.destroy();
				stickyJoints.remove(i);
			}
		}
	}

	public static void notify(Body body, ArrayList<StickyJoint> stickyJoints) {
		StickyJoint stickyJoint;
		for(int i= stickyJoints.size()-1; i>=0; i--){
			stickyJoint = stickyJoints.get(i);
			if(stickyJoint.joint == null){
				stickyJoints.remove(i);
			}
			else if((stickyJoint.joint.getBodyA() == body || stickyJoint.joint.getBodyB() == body)){
				stickyJoint.destroy();
				stickyJoints.remove(i);
			}
		}
	}

	public static boolean isStickingTo(Body body,ArrayList<StickyJoint> stickyJoints) {
		StickyJoint stickyJoint;
		for(int i= stickyJoints.size()-1; i>=0; i--){
			stickyJoint = stickyJoints.get(i);
			if(stickyJoint.joint != null && stickyJoint.joint.getBodyB() == body){
				return true;
			}
		}
		
		for(int i= stickyJoints.size()-1; i>=0; i--){
			stickyJoint = stickyJoints.get(i);
			if(stickyJoint.jointDef != null && stickyJoint.jointDef.bodyB == body){
				return true;
			}
		}
		
		return false;
	}
	
}
