package com.solvevolve.actors;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;

public class Alien extends Enemy{

	private TextureRegion walk2;
	private TextureRegion walk1;
	
	public Alien() {
		super("alien"+(new String[]{"Green","Yellow","Pink","Blue"})[MathUtils.random(3)], 66/70f,82/70f,MyBoxActor.Type.ALIEN);
		this.centered = true;
		setUpWallAnimation();
	}

	protected void setUpWallAnimation() {
		this.walk1 = new TextureRegion(getRegion(name+"_walk1"));
		this.walk2 = new TextureRegion(getRegion(name+"_walk2"));
		this.walk = new Animation(speed/4, new TextureRegion[]{
				walk1,walk2
		});
		flipAnimation(true, false);
	}
	
	protected void flipAnimation(boolean flipX, boolean flipY) {
		walk1.flip(flipX, flipY);
		walk2.flip(flipX, flipY);
	}
	
	protected Shape getShape(){
		PolygonShape shape = new PolygonShape(); 
		if (centered)
			shape.setAsBox((width-GAP) / 2, (height-GAP) / 2);
		else
			shape.setAsBox((width-GAP) / 2, (height-GAP) / 2, new Vector2((width-GAP) / 2,
					(height-GAP) / 2), 0);
		return shape;
	}

}
