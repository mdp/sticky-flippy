package com.solvevolve.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.solvevolve.sticky.GameEngine;
import com.solvevolve.sticky.MyBoxStage;
import com.solvevolve.sticky.MyGroup;
import com.solvevolve.sticky.Sticky;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;
public class Falling extends Actor{

	private TextureRegion textureRegion;
	private Body body;
	private boolean steppedOn = false;
	private float countDown = 1.5f;
	private boolean takenCare= false;

	public Falling(TiledMapTile tiledMapTile, int i, int j) {
		setSize(1, 1);
		this.textureRegion = tiledMapTile.getTextureRegion();
		
	}
	
	public void setUp(MyBoxStage stage, Body body){
		this.body = body;
		stage.getGroup().addActor(this);
		body.setUserData(this);
		setPosition(body.getPosition().x, body.getPosition().y);
	}
	

	
	public MyBoxStage getMyBoxStage(){
		if(getParent() == null){
//			Gdx.app.log("no group", getX()+":"+getY());
			return null;
		}
		return (MyBoxStage) ((MyGroup)getParent()).getStage();
	}
	
	public void steppedOn(){
		steppedOn = true;
	}
	
	@Override
	public void act(float delta) {
		if(steppedOn){
			super.act(delta);
			this.countDown -= delta;
			if(countDown < 0.8 && !takenCare){
				takenCare = true;
				addAction(sequence(repeat(4, sequence(fadeOut(0.1f),fadeIn(0.1f))),run(new Runnable() {
					@Override
					public void run() {
						destroy(getMyBoxStage().getEngine());
						steppedOn = false;
						
					}
				})));
			}
		}
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		Color color = batch.getColor();
		Color actorColor = getColor();
		batch.setColor(color.r, color.g, color.b, actorColor.a);
		if (body != null && textureRegion != null) {
			batch.draw(textureRegion, getX(), getY(),getWidth(),getHeight());
		}
		batch.setColor(color);
	}

	public void destroy(GameEngine engine) {
		remove();
		if (body != null) {
			engine.destroyBody(body);
		}
	}

}
