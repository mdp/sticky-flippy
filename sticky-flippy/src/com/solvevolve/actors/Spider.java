package com.solvevolve.actors;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.solvevolve.sticky.MyBoxStage;

public class Spider extends Enemy{

	private TextureRegion walk2;
	private TextureRegion walk1;
	public static float GAP = 0.25f;
	
	public Spider() {
		super("spider", 1f,51/70f,MyBoxActor.Type.SPIDER);
		this.centered = true;
		setUpWallAnimation();
	}

	protected void setUpWallAnimation() {
		this.walk1 = new TextureRegion(getRegion("spider_walk1"));
		this.walk2 = new TextureRegion(getRegion("spider_walk2"));
		this.walk = new Animation(0.4f/speed, new TextureRegion[]{
				walk1,walk2
		});
	}
	
	protected void flipAnimation(boolean flipX, boolean flipY) {
		walk1.flip(flipX, flipY);
		walk2.flip(flipX, flipY);
	}
	
	protected Shape getShape(){
		PolygonShape shape = new PolygonShape(); 
		if (centered)
			shape.setAsBox((width-GAP) / 2, (height-GAP) / 2);
		else
			shape.setAsBox((width-GAP) / 2, (height-GAP) / 2, new Vector2((width-GAP) / 2,
					(height-GAP) / 2), 0);
		return shape;
	}

	public void spawn(MyBoxStage stage, float x, float y) {
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.density = 1000;
		this.setWorld(stage.getEngine().getWorld(), fixtureDef, BodyType.DynamicBody, x, y, true);
		body.setGravityScale(0);
		stage.getGroup().addActor(this);		
	}
}
