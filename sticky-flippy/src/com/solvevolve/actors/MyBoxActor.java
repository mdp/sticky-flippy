package com.solvevolve.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.solvevolve.sticky.GameEngine;
import com.solvevolve.sticky.MyBoxStage;
import com.solvevolve.sticky.MyGroup;
import com.solvevolve.sticky.Sticky;
import com.solvevolve.sticky.MyBoxStage.CamType;

public abstract class MyBoxActor extends Actor {

	public Body body;
	protected TextureRegion textureRegion;
	protected float width;
	protected float height;
	protected boolean centered;
	protected String name;
	protected boolean inView;
	protected boolean inPadView;
	private boolean enter;
	private boolean viewChanged;
	protected boolean lastPadView = false;
	protected Sticky sticky;
	
	
	public enum Type {
		BREAKABLE,ALIEN,BAT,BEE,FLY,RAT,SLIME,SPIDER,SPINNER,SPINNER_HALF, BLOCK, PLATE,NONE,GUM,LASER, TRIPLATE;

		public boolean isFlying() {
			if(this == BAT || this == BEE || this == FLY){
				return true;
			}
				
			return false;
		}
	}
	public void setTextureRegion(String name){
		Sticky game = (Sticky) Gdx.app.getApplicationListener();
		this.textureRegion = game.getTextues().getAtlas().findRegion(name);
	}
	
	public MyBoxActor(String name, float width, float height) {
		this.sticky = (Sticky) Gdx.app.getApplicationListener();
		this.width = width;
		this.height = height;
		if(name != null)
			this.textureRegion = sticky.getTextues().getAtlas().findRegion(name);
		
		setWidth(width);
		setHeight(height);
		this.name = name;
		centered = false;
		
	}

	@Override
	public String getName() {
		return name;
	}

	protected TextureRegion getRegion(String name){
		Sticky game = (Sticky) Gdx.app.getApplicationListener();
		TextureRegion region = game.getTextues().getAtlas().findRegion(name);
		
		return region;
	}
	
	public void setWorld(World world, FixtureDef fixtureDef, BodyType bodyType,
			float x, float y, boolean centered) {
		PolygonShape shape = new PolygonShape();
		BodyDef bodyDef = new BodyDef();
		bodyDef.angularDamping = 0.3f;
		bodyDef.type = bodyType;
		bodyDef.position.set(x, y);
		this.centered = centered;
		if (centered)
			shape.setAsBox(width / 2, height / 2);
		else
			shape.setAsBox(width / 2, height / 2, new Vector2(width / 2,
					height / 2), 0);
		body = world.createBody(bodyDef);
		fixtureDef.shape = shape;
		body.createFixture(fixtureDef);
		body.setUserData(this);
		shape.dispose();
		setPosition(x, y);
	}

	@Override
	public void setPosition(float x, float y) {
		super.setPosition(x, y);
		if (body != null) {
			body.setTransform(x, y, 0);
		}
	}

	public abstract void spawn(MyBoxStage stage, float x, float y);

	
	public MyBoxStage getMyBoxStage(){
		return (MyBoxStage) ((MyGroup)getParent()).getStage();
	}
	
	
	@Override
	public void act(float delta) {
		
		super.act(delta);
		inPadView =inViewBounds(getMyBoxStage().getViewBounds(), 0);
		inView =inViewBounds(getMyBoxStage().getViewBounds(), 0);
		
		if(lastPadView ^ inPadView){
			if(lastPadView){
				exitView();
			}
			else {
				enteredView();
			}
		}
		
		if(getMyBoxStage().getCamSpanType() == CamType.NORMAL)
			lastPadView = inPadView;
	}
	
	protected void exitView() {
	}

	protected void enteredView() {
	}

	protected boolean inViewBounds(Rectangle viewBounds,float pad){
		float x,y;
		if(centered){
			x = body.getPosition().x - width/2;
			y = body.getPosition().y - height/2;
		}
		else {
			x = body.getPosition().x;
			y = body.getPosition().y;
		}
		
		if ((((x + width) > viewBounds.x - pad) && x < (viewBounds.x + viewBounds.width+pad))
				&& (((y + height) > viewBounds.y -pad) && y < (viewBounds.y + viewBounds.height+pad))) {
			return true;	
		}
		return false;
	}
	
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		
		if (inView) {
			if (body != null && textureRegion != null) {
				float x ,y;
				if(centered){
					x = body.getPosition().x - width/2;
					y = body.getPosition().y - height/2;
				}
				else {
					x = body.getPosition().x;
					y = body.getPosition().y;
				}
				batch.draw(textureRegion, x, y, width / 2, height / 2, width,
						height, 1, 1, body.getAngle()* MathUtils.radiansToDegrees);
			}
		}
		
	
	}

	public void destroy(GameEngine engine) {
		remove();
		if (body != null) {
			engine.destroyBody(body);
		}
	}

	@Override
	public float getX() {
		return body.getPosition().x;
	}
	@Override
	public float getY() {
		return body.getPosition().y;
	}
	
	public static MyBoxActor getBoxActor(MyBoxActor.Type type){
		switch(type){
		case ALIEN:
			return new Alien();
		case BAT:
			return new Bat();
		case BEE:
			return new Bee();
		case FLY:
			return new Fly();
		case RAT:
			return new Rat();
		case SLIME:
			return new Slime();
		case SPIDER:
			return new Spider();
		case SPINNER:
			return new Spinner();
		case SPINNER_HALF:
			return new SpinnerHalf();
		case NONE:
			return null;
		case BLOCK:
			return new Block();
		case PLATE:
			return new Plate(3);
		case GUM:
			return new Gum();
		case BREAKABLE:
			return new Breakable();
		case LASER:
			return new LaserButton();
		default:
			break;
		}
		
		return null;
	}
}
