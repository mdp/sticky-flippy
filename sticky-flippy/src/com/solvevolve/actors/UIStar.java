package com.solvevolve.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.solvevolve.screen.MySkin;
import com.solvevolve.sticky.Level;
import com.solvevolve.sticky.Sticky;
import com.solvevolve.sticky.UserStats;

public class UIStar extends Actor{

	private AtlasRegion textureRegion;
	private StarType type;
	private MySkin skin;
	private Level level;
	private float alpha;
	private AtlasRegion textureRegionHold;
	private float starWidth;
	private float starSmallWidth;
	
	public enum StarType{
		GOLD, SILVER, BRONZE;
		public String getName(){
			String name = this.toString();
			return Character.toUpperCase(name.charAt(0)) + name.substring(1).toLowerCase();
		}
	}
	
	public UIStar(StarType type, Level level) {
		Sticky game = (Sticky) Gdx.app.getApplicationListener();
		this.skin = game.getSkin();
		this.textureRegion = game.getTextues().getAtlas().findRegion("star"+type.getName());
		this.textureRegionHold = game.getTextues().getAtlas().findRegion("star"+type.getName()+"Hold");
		float width = Gdx.graphics.getWidth();
		starWidth = width/6f;
		starSmallWidth = width/8f;
		setWidth(starSmallWidth);
		setHeight(starSmallWidth);
		if(type == StarType.GOLD){
			setWidth(starWidth);
			setHeight(starWidth);
		}
		else{
			setX((starWidth - starSmallWidth)*0.5f);
			setY((starWidth - starSmallWidth)*0.5f);
		}
		
		this.type = type;
		this.level = level;
		this.alpha = 0.15f;
		
		if(level.getStarStatus().get(type.toString()) == true){
			setAlpha(1f);
		}
	}
	

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		Color color = batch.getColor();
		batch.setColor(color.r, color.g, color.b, this.alpha);
		batch.draw(textureRegion, getX(), getY(), getWidth(), getHeight());
		batch.setColor(color);
	}


	public Actor getActor() {
		Group group = new Group();
		Table textTable = new Table();
		Label timeText = new Label("", skin,"starText");
		timeText.size(getWidth(), getHeight());
		float width = Gdx.graphics.getWidth();
		switch (type) {
		case BRONZE:
			timeText.setText("Finish");
			break;
		case GOLD:
			timeText.setText("Find");
			break;
		case SILVER:
			timeText.setText(""+level.getSilverStarTime());
			break;
		default:
			break;

		}
//		timeText.setWrap(true);
		textTable.size(getWidth(), getHeight());
		if(type == StarType.GOLD){
		}
		else{
			textTable.setX((starWidth - starSmallWidth)*0.5f);
			textTable.setY((starWidth - starSmallWidth)*0.5f);
		}
		
//		textTable.add(this).row();
		textTable.add(timeText);
		group.addActor(this);
		group.addActor(textTable);
		return group;
	}


	public void setAlpha(float f) {
		this.alpha = 1f;
		
	}
}
