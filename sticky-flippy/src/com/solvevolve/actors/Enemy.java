package com.solvevolve.actors;

import org.w3c.dom.UserDataHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.solvevolve.sticky.Box2DParser;
import com.solvevolve.sticky.Direction;
import com.solvevolve.sticky.MyBoxStage;
import com.solvevolve.sticky.MyBoxStage.CamType;

public  abstract class Enemy extends MyBoxActor implements CollideNotify,Spawner{
	public static float GAP = 0.45f;
	protected Animation walk;
	private float timePassed = 0;
	private float length;
	protected float verticalRotate = MathUtils.PI/2;
	protected float speed = 4.5f;
	
	private Vector2 startPos;
	private boolean dieOnHit = false;
	private float spawnerTimeGap = 4;
	public void setSpawnerTimeGap(float spawnerTimeGap) {
		this.spawnerTimeGap = spawnerTimeGap;
		this.spawnerTime = spawnerTimeGap;
	}

	private float spawnerTime = spawnerTimeGap;

	public Direction initStartDir;
	public int initLen;
	public Direction initSupportDir;
	public Vector2 initPos;
	public final MyBoxActor.Type type;
	private boolean reverseDir;
	private boolean flipX;
	private boolean flipY;
	private boolean justReversed;
	private float reverseTime;
	private Vector2 linearVel;
	
	public Enemy(String name, float width, float height, MyBoxActor.Type type) {
		super(name,width,height);
		this.centered = true;
		setUpWallAnimation();
		this.name = name;
		this.type = type;
	}
	
	

	public void setSpeed(float speed){
		this.speed = speed;
		setUpWallAnimation();
		if(length > 0){
			linearVel = new Vector2(body.getLinearVelocity().nor().scl(speed));
			body.setLinearVelocity(linearVel);
		}	
		else {
			linearVel = null;
			body.setLinearVelocity(0,0);
		}	
		flipAnimation(flipX, flipY);
		if(initStartDir == Direction.LEFT || initStartDir == Direction.DOWN){
			flipAnimation(true,false);
		}
		
	}
	
	public float getSpeed(){
		return this.speed;
	}
	
	protected abstract void setUpWallAnimation();

	
	public boolean shouldSpawn(float delta){
		spawnerTime -= delta;
		if(spawnerTime < 0){
			spawnerTime = spawnerTimeGap;
			return true;
		}
		return false;
	}
	@Override
	public void act(float delta) {
		super.act(delta);
		
		timePassed += delta;
		if(timePassed > walk.animationDuration){
			timePassed -= walk.animationDuration;
		}
		
		textureRegion = walk.getKeyFrame(timePassed, true);
		
		Vector2 lenVector = new Vector2(body.getPosition()).sub(startPos);
		float len2 = lenVector.len2();
		
		
		if(len2 >length*length && (linearVel != null && linearVel.dot(lenVector) > 0)){
			changeDir();
		}
		
	}
	
	private void changeDir() {
		if(dieOnHit){
			if(getParent() != null){
				inPadView =inViewBounds(getMyBoxStage().getViewBounds(), 0);
				if(inPadView)
					exitView();
//				if(lastPadView ^ inPadView){
//					if(lastPadView){
//						
//					}
//				}
				getMyBoxStage().getEngine().destroyBody(body);
				remove();
			}
			else {
//				Gdx.app.log("parent null", "WTF");
			}
			
		}
		else {
			linearVel.scl(-1);
			body.setLinearVelocity(linearVel);
			flipAnimation(true,false);
			
			justReversed = true;
		}
		
	}

	public void setDieOnHit(){
		dieOnHit = true;
	}


	protected abstract void flipAnimation(boolean flipX, boolean flipY);

	public void spawn(MyBoxStage stage, float x, float y) {
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.isSensor = true;
		this.setWorld(stage.getEngine().getWorld(), fixtureDef, BodyType.DynamicBody, x, y, true);
		body.setGravityScale(0);
		stage.getGroup().addActor(this);	
		sticky.getSounds().load(type);
		
		enteredView();
		exitView();
	}
	
	public void setOnTopRectUpSideDown(){
		Vector2 pos = body.getPosition();
		pos.y -= Box2DParser.rectWidth+0.1f;
		body.setTransform(pos, body.getAngle()+MathUtils.PI);
	}
	
	public void spawnAndSetMotion(MyBoxStage stage, Vector2 pos,Direction support, Direction move, int length){
		spawn(stage, pos.x, pos.y);
		flipX=true;
		flipY=false;
		
		this.initPos = pos;
		this.initSupportDir = move;
		this.initLen = length;
		this.initStartDir = move;
		
		if(!move.isVertical()){
			if(support == Direction.UP){
				flipY = true;
				body.setTransform(pos.x+width/2, pos.y + 1 -height/2, 0);
			}
		}
		else {
			if(support == Direction.LEFT){
				flipY = true;
				body.setTransform(pos.x+height/2, pos.y + width/2, -verticalRotate);
			}
			else {
				body.setTransform(pos.x+1-height/2, pos.y + width/2, verticalRotate);
			}
		}
		length -= 1;
		this.length = length;
		this.length = this.length/2;
		this.startPos = new Vector2(body.getPosition()).add(move.getDirVector().scl((length)/2f));
		body.setLinearVelocity(move.getDirVector());
		
		flipAnimation(flipX, flipY);
		if(initStartDir == Direction.LEFT || initStartDir == Direction.DOWN){
			flipAnimation(true,false);
		}
		
	}

	protected abstract Shape getShape();
	
	public void setWorld(World world, FixtureDef fixtureDef, BodyType bodyType,
			float x, float y, boolean centered) {
		Shape shape = getShape();
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = bodyType;
		bodyDef.position.set(x+width/2, y+height/2);
		this.centered = centered;
		body = world.createBody(bodyDef);
		fixtureDef.shape = shape;
		body.createFixture(fixtureDef);
		body.setUserData(this);
		shape.dispose();
		setPosition(x+width/2, y+height/2);
	}

	public boolean collideWith(Body collBody,Fixture inBodyFix){
		Object userData = collBody.getUserData();
		if(userData == null){
			return false;
		}
		if((userData instanceof Cell && ((Cell) userData).getTile()!= null && ((Cell) userData).getTile().getProperties().containsKey("lock") ) || userData instanceof Block || userData instanceof Breakable || LaserButton.isLaser(collBody) ){
			if(userData instanceof Block){
//				((Block)userData).stomp(2, body.getLinearVelocity().nor().scl(-1));
			}
			changeDir();
			
		}
		else if(userData instanceof Gum){
			if(getParent() == null) {
				return false;
			}
			getMyBoxStage().getEngine().foundEnemy((Gum)userData,this);
		}
		return true;
	}
	
	public void collideEnd(Body body, Fixture bodyFixture){
		
	}



	@Override
	public Type getType() {
		return type;
	}

	@Override
	public void spawnNew(MyBoxStage stage, Spawner refSpawner) {
		Enemy refEnemy = (Enemy) refSpawner;
		this.spawnAndSetMotion(stage, refEnemy.initPos, refEnemy.initSupportDir, refEnemy.initStartDir, refEnemy.initLen);
		this.setSpeed(refEnemy.getSpeed());
		this.setDieOnHit();
	};
	
	public boolean playSound(){
		if(getMyBoxStage().getCamSpanType() == CamType.NORMAL)
			return true;
		else {
			return false;
		}
	}
	
	@Override
	protected void enteredView() {
		super.enteredView();
		if(playSound())
			sticky.getSounds().actorEnter(type,dieOnHit);
	}
	
	@Override
	protected void exitView() {
		super.exitView();
		if(playSound()){
			sticky.getSounds().actorExit(type,dieOnHit);
		}
	}
	
	
	
}
