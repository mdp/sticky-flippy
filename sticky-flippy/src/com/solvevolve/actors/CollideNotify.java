package com.solvevolve.actors;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;

public interface CollideNotify {

	public boolean collideWith(Body body, Fixture bodyFixture);
	public void collideEnd(Body body, Fixture bodyFixture);
}
