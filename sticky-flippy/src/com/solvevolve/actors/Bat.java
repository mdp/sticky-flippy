package com.solvevolve.actors;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.solvevolve.sticky.Direction;
import com.solvevolve.sticky.GameEngine;

public class Bat extends Enemy{

	private TextureRegion walk2;
	private TextureRegion walk1;
	
	public Bat() {
		super("bat", 88/70f,47/70f, MyBoxActor.Type.BAT);
		this.centered = true;
		verticalRotate = 0;
		setUpWallAnimation();
	}

	protected void setUpWallAnimation() {
		this.walk1 = new TextureRegion(getRegion("bat"));
		this.walk2 = new TextureRegion(getRegion("bat_fly"));
		this.walk = new Animation(speed/18, new TextureRegion[]{
				walk1,walk2
		});
	}
	
	protected void flipAnimation(boolean flipX, boolean flipY) {
		if(body.getLinearVelocity().x !=0){
			walk1.flip(flipX, false);
			walk2.flip(flipX, false);
		}
		
	}
	
	protected Shape getShape(){
		PolygonShape shape = new PolygonShape(); 
		shape.setAsBox((width-0.2f-GAP*width) / 2, (height-0.2f-GAP*height) / 2);
	
		return shape;
	}

	public static void swarm(GameEngine engine, float batSpeed, int yOff, int yTill, int sSize,
			int sGap, int rows, int columns) {
		
		for(int i=0; i<sSize; i++){
			for(int j=yOff; j<yTill; j++){
				if(i%2==0 || i == sSize-1 || j==yOff || j==yTill-1){
					Bat bat = new Bat();
					bat.spawnAndSetMotion(engine.getStage(), new Vector2(i, j), Direction.DOWN, Direction.RIGHT, rows);
					bat.setSpeed(batSpeed);
					bat.setDieOnHit();
					engine.addSpawningEnemy(bat);
					bat.setSpawnerTimeGap(sGap);
				}
			}
		}
		
	}

}
