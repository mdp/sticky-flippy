package com.solvevolve.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Shape;

public class Spinner extends Enemy{

	private TextureRegion walk2;
	private TextureRegion walk1;
	
	public Spinner() {
		super("spinner", 61/70f,61/70f,MyBoxActor.Type.SPINNER);
		this.centered = true;
		setUpWallAnimation();
	}

	protected void setUpWallAnimation() {
		this.walk1 = new TextureRegion(getRegion("spinner"));
		this.walk2 = new TextureRegion(getRegion("spinner_spin"));
		this.walk = new Animation(2f/10, new TextureRegion[]{
				walk1,walk2
		});
		
	}
	
	protected void flipAnimation(boolean flipX, boolean flipY) {
//		walk1.flip(flipX, flipY);
//		walk2.flip(flipX, flipY);
	}
	
	protected Shape getShape(){
		CircleShape shape = new CircleShape(); 
		shape.setRadius((width-GAP)/2);
		return shape;
	}
}
