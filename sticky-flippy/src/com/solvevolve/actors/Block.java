package com.solvevolve.actors;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.solvevolve.sticky.MyBoxStage;

public class Block extends MyBoxActor{
		
	public static float GAP = 0f;
	public Block() {
		super("boxEmpty",1-GAP,1f-GAP);
		this.centered = true;
		this.name = "boxEmpty";
	}
	

	
	public void spawn(MyBoxStage stage, float x, float y) {
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.friction = 0.6f;
		fixtureDef.restitution = 0;
		fixtureDef.density = 1.f;
		this.setWorld(stage.getEngine().getWorld(), fixtureDef, BodyType.DynamicBody, x, y, true);
		stage.getGroup().addActor(this);		
	}
	

	protected  Shape getShape(){
		PolygonShape shape = new PolygonShape(); 
		if (centered)
			shape.setAsBox((width)/2-GAP/2, (height)/2 - GAP/2);
		return shape;
	}
	
	public void setWorld(World world, FixtureDef fixtureDef, BodyType bodyType,
			float x, float y, boolean centered) {
		Shape shape = getShape();
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = bodyType;
		bodyDef.position.set(x+width/2, y+height/2);
		this.centered = centered;
		body = world.createBody(bodyDef);
		fixtureDef.shape = shape;
		body.createFixture(fixtureDef);
//		body.setFixedRotation(true);
		body.setUserData(this);
		body.setGravityScale(0.8f);
		
		body.setSleepingAllowed(false);
		
		shape.dispose();
		setPosition(x+width/2, y+height/2);
	}



	public void stomp(float speed, Vector2 impactNormal) {
		body.setLinearVelocity(0, 0);
		body.applyForceToCenter(impactNormal.scl(speed*-1*70), true);
		
	}

}
