package com.solvevolve.actors;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;

public class Bee extends Enemy{

	private TextureRegion walk2;
	private TextureRegion walk1;
	
	public Bee() {
		super("bee", 56/70f,48/70f,MyBoxActor.Type.BEE);
		this.centered = true;
		verticalRotate = 0;
		setUpWallAnimation();
	}

	protected void setUpWallAnimation() {
		this.walk1 = new TextureRegion(getRegion("bee"));
		this.walk2 = new TextureRegion(getRegion("bee_fly"));
		this.walk = new Animation(speed/12, new TextureRegion[]{
				walk1,walk2
		});
	}
	
	protected void flipAnimation(boolean flipX, boolean flipY) {
		if(body.getLinearVelocity().x !=0){
			walk1.flip(flipX, false);
			walk2.flip(flipX, false);
		}
		
	}
	
	protected Shape getShape(){
		PolygonShape shape = new PolygonShape(); 
		if (centered)
			shape.setAsBox((width-GAP*width) / 2, (height-GAP*height) / 2);
		else
			shape.setAsBox((width-GAP*width) / 2, (height-GAP*height) / 2, new Vector2((width-GAP*width) / 2,
					(height-GAP*height) / 2), 0);
		return shape;
	}

}
