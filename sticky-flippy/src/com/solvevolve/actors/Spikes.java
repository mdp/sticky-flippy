package com.solvevolve.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.solvevolve.sticky.Direction;
import com.solvevolve.sticky.GameEngine;
import com.solvevolve.sticky.Level;
import com.solvevolve.sticky.MyBoxStage;

public class Spikes extends MyBoxActor{

	private Direction moveDir;
	private float heightSpike;
	private float widthSpike;
	private Direction drawDir;
	private int rotateAngle;


	public Spikes() {
		super("spikes", 1, 20/70f);
	}

	public void spawn(MyBoxStage stage, Direction dir) {
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.friction = 1;
		fixtureDef.restitution =0;
		fixtureDef.isSensor = true;
		this.moveDir = dir;
		stage.getGroup().addActor(this);
		Level lvl = stage.getEngine().getLevel();
		
		if(moveDir== Direction.RIGHT){
			this.heightSpike = lvl.getMapSize().y;
			this.widthSpike = height;
			this.drawDir = Direction.UP;
			this.rotateAngle = -90;
		}
		else if(moveDir == Direction.UP){
			this.heightSpike = height;
			this.widthSpike = lvl.getMapSize().x;
			this.drawDir = Direction.RIGHT;
			this.rotateAngle = 0;
		}
		this.setWorld(stage.getEngine().getWorld(), fixtureDef, BodyType.KinematicBody, 0, 0, true);
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		if (body != null && textureRegion != null) {
				for(int i=0; i<getMyBoxStage().getEngine().getLevel().getMapSize().x;i++){
					Vector2 offset = drawDir.getDirVector().scl(i);
					if(moveDir == Direction.RIGHT){
						batch.draw(textureRegion, body.getPosition().x+offset.x +width,
								body.getPosition().y+offset.y, 0,width, width, height,
								1, 1, rotateAngle);
					}
					else {
						batch.draw(textureRegion, body.getPosition().x+offset.x,
								body.getPosition().y+offset.y, 0,width, width, height,
								1, 1, rotateAngle);
					}
					
				}
				
		}
		
	}
	
	public void setWorld(World world, FixtureDef fixtureDef, BodyType bodyType,
			float x, float y, boolean centered) {
		PolygonShape shape = new PolygonShape();
		BodyDef bodyDef = new BodyDef();
		bodyDef.angularDamping = 0.3f;
		bodyDef.type = bodyType;
		bodyDef.position.set(x, y);
		this.centered = centered;
		shape.setAsBox(widthSpike / 2, heightSpike / 2, new Vector2(widthSpike / 2,
					heightSpike / 2), 0);
		body = world.createBody(bodyDef);
		fixtureDef.shape = shape;
		body.createFixture(fixtureDef);
		body.setUserData(this);
		shape.dispose();
		setPosition(x, y);
	}
	
	public void setSpeed(float speed){
		body.setLinearVelocity(new Vector2(moveDir.getDirVector()).scl(speed));
	}

	@Override
	public void spawn(MyBoxStage stage, float x, float y) {
	}
}
