package com.solvevolve.actors;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Json.Serializable;
import com.badlogic.gdx.utils.JsonValue;
import com.solvevolve.sticky.Level;

public class Tracker implements Serializable {

	public static class Interaction {
		public Interaction(long l, Vector2 fling) {
			timeGap = l;
			flingVelocity = fling;
		}
		public Interaction(){
			
		}
		public long timeGap;
		public Vector2 flingVelocity;
	}
	
	public long initTime;
	public ArrayList<Interaction> interactions;
	public String levelName;
	public int pointer = 0;
	private Boolean readState;

	private long current;
	
	public Tracker() {
	}
	
	public Tracker(Level lvl, Boolean read) {
		if(read == false) {
			initTime = System.nanoTime();
			interactions = new ArrayList<Tracker.Interaction>();
			levelName = lvl.name;
			readState = read;
			current = 0;
		}
		else {
			Json json = new Json();
			Tracker tracker = json.fromJson(Tracker.class, Gdx.files.local(lvl.name.replace(".tmx", ".dpx")));
			this.interactions = tracker.interactions;
			this.levelName = tracker.levelName;
			this.initTime = System.nanoTime();
			readState = true;
		}
	}
	
	public void addIteraction(Vector2 fling) {
		interactions.add(new Interaction(System.nanoTime() - initTime, fling));
	}
	
	public void save() {
		Json json = new Json();
		String serializied = json.toJson(this, Tracker.class);
		FileHandle file = Gdx.files.local(levelName.replace(".tmx", ".dpx"));
		file.writeString(serializied, false);
	}
	
	public void act(float delta, Gum gum){
		if(!readState) return; 
		current += delta*1000000000;
		if(pointer == interactions.size()) {
			return;
		}
		Interaction interaction = interactions.get(pointer);
		if(current >= (interaction.timeGap)){
			if(interaction.flingVelocity == null) {
				gum.stop();
			}
			else {
				gum.flick(interaction.flingVelocity);
			}
			pointer++;
		}
	}

	@Override
	public void write(Json json) {
		
		
//		json.writeObjectStart("levelName");
//		json.writeField(interactions, "interactions","interactions");
		json.writeValue("interactions", interactions, ArrayList.class, Interaction.class);
		json.writeValue("levelName", levelName);
//		json.writeObjectEnd();
	}

	@Override
	public void read(Json json, JsonValue jsonData) {
		
		interactions = json.readValue("interactions", ArrayList.class, Interaction.class, jsonData);
		levelName = json.readValue("levelName", String.class, jsonData);
		
		
	}
}
