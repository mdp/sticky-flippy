package com.solvevolve.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.solvevolve.sticky.GameEngine;
import com.solvevolve.sticky.MyBoxStage;
import com.solvevolve.sticky.MyGroup;
import com.solvevolve.sticky.Sticky;

public class Dots extends Actor{

	private float DOTSIZE = 0.3f;
	private Vector2[] points= new Vector2[0];
	private TextureRegion textureRegion;
	protected int maxPoint;

	public Dots() {
		
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		for(int i=0; i<maxPoint; i+=2){
			batch.draw(textureRegion,points[i].x, points[i].y, DOTSIZE, DOTSIZE);
		}
	}
	
	public void trace(Vector2 pos, Vector2 velocity) {
		float stepDuration = GameEngine.TIME_STEP ;
		int maxPoints = 200;//Math.round(-1*(velocity.y/(GameEngine.GRAVITY.y*stepDuration)) - 1);
		if(maxPoints<=0) return ;
		points = new Vector2[maxPoints];
		maxPoint = maxPoints;
		for(int i=0; i<maxPoint;i++){
			points[i] = getTrackingPoint(pos, velocity, i);
			collides(i);
		}
		
	}
	
	private Vector2 getTrackingPoint(Vector2 pos,Vector2 velocity, int timeStep){
		float stepDuration = GameEngine.TIME_STEP ;
		Vector2 tracePoint = new Vector2(pos);
		Vector2 timeStepVelocity = new Vector2(velocity.x*stepDuration, velocity.y*stepDuration);
		Vector2 timeStepGravity = new Vector2(GameEngine.GRAVITY.x*stepDuration*stepDuration, GameEngine.GRAVITY.y*stepDuration*stepDuration);
		tracePoint.add(timeStepVelocity.scl(timeStep));
		tracePoint.add(timeStepGravity.scl(timeStep*timeStep+timeStep).scl(0.5f));
		
		return tracePoint;
	}
	
	private void collides(final int pos){
		RayCastCallback callback = new RayCastCallback() {
			
			@Override
			public float reportRayFixture(Fixture fixture, Vector2 point,
					Vector2 normal, float fraction) {
				if(!fixture.isSensor())
				maxPoint = pos;
				return 0;
			}
		};
		if(pos < 5) return;
		
		MyBoxStage stage = ((MyGroup)getParent()).getStage();
		World world = stage.getEngine().getWorld(); 
		if(world != null)
			world.rayCast(callback, points[pos-1],points[pos]);
		
	}

	public void init(Vector2 position, Vector2 velocity, GameEngine gameEngine) {
		trace(position,velocity);
		Sticky game = (Sticky) Gdx.app.getApplicationListener();
		this.textureRegion = game.getTextues().getAtlas().findRegion("slimeBlue");
		if(textureRegion == null){
//			Gdx.app.log("null","blueCandy");
		}
	}
}
