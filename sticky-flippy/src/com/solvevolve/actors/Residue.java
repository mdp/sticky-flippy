package com.solvevolve.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.solvevolve.sticky.MyBoxStage;
import com.solvevolve.sticky.Sticky;

public class Residue extends Actor{

	private Vector2 normal;
	private Vector2 position;
	private AtlasRegion textureRegion;
	private float angle;
	private MyBoxStage stage;
	
	public Residue(Vector2 normal,Vector2 position) {
		this.normal = normal;
		this.position = new Vector2(position);
		this.angle = this.normal.angle() - 90;
		setWidth(0.16f);
		setHeight(0.08f);
		
	}
	

	@Override
	protected void setStage(Stage stage) {
		super.setStage(stage);
		this.stage = (MyBoxStage) stage;
		Sticky game = (Sticky) Gdx.app.getApplicationListener();
		this.textureRegion = game.getTextues().getAtlas().findRegion("residue");
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		if(stage!= null && inViewBounds(stage.getViewBounds(),2))
			batch.draw(textureRegion, position.x-getWidth()/2, position.y-getHeight()/2, getWidth() / 2, getHeight() / 2, getWidth(),
				getHeight(), 1, 1, angle);
	}
	
	private boolean inViewBounds(Rectangle viewBounds,float pad){
		float x,y;
		x = position.x - getWidth()/2;
		y = position.y - getHeight()/2;
		
		
		if ((((x + getWidth()) > viewBounds.x - pad) && x < (viewBounds.x + viewBounds.width+pad))
				&& (((y + getHeight()) > viewBounds.y -pad) && y < (viewBounds.y + viewBounds.height+pad))) {
			return true;	
		}
		return false;
	}
}
