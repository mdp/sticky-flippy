package com.solvevolve.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.solvevolve.sticky.Box2DParser;
import com.solvevolve.sticky.Direction;
import com.solvevolve.sticky.MyBoxStage;
import com.solvevolve.sticky.Sticky;

public class Breakable extends MyBoxActor{
	

	private AtlasRegion highlight;
	private TextureRegion normal;
	
	public boolean broke = false;
	private AtlasRegion fillRegion;
	private AtlasRegion blankRegion;
	private float maxStompVelocity = 20;
	private float currStompVelocity = 0;
	
	
	public Breakable() {
		super("breakable1",1,1f);
		this.centered = true;
		this.name = "bridge";
		Sticky game = (Sticky) Gdx.app.getApplicationListener();
		this.highlight = game.getTextues().getAtlas().findRegion("breakable2"); 
		this.normal = this.textureRegion;
		
		this.blankRegion = game.getTextues().getAtlas().findRegion("blankBar");
		this.fillRegion = game.getTextues().getAtlas().findRegion("fillBar");
	}
	
	public void stomp(float impactVelocity){
		if(getParent() == null){
			return ;
		}
		if(textureRegion == highlight){
			destroy(getMyBoxStage().getEngine());
			remove();
			broke = true;
			return;
		}
		if(impactVelocity > 0 ){
			currStompVelocity += impactVelocity;
			if(currStompVelocity > maxStompVelocity){
				this.textureRegion = this.highlight;
			}
		}
		
	}


	@Override
	public void act(float delta) {
		super.act(delta);
		if(currStompVelocity > 0){
			currStompVelocity -= 0.02f;
			if(currStompVelocity < maxStompVelocity){
				this.textureRegion = this.normal;
			}
		}
	}
	
	
	public void spawn(MyBoxStage stage, float x, float y) {
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.friction = 1;
		fixtureDef.restitution =0;
		this.setWorld(stage.getEngine().getWorld(), fixtureDef, BodyType.StaticBody, x, y, true);
		stage.getGroup().addActor(this);		
	}
	


	protected  Shape getShape(){
		PolygonShape shape = new PolygonShape(); 
		if (centered)
			shape.setAsBox((width) / 2, (height) / 2);
		return shape;
	}
	
	public void setWorld(World world, FixtureDef fixtureDef, BodyType bodyType,
			float x, float y, boolean centered) {
		Shape shape = getShape();
		BodyDef bodyDef = new BodyDef();
		bodyDef.allowSleep = false;
		bodyDef.type = bodyType;
		bodyDef.position.set(x+width/2, y+height/2);
		this.centered = centered;
		body = world.createBody(bodyDef);
		fixtureDef.shape = shape;
		body.createFixture(fixtureDef);
		body.setUserData(this);
		shape.dispose();
		setPosition(x+width/2, y+height/2);
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		float fillHeight = 0.2f;
		float fillWidth = currStompVelocity/maxStompVelocity;
		fillWidth = fillWidth > 1 ? 1: fillWidth;
		Vector2 offset = new Vector2(0,0.6f);
		batch.draw(blankRegion, body.getPosition().x + offset.x - width/2,
				body.getPosition().y + offset.y -height/2, 1 / 2, fillHeight / 2, 1, fillHeight,
				1, 1, body.getAngle() * MathUtils.radiansToDegrees);
		batch.draw(fillRegion, body.getPosition().x  + offset.x - width/2,
				body.getPosition().y+ offset.y -height/2, 1 / 2, fillHeight / 2, fillWidth, fillHeight,
				1, 1, body.getAngle() * MathUtils.radiansToDegrees);
		
	}
}
