package com.solvevolve.actors;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;

public class Key extends ActorCell{

	public Key(MapProperties properties) {
		super(properties);
	}

	@Override
	public boolean collideWith(Body body,Fixture fix) {
		if (body.getUserData() instanceof Gum) {
			if (getParent() == null)
				return true;
			getMyBoxStage().getEngine().keyDetected(this); 
			destroy(getMyBoxStage().getEngine());
			
		}

		if(isDeadly(body)){
			death(body.getUserData());
		}
		return true;
	}
	
	private void death(Object enemy) {
		if(type.toString().toLowerCase().indexOf("key") != -1){
			if(getParent() != null)
				getMyBoxStage().getEngine().death(this,enemy);
		}
		else {
			if(getParent() != null)
				destroy(getMyBoxStage().getEngine());
		}
	}
}
