package com.solvevolve.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;

public class Rat extends Enemy{

	private TextureRegion walk2;
	private TextureRegion walk1;
	
	public Rat() {
		super("mouse", 59/70f,35/70f,MyBoxActor.Type.RAT);
		this.centered = true;
		setUpWallAnimation();
	}

	protected void setUpWallAnimation() {
		this.walk1 = new TextureRegion(getRegion(name));
		this.walk2 = new TextureRegion(getRegion(name+"_walk"));
		this.walk = new Animation(speed/12, new TextureRegion[]{
				walk1,walk2
		});
	}
	
	protected void flipAnimation(boolean flipX, boolean flipY) {
		walk1.flip(flipX, flipY);
		walk2.flip(flipX, flipY);
		
	}
	
	protected Shape getShape(){
		PolygonShape shape = new PolygonShape(); 
		if (centered)
			shape.setAsBox((width-GAP) / 2, (height-GAP) / 2);
		else
			shape.setAsBox((width-GAP) / 2, (height-GAP) / 2, new Vector2((width-GAP) / 2,
					(height-GAP) / 2), 0);
		return shape;
	}

}
