package com.solvevolve.actors;

import javax.xml.ws.Endpoint;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.solvevolve.sticky.MyBoxStage;
import com.solvevolve.sticky.Sticky;

public class LaserButton extends MyBoxActor implements CollideNotify{

	TextureRegion pressed,relaxed,laser;
	private Fixture sensorFixture;
	private boolean isRelaxed;
	private final float maxPressTime = 3f;
	private float pressedTime=0;
	private ColorType colorType;
	private int contacts;
	private boolean laserOnPress = true;
	public void setLaserOnPress(boolean laserOnPress) {
		this.laserOnPress = laserOnPress;
	}

	protected Vector2 endpoint;
	private Body laserBody;
	protected float laserLength = 0;
	private boolean removeLaser;
	final float laserWidth = 10/70f;
	private Fixture laserFixture;
	
	public enum ColorType{
		RED,YELLOW,BLUE,GREEN;
		public String getName(){
			String name = this.toString();
			return Character.toUpperCase(name.charAt(0)) + name.substring(1).toLowerCase();
		}
	}
	
	public void setColorType(ColorType type){
		Sticky game = (Sticky) Gdx.app.getApplicationListener();
		pressed =game.getTextues().getAtlas().findRegion("button"+type.getName()+"_pressed");
		relaxed =game.getTextues().getAtlas().findRegion("button"+type.getName());
		laser = game.getTextues().getAtlas().findRegion("laser"+type.getName()+"Vertical");
		textureRegion = relaxed;
		
		this.colorType = type;
	}
	
	public LaserButton() {
		super(null, 1, 1);
		
	}

	public static boolean isLaser(Body collBody){
		LaserButton laserButton; 
		if(collBody.getUserData() instanceof LaserButton){
			laserButton = (LaserButton) collBody.getUserData();
			return laserButton.laserBody == collBody;
		}
		return false; 
	}
	
	
	
	@Override
	public void act(float delta) {
		super.act(delta);
		if(!isRelaxed && contacts ==0){
			pressedTime -= delta;
			if(pressedTime <0){
				relax();
			}
		}
		if(laserLength > 0){
			createLaser(laserLength);
			laserLength *= -1;
		}
		else if(laserLength < 0 && (isRelaxed ^ laserOnPress)){
			float oldLength = laserLength*-1;
			shootLaser();
			if(laserLength == oldLength){
				laserLength *= -1;
			}
		}
		
		if(removeLaser && laserBody != null){
			removeLaser = false;
			laserBody.getWorld().destroyBody(laserBody);
			laserBody = null;
			if(inPadView && inViewBounds(getMyBoxStage().getViewBounds(), 0)){
				exitView();
			}
		}
		if(Gdx.input.isKeyPressed(Keys.D)){
			if(laserBody !=null){
				laserBody.getWorld().destroyBody(laserBody);
				laserBody = null;
			}
			
		}
	}
	
	@Override
	public boolean collideWith(Body body, Fixture bodyFixture) {
		if(bodyFixture == sensorFixture){
			getPressed();
			contacts++;
			return true;
		}
		return false;
	}

	public void collideEnd(Body body, Fixture bodyFixture){
		if(bodyFixture == sensorFixture){
			contacts--;
		}
	}
	
	private void getPressed() {
		isRelaxed = false;
		textureRegion = pressed;
		pressedTime = maxPressTime;
		if(laserOnPress){
			shootLaser();
		}
		else{
			deleteLaser();
		}
	}
	
	private void shootLaser() {
		World world = body.getWorld();
		RayCastCallback callback = new RayCastCallback() {			
			@Override
			public float reportRayFixture(Fixture fixture, Vector2 point,
					Vector2 normal, float fraction) {
				if(!fixture.isSensor() || fixture.getBody().getType() != BodyType.StaticBody || ( fixture.getBody().getUserData() instanceof Plate )){
					endpoint = point;
					float length = body.getPosition().add(0.5f, 0).sub(endpoint).len();
					laserLength  = length;
					return fraction;
				}
				return fraction;
			}

		};
		
		world.rayCast(callback, new Vector2(body.getPosition()).add(0.5f, 0), new Vector2(body.getPosition()).add(0.5f, -100f));	
		
		
	}
	
	private void createLaser(float length) {
		if(length < 0.5){
			return;
		}
		
		if(laserBody != null){
			laserBody.getWorld().destroyBody(laserBody);
		}
		removeLaser = false;
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.density = 0.00000001f;
		fixtureDef.isSensor = true;
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.StaticBody;
		bodyDef.position.set(body.getPosition().add(0.5f, 0));
		laserBody = body.getWorld().createBody(bodyDef);
		laserBody.setUserData(this);
		PolygonShape shape = new PolygonShape();
		shape.set(new Vector2[]{
			new Vector2(-laserWidth/2,0),
			new Vector2(laserWidth/2,0),
			new Vector2(laserWidth/2,-length),
			new Vector2(-laserWidth/2,-length),
		});
		fixtureDef.shape = shape;
		laserFixture = laserBody.createFixture(fixtureDef);
		shape.dispose();
		laserBody.setGravityScale(0);
		if(inPadView && inViewBounds(getMyBoxStage().getViewBounds(), 0)){
			enteredView();
		}
	}
	
	private void deleteLaser() {
		removeLaser = true;
	}


	private void relax() {
		isRelaxed = true;
		textureRegion = relaxed;
		pressedTime = 0;
		if(laserOnPress){
			deleteLaser();
		}
		else{
			shootLaser();
		}
	}

	@Override
	public void spawn(MyBoxStage stage, float x, float y) {
		final float tempHeight = 15/70f;
		y = y + 1-tempHeight;
		FixtureDef fixtureDef = new FixtureDef();
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.StaticBody;
		bodyDef.position.set(x, y);
		body = stage.getEngine().getWorld().createBody(bodyDef);
		this.centered = false;
		
		PolygonShape shape = new PolygonShape();
		if (centered)
			shape.setAsBox(width / 2, height / 2);
		else
			shape.setAsBox(width / 2, tempHeight / 2, new Vector2(width / 2,
					tempHeight / 2), 0);
		fixtureDef.shape = shape;
		body.createFixture(fixtureDef);
		final float arcHeight = 20/70f;
		shape.setAsBox(width*0.4f / 2 , arcHeight / 2 , new Vector2(width / 2,
				tempHeight  + arcHeight/2), 0);
		
		fixtureDef.isSensor = true;
		fixtureDef.shape = shape;
		sensorFixture = body.createFixture(fixtureDef);
		body.setUserData(this);
		shape.dispose();
		setPosition(x, y);
		
		stage.getGroup().addActor(this);
		
		relax();
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		float laserLength = this.laserLength;
		if(laserLength > 0){
			laserLength *= -1;
		}
		if(laserBody !=null){
			batch.draw(laser, body.getPosition().x,
					body.getPosition().y + laserLength , 1, laserLength*-1);
		}
		
	}
	
	@Override
	protected void enteredView() {
		super.enteredView();
//		if(laserBody != null)
//			sticky.getSounds().actorEnter(Type.LASER);
	}
	
	@Override
	protected void exitView() {
		super.exitView();
//		if(laserBody != null)
//			sticky.getSounds().actorExit(Type.LASER);
	}
	
	
}
