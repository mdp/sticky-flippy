package com.solvevolve.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.solvevolve.actors.MyBoxActor.Type;
import com.solvevolve.sticky.Box2DParser;
import com.solvevolve.sticky.Direction;
import com.solvevolve.sticky.MyBoxStage;

public class Plate extends MyBoxActor implements CollideNotify,Spawner{
	public static float GAP = 0.25f;
	protected Animation walk;
	private float timePassed = 0;
	private float length;
	protected float verticalRotate = MathUtils.PI/2;
	protected float speed = 1f;
	private Direction startDir;
	private Vector2 startPos;
	private int count;
	private Vector2 collided;
	private float waitCountDown;
	private float maxWaitCountDown = 0.5f;
	private float spawnerTimeGap;
	private float spawnerTime;
	private boolean dieOnHit;
	private Vector2 initPos;
	private Direction initSupportDir;
	private Direction initStartDir;
	private int initLen;
	private Vector2 linearVel;
		
	public Plate(int count) {
		super("plate",count,40f/70f);
		this.centered = true;
		this.name = "plate";
		this.count = count;
	}
	
	

	public void setSpeed(float speed){
		this.speed = speed;
		linearVel = body.getLinearVelocity().nor().scl(speed);
		body.setLinearVelocity(linearVel);
		
	}
	

	@Override
	public void act(float delta) {
		super.act(delta);
		timePassed += delta;
		
		String str = "";
		
		Vector2 lenVector = new Vector2(body.getPosition()).sub(startPos);
		float len = lenVector.len();
		
		
//		Vector2 futVector = new Vector2(lenVector);
//		Vector2 vel = new Vector2(body.getLinearVelocity());
//		futVector.add(vel.scl(0.2f));
		
		
		
		if(collided != null){
			waitCountDown -= delta;
			if(waitCountDown < 0){
				moveOtherWay();
				str += "moved other way";
			}
			else {
				linearVel = null;
				body.setLinearVelocity(new Vector2(0,0));
			}
			
		}
		else {
			if(len > length && (linearVel != null && linearVel.dot(lenVector) > 0)){
				stopTochangeDir();
				str += "stopping for a while";
			}
		}
		
	}
	
	private void moveOtherWay() {
		if(dieOnHit){
			getMyBoxStage().getEngine().destroyBody(body);
			remove();
		}
		else {
			body.setLinearVelocity(collided.scl(-1));	
			linearVel = collided;
			collided = null;
			waitCountDown = maxWaitCountDown ;
		}
	}



	private void stopTochangeDir() {
		if(collided == null){
			collided = body.getLinearVelocity();
			waitCountDown = maxWaitCountDown;
		}
	}



	public void spawn(MyBoxStage stage, float x, float y) {
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.friction = 1;
		fixtureDef.restitution =0;
		fixtureDef.density = 100000;
		this.setWorld(stage.getEngine().getWorld(), fixtureDef, BodyType.DynamicBody, x, y, true);
		stage.getGroup().addActor(this);
		this.body.setGravityScale(0);
		this.body.setFixedRotation(true);
	}
	

	public void spawnAndSetMotion(MyBoxStage stage, Vector2 pos,Direction support,Direction move, int length){
		spawn(stage, pos.x, pos.y);
		
		length--;
		this.length = length;
		this.length = this.length/2;
		Vector2 bodyPos = body.getPosition();
		this.startPos = new Vector2(body.getPosition()).add(move.getDirVector().scl((length)/2f));
		
		this.initPos = pos;
		this.initSupportDir = move;
		this.initLen = length;
		this.initStartDir = move;
		
		this.body.setLinearVelocity(move.getDirVector().scl(speed));
		
	}

	protected  Shape getShape(){
		PolygonShape shape = new PolygonShape();
		float gap = GAP;
		if (centered)
			shape.setAsBox((width) / 2, (height-gap) / 2);
		return shape;
	}
	
	public void setWorld(World world, FixtureDef fixtureDef, BodyType bodyType,
			float x, float y, boolean centered) {
		Shape shape = getShape();
		BodyDef bodyDef = new BodyDef();
		bodyDef.allowSleep = false;
		bodyDef.type = bodyType;
		bodyDef.position.set(x+width/2, y+1-height/2);
		this.centered = centered;
		body = world.createBody(bodyDef);
		fixtureDef.shape = shape;
		body.createFixture(fixtureDef);
		body.setUserData(this);
		shape.dispose();
		setPosition(x+width/2, y+1-height/2);
	}

	public boolean collideWith(Body collBody,Fixture inBodyFix){
		Object userData = collBody.getUserData();
		if((collBody.getType() == BodyType.StaticBody || userData instanceof Block) && ( userData == null || !LaserButton.isLaser(collBody))){
			stopTochangeDir();
		}
		return false;
	}
	
	public void collideEnd(Body body, Fixture bodyFixture){
		
	};
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		int i =0;
		if (body != null && textureRegion != null) {
			if(centered){
					batch.draw(textureRegion, body.getPosition().x-width/2 + i,
							body.getPosition().y - height/2,  count, height);
				
			}
			else {
				batch.draw(textureRegion, body.getPosition().x,
						body.getPosition().y, width / 2, height / 2, width, height,
						1, 1, body.getAngle() * MathUtils.radiansToDegrees);
			}
		}
	}



	public void setSpawnerTimeGap(float spawnerTimeGap) {
		this.spawnerTimeGap = spawnerTimeGap;
		this.spawnerTime = spawnerTimeGap;
	}



	public boolean shouldSpawn(float delta){
		spawnerTime -= delta;
		if(spawnerTime < 0){
			spawnerTime = spawnerTimeGap;
			return true;
		}
		return false;
	}



	@Override
	public Type getType() {
		return MyBoxActor.Type.PLATE;
	}

	@Override
	public void spawnNew(MyBoxStage stage, Spawner refSpawner) {
		Plate palteRef = (Plate) refSpawner;
		this.spawnAndSetMotion(stage, palteRef.initPos, palteRef.initSupportDir, palteRef.initStartDir, palteRef.initLen);
		this.setDieOnHit();
	}



	public void setDieOnHit() {
		this.dieOnHit = true;
	};
}
