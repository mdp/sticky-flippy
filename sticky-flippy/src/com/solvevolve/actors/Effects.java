package com.solvevolve.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Effects extends Actor{

	
	private ParticleEffect effect;

	public Effects() {
		effect = new ParticleEffect();
		effect.load(Gdx.files.internal("effects/particle"),Gdx.files.internal("tiles/effects"));
		effect.setPosition(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
		effect.start();
		
	}
	
	public void start(){
		effect.start();
	}
	
	@Override
	public void act(float delta) {
		super.act(delta);
		effect.update(delta);
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		effect.draw(batch);
	}
}
