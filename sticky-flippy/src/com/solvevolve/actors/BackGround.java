package com.solvevolve.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.solvevolve.sticky.GameEngine;

public class BackGround extends Actor{

	Texture backTexture;
	
	public enum BGType{
		MUSHROOM, GRASSLAND, DESERT, CASTLE;
		public String getImage(){
			switch (this) {
			case CASTLE:
				return "bg_castle.png";
			case DESERT:
				return "bg_desert.png";
			case GRASSLAND:
				return "bg_grasslands.png";
			case MUSHROOM:
				return "bg_shroom.png";
			default:
				return "bg_castle.png";
			}
			
		}
	}
	
	public BackGround(BGType type, int offSetY,int height) {
		backTexture = new Texture(Gdx.files.internal("tiles/justBackground/"+type.getImage()));
		backTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		backTexture.setWrap(TextureWrap.Repeat,TextureWrap.Repeat);
		setX(-20);
		setY(offSetY);
		
	}
	
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		Color color = batch.getColor();
		batch.setColor(color.r, color.g, color.b, 1f);
		batch.draw(backTexture, getX(), getY(), 16*30, 9*30,0,30 ,30,0);
		batch.setColor(color);
	}


	public void reactToMove(Vector3 camPos, Vector3 camOld) {
		setX(getX()- 0.2f*(camPos.x-camOld.x));
//		setY((camPos.y)-9);
	}


	public void draw(SpriteBatch batch, int i, Rectangle viewBounds) {
		Vector2 grid = GameEngine.SCREEN_GRID;
		int leftX = MathUtils.floor(viewBounds.x/grid.x);
		int leftY = MathUtils.floor(viewBounds.y/grid.y);
		
		int down = leftY%2==0? 1 : 0;
		int up = leftY%2==0? 0 : 1;
		batch.draw(backTexture, leftX*grid.x, leftY*grid.y, grid.x, grid.y,0,down,1,(down+1)%2);
		batch.draw(backTexture, (leftX+1)*grid.x, leftY*grid.y, grid.x, grid.y,0,down,1,(down+1)%2);
		batch.draw(backTexture, (leftX+1)*grid.x, (leftY+1)*grid.y, grid.x, grid.y,0,up,1,(up+1)%2);
		batch.draw(backTexture, (leftX)*grid.x, (leftY+1)*grid.y, grid.x, grid.y,0,up,1,(up+1)%2);
	}
	
	
}
