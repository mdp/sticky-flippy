package com.solvevolve.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.solvevolve.sticky.MyBoxStage;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;
public class Star extends ActorCell{

	
	
	public Star(MapProperties properties) {
		super(properties,0.9f);
	}

	@Override
	public void act(float delta) {
		super.act(delta);
		
	}
	@Override
	public boolean collideWith(Body body,Fixture fix) {
		if (body.getUserData() instanceof Gum) {
			if (getParent() == null)
				return true;
			getMyBoxStage().getEngine().keyDetected(this); 
			destroy(getMyBoxStage().getEngine());
			
		}

		if(isDeadly(body)){
			destroy(getMyBoxStage().getEngine());
			remove();
		}
		if(body.getUserData() instanceof Gum){
			((Gum)body.getUserData()).foundStar();
		}
		return true;
	}
	
	@Override
	protected void setParent(Group parent) {
		super.setParent(parent);
//		addAction(forever(sequence(alpha(0.65f, 0.2f),alpha(1f, 0.2f))));
	}
	
	@Override
	public void spawn(MyBoxStage stage, float x, float y) {
		super.spawn(stage, x, y);
		body.setFixedRotation(true);
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
//		super.draw(batch, parentAlpha);
		
		if (inView) {
			if (body != null && textureRegion != null) {
				float x ,y;
				if(centered){
					x = body.getPosition().x - width/2;
					y = body.getPosition().y - height/2;
				}
				else {
					x = body.getPosition().x;
					y = body.getPosition().y;
				}
				
				batch.draw(textureRegion, x, y, width / 2, height / 2, width,
						height, 1, 1, body.getAngle()* MathUtils.radiansToDegrees);
				
			}
		}
	}
}
