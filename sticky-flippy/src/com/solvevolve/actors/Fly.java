package com.solvevolve.actors;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;

public class Fly extends Enemy{

	private TextureRegion walk2;
	private TextureRegion walk1;
	
	public Fly() {
		super("fly", 65/70f,45/70f,MyBoxActor.Type.FLY);
		this.centered = true;
		verticalRotate = 0;
		setUpWallAnimation();
	}

	protected void setUpWallAnimation() {
		this.walk1 = new TextureRegion(getRegion("fly"));
		this.walk2 = new TextureRegion(getRegion("fly_fly"));
		this.walk = new Animation(speed/4, new TextureRegion[]{
				walk1,walk2
		});
	}
	
	protected void flipAnimation(boolean flipX, boolean flipY) {
		if(body.getLinearVelocity().x !=0){
			walk1.flip(flipX, false);
			walk2.flip(flipX, false);
		}
		
	}
	
	protected Shape getShape(){
		PolygonShape shape = new PolygonShape(); 
		shape.setAsBox((width-0.2f-GAP*width) / 2, (height-0.2f-GAP*height) / 2);
	
		return shape;
	}

}
