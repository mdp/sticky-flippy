package com.solvevolve.actors;

import com.solvevolve.actors.MyBoxActor.Type;
import com.solvevolve.sticky.MyBoxStage;

public interface Spawner {

	public void setSpawnerTimeGap(float spawnerTimeGap);
	public boolean shouldSpawn(float delta);
	public Type getType();
	public void spawnNew(MyBoxStage stage, Spawner refEnemy);
	
}
