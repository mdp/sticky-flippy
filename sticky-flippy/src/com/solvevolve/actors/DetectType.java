package com.solvevolve.actors;

public enum DetectType {
	SPIKE,LAVA,KEY_GREEN,KEY_YELLOW,KEY_RED,KEY_BLUE,EXIT,COIN_GOLD,COIN_SILVER,COIN_BRONZE,SIGN, STAR;
	public enum LockType {
		GREEN,YELLOW,RED,BLUE;
	}
}
