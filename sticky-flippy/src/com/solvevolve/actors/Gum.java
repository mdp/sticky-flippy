package com.solvevolve.actors;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.JointDef.JointType;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.DistanceJointDef;
import com.badlogic.gdx.physics.box2d.joints.WeldJointDef;
import com.solvevolve.sticky.GameEngine;
import com.solvevolve.sticky.MyBoxStage;
import com.solvevolve.sticky.Sticky;

public class Gum extends MyBoxActor {

	public static float FREE_MOVE_WIDTH = 0f;
	public static float FREE_MOVE_HEIGHT = 0f;
	public static float MAX_LAUNCH_SPEED = 12f;
	public static float MAX_FLING_SPEED = 8f;
	public static float MAX_DOUBLE_FLING_SPEED = 8f;
	public static float MAX_LOW_FLING_SPEED = 8f;
	private static float BASIC_STICK_SPEED = 0f;
	public static float LOW_FLING_DISTANCE = 1.2f;
	public static float MIN_SPEED = 0.5f;

	public int jumps = 0;

	private State currState = State.FLICK;
	private AtlasRegion fillRegion;
	private AtlasRegion blankRegion;
	private boolean isAttached = false;
	private float attachVelocity;
	private boolean justAttached;
	public static float MAX_ATTACH_VELOCITY = 20f;
	

	private int bodyAngle = 20;
	private float bodyRadius = 0.01f;
	public int radialBodyCount = 360 / bodyAngle;
	private Body[] radialBodies = new Body[radialBodyCount];

	private ArrayList<StickyJoint> stickyJoints = new ArrayList<StickyJoint>();
	private float spingyness = 30f;
	private float dampaning = 0.5f;
	private boolean bounceStuck;
	private Set<Body> lastAttachedBodies = new HashSet<Body>();
	private Set<Body> currAttachedBodies = new HashSet<Body>();
	
	private float ignoreStickTime = 0.2f;
	private float ignoreStickCountDown;
//	private Tracker tracker;
	public Vector2 lastVel = new Vector2();
	private Vector2 bounceNormal;
	private Vector2 bounceVel;
	
	public static float GUM_SIZE = 0.75f;
	
	public enum State {
		STICK, FLICK;
	}
	
	
	

	public Gum() {
		super("candyFloat",GUM_SIZE, GUM_SIZE);
		Sticky game = (Sticky) Gdx.app.getApplicationListener();
		
		bodyAngle = 20;
		bodyRadius = 0.01f;
		radialBodyCount = 360 / bodyAngle;
		radialBodies = new Body[radialBodyCount];
		
		this.blankRegion = game.getTextues().getAtlas().findRegion("blankBar");
		this.fillRegion = game.getTextues().getAtlas().findRegion("fillBar");


		this.gumTexture = new Texture("tileset/gum.png");
		this.gumTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		this.gumTexture.unsafeSetFilter(TextureFilter.Linear, TextureFilter.Linear, true);
		
	}

	public void actStick(float delta) {
		if (currState == State.STICK) {
			StickyJoint.update(stickyJoints, delta,isAttached);
			
		} else {
			StickyJoint.removeAllJoints(this, stickyJoints);
		}
	}

	@Override
	public void act(float delta) {
		super.act(delta);
		actStick(delta);
//		tracker.act(delta, this);
		if (isAttached && attachVelocity > 0) {
			attachVelocity -= 0.06f;
			if (attachVelocity < 0) {
				attachVelocity = 0;
			}
		}
		
		if(ignoreStickCountDown > 0){
			ignoreStickCountDown -= delta;
		}
		
		
		if(bounceStuck ){
			Vector2 impactVel  = StickyJoint.getAvgReflectVelocity(this, stickyJoints);
			if(impactVel != null){
				bounceStuck = false;
				setVel(impactVel, true);
			}
		}
		
		
	}

	public void spawn(MyBoxStage stage, float x, float y) {
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.density = 100;
		fixtureDef.restitution = 1f;
		fixtureDef.friction = 1f;
		this.setWorld(stage.getEngine().getWorld(), fixtureDef,
				BodyType.DynamicBody, x, y, true);
		stage.getGroup().addActor(this);
//		tracker = new Tracker(getMyBoxStage().getEngine().getLevel(), false);
	}

	public void setRestitution(float res) {
		body.getFixtureList().get(0).setRestitution(res);
		for (int i = 0; i < radialBodies.length; i++) {
			radialBodies[i].getFixtureList().get(0).setRestitution(res);
		}
	}

	public Vector2 adjustFlickVelocity(Vector2 velocity) {
		velocity.add(body.getLinearVelocity());
		velocity.clamp(MIN_SPEED, MAX_FLING_SPEED);
		return velocity;
	}

	public void setAttached() {
		isAttached = true;
		justAttached = false;
	}

	public void flick(Vector2 velocity) {
//		tracker.addIteraction(velocity);
		if (isAttached && StickyJoint.getNormal(this, stickyJoints) != null) {
			 Vector2 normal = StickyJoint.getNormal(this, stickyJoints);
			 attachVelocity += normal.dot(velocity);
			 if(attachVelocity > MAX_ATTACH_VELOCITY){
				isAttached = false;
				justAttached = true;				 
				StickyJoint.removeAllJoints(this, stickyJoints);
				attachVelocity = 0;
				lastAttachedBodies = currAttachedBodies;
				currAttachedBodies = new HashSet<Body>();
			 }
			
			 if(attachVelocity < 0){
				 attachVelocity = 0;
			 }
		} else if (jumps < 2) {
			Vector2 normal = StickyJoint.getNormal(this, stickyJoints);
			if(jumps ==1){
				velocity.clamp(MIN_SPEED, MAX_DOUBLE_FLING_SPEED);
			}
			
			if ( normal == null  || normal.dot(velocity) >= -0.75f* MAX_DOUBLE_FLING_SPEED) {
				jumps++;
				setVel(velocity,true);
				setCurrState(State.FLICK);
				justAttached = false;
				getMyBoxStage().showMessage("", null);
				if(jumps == 0 ){
					ignoreStickCountDown = ignoreStickTime;
				}
			}

			lastAttachedBodies = currAttachedBodies;
			currAttachedBodies = new HashSet<Body>();
			
		}
	}

	public void stop() {
//		tracker.addIteraction(null);
		
		if(!isAttached){
			setVel(new Vector2(0, 0),true);
		}
		ignoreStickCountDown = ignoreStickTime;
		
	}
	
	public void levelDone(){
//		tracker.save();
	}

	public void setVel(Vector2 vel, boolean removeJoints) {
		bounceStuck = false;
		if(removeJoints){
			 StickyJoint.removeAllJoints(this, stickyJoints);
		}	
		else {
			StickyJoint.getNormal(this, stickyJoints);
		}
		if(vel == null) return;
		body.setLinearVelocity(vel);
		for (int i = 0; i < radialBodies.length; i++) {
			radialBodies[i].setLinearVelocity(vel);
		}
		this.lastVel = vel;

	}

	public void stick(Fixture radialFix, Body detectBody, Contact contact) {
		
		this.bounceStuck = false;
		Body radialBody = radialFix.getBody();
		Object detectData = detectBody.getUserData();
		GameEngine engine = getMyBoxStage().getEngine();
		if(LaserButton.isLaser(detectBody)){
			engine.death(this, detectBody.getUserData());
		}
		else if(detectData instanceof Cell){
			Cell detectCell = (Cell) detectData;
			if(detectCell.getTile() == null){
				return;
			}
			MapProperties properties = detectCell.getTile().getProperties();
			if(properties.containsKey("attach")){
				if(!justAttached || !lastAttachedBodies.contains(detectBody)){
					getStuck(radialBody, detectBody, contact, false);
					setAttached();
					currAttachedBodies.add(detectBody);
				}
			}
			else if(properties.containsKey("bounce")){
				setRestitution(0f);
				getStuck(radialBody, detectBody, contact,false);
//				if(bounceStuck == false){
					this.bounceStuck = true;
//					this.bounceNormal = contact.getWorldManifold().getNormal();
//					this.bounceVel = body.getLinearVelocity();
//				}
			}
			else if(properties.containsKey("detect")&& !properties.containsKey("lock")){
				engine.detected(this, detectBody);
			}
			else {
				getStuck(radialBody, detectBody, contact,true);
			}
		}
		else if(detectData instanceof MyBoxActor){
			if(detectData instanceof Enemy){
				engine.foundEnemy(this,(Enemy) detectData);
			}
			else if(detectData instanceof Spikes){
				engine.death(this, (Spikes)detectData);
			}
			else if(detectData instanceof Breakable){
				Breakable breakable = (Breakable) detectData;
				float impactSpeed = radialBody.getLinearVelocity().len();//contact.getWorldManifold().getNormal().dot(radialBody.getLinearVelocity())*-1;
				if (!StickyJoint.isStickingTo(breakable.body, stickyJoints)) {
					breakable.stomp(impactSpeed);
				}
				if (breakable.broke) {
					return;
				} else {
					getStuck(radialBody, detectBody, contact,false);
				}
			}
			else if(detectData instanceof Block){
				Block block = (Block) detectData;
				float impactVel = contact.getWorldManifold().getNormal().dot(radialBody.getLinearVelocity())*-1;
				if (!StickyJoint.isStickingTo(block.body, stickyJoints)) {
					block.stomp(impactVel, contact.getWorldManifold().getNormal());
				}
				getStuck(radialBody, detectBody, contact,false);
				
			}
			else {
				getStuck(radialBody, detectBody, contact,false);
			}
		}
		else if(detectData instanceof Falling){
			Falling falling = (Falling) detectData;
			falling.steppedOn();
			getStuck(radialBody, detectBody, contact,false);
		}
		else {
			getStuck(radialBody, detectBody, contact,true);
		}

		
	}
	
	private void getStuck(Body radialBody, Body detectBody, Contact contact, boolean useIgnoreStickTime) {
		if (radialBody == body)
			return;

		if( ignoreStickCountDown > 0 && !(detectBody.getUserData() instanceof Breakable)) {
			jumps = 0; 
			return;
		}
		
		WeldJointDef def = new WeldJointDef();
		def.collideConnected = true;
		Vector2 normal = contact.getWorldManifold().getNormal();
		Vector2 contactPoint = contact.getWorldManifold().getPoints()[0];
		def.initialize(radialBody, detectBody, contactPoint);
		if (normal.dot(radialBody.getLinearVelocity()) < -Gum.BASIC_STICK_SPEED) {
			StickyJoint stickyJoint = new StickyJoint(getMyBoxStage()
					.getEngine().getWorld(), def, normal, getIndex(radialBody));
			stickyJoints.add(stickyJoint);
			jumps = 0;
			if(detectBody.getUserData() instanceof Cell){
				Cell cell = (Cell) detectBody.getUserData();
				if(!cell.getTile().getProperties().containsKey("detect")){
					getMyBoxStage().addResidue(normal, radialBody.getPosition());
				}
			}	
		}
		
		setCurrState(State.STICK);

	}
	
	public int getIndex(Body body){
		for(int i=0; i<radialBodies.length; i++){
			if(body == radialBodies[i])
				return i+1;
		}
		return -1;
	}

	public Vector2 getCameraPos() {
		Vector2 orgPos = new Vector2(getX(), getY());
		Vector2 pos = localToStageCoordinates(new Vector2(getX(), getY()));
		pos = getStage().stageToScreenCoordinates(pos);
		int height = Gdx.graphics.getHeight();
		int width = Gdx.graphics.getWidth();
		pos.y = Gdx.graphics.getHeight() - pos.y;
		
		
		return orgPos;
	}

	public void setWorld(World world, FixtureDef fixtureDef, BodyType bodyType,
			float x, float y, boolean centered) {
		BodyDef bodyDef = new BodyDef();
		bodyDef.angularDamping = 0.3f;
		bodyDef.type = bodyType;
		bodyDef.position.set(x, y);
		this.centered = centered;

		CircleShape shape = new CircleShape();
		shape.setRadius(bodyRadius / 2);
		
		body = world.createBody(bodyDef);
		fixtureDef.shape = shape;
		body.createFixture(fixtureDef);
		body.setUserData(this);

		setPosition(x, y);
		
		
		shape.setRadius(bodyRadius / 2);
		fixtureDef.isSensor = false;
		createRadialBodies(world, fixtureDef, bodyDef, shape);
		createCentralJoints(world);
		createRadialJoints(world);
		shape.dispose();
	}

	private void createRadialBodies(World world, FixtureDef fixtureDef,
			BodyDef bodyDef, CircleShape shape) {
		Vector2 radial = new Vector2(width / 2, 0);
		Vector2 center = new Vector2(body.getPosition());
		for (int i = 0; i < radialBodies.length; i++) {
			radial.rotate(bodyAngle);
			bodyDef.position.set(center.x + radial.x, center.y + radial.y);
			radialBodies[i] = world.createBody(bodyDef);
			radialBodies[i].createFixture(fixtureDef);
			radialBodies[i].setUserData(this);
		}
	}

	private DistanceJointDef getBasicDef() {
		DistanceJointDef distanceJointDef = new DistanceJointDef();
		distanceJointDef.dampingRatio = this.dampaning ;
		distanceJointDef.frequencyHz = this.spingyness;
		distanceJointDef.collideConnected = true;
		distanceJointDef.type = JointType.DistanceJoint;
		return distanceJointDef;
	}

	private void createCentralJoints(World world) {
		DistanceJointDef def = getBasicDef();
		def.collideConnected = false;
		// def.dampingRatio = 0f;
		// def.frequencyHz = 0;
		for (int i = 0; i < radialBodies.length; i++) {
			def.initialize(body, radialBodies[i], body.getPosition(),
					radialBodies[i].getPosition());
			world.createJoint(def);
		}
	}

	private void createRadialJoints(World world) {
		DistanceJointDef def = getBasicDef();
		for (int i = 1; i < radialBodies.length; i++) {
			def.initialize(radialBodies[i - 1], radialBodies[i],
					radialBodies[i - 1].getPosition(),
					radialBodies[i].getPosition());
			world.createJoint(def);
		}
		int length = radialBodies.length;
		def.initialize(radialBodies[length - 1], radialBodies[0],
				radialBodies[length - 1].getPosition(),
				radialBodies[0].getPosition());
		world.createJoint(def);
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
//		super.draw(batch, parentAlpha);
		if (isAttached && StickyJoint.getNormal(this,
				stickyJoints) != null) {
			float fillHeight = 0.2f;
			float fillWidth = width*attachVelocity / MAX_ATTACH_VELOCITY ;
			Vector2 offset = new Vector2(StickyJoint.getNormal(this,
					stickyJoints)).scl(0.1f);
			batch.draw(blankRegion,
					body.getPosition().x - width / 2 + offset.x,body.getPosition().y + offset.y - height / 2,
					width / 2,fillHeight / 2,
					width, fillHeight,
					1, 1, body.getAngle()* MathUtils.radiansToDegrees);
			batch.draw(fillRegion, body.getPosition().x - width / 2 + offset.x,body.getPosition().y + offset.y - height / 2,
					width / 2,fillHeight / 2,
					fillWidth, fillHeight,
					1, 1,body.getAngle() * MathUtils.radiansToDegrees);

		}
		customDraw();
	}

	public void setCurrState(State currState) {
		this.currState = currState;
	}

	public ArrayList<StickyJoint> getStickyJoints() {
		return stickyJoints;
	}

	private FloatBuffer vertexBuffer; // buffer holding the vertices
	private int count;
	private FloatBuffer textureBuffer;	// buffer holding the texture coordinates
	private Texture gumTexture;
	private boolean starFound;
	public void initBuffer() {
	
		float vertices[] = new float[3+radialBodies.length*3+3];
		vertices[0] = body.getPosition().x;
		vertices[1] = body.getPosition().y;
		vertices[2] = 0;
		int last = radialBodies.length+1;
		vertices[3*last + 0] = radialBodies[0].getPosition().x;
		vertices[3*last + 1] = radialBodies[0].getPosition().y;
		vertices[3*last + 2] = 0;
		for(int i=0; i<radialBodies.length;i++){
			vertices[3*(i+1)+0] = radialBodies[i].getPosition().x;
			vertices[3*(i+1)+1] = radialBodies[i].getPosition().y;
			vertices[3*(i+1)+2] = 0;
		}
		
		float texture[] = new float[2+radialBodies.length*2+2];
		Vector2 center = new Vector2(0.5f,0.5f);
		Vector2 radial = new Vector2(0.5f,0.0f);
		
		texture[0] = center.x;
		texture[1] = center.y;
		texture[2*last + 0] = center.x+radial.x;
		texture[2*last + 1] = center.y+radial.y;
		
		for(int i=0; i<radialBodies.length;i++){
			texture[2*(i+1)+0] = center.x+radial.x;
			texture[2*(i+1)+1] = center.y+radial.y;
			radial.rotate(bodyAngle);
		}
		
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(vertices.length * 4); 
		byteBuffer.order(ByteOrder.nativeOrder());
		vertexBuffer = byteBuffer.asFloatBuffer();
		vertexBuffer.put(vertices);
		vertexBuffer.position(0);

		byteBuffer = ByteBuffer.allocateDirect(texture.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		textureBuffer = byteBuffer.asFloatBuffer();
		textureBuffer.put(texture);
		textureBuffer.position(0);
		
		this.count = vertices.length/3;
	}


	public void draw(GL10 gl) {
		// bind the previously generated texture
		gumTexture.bind();
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, textureBuffer);
		gl.glDrawArrays(GL10.GL_TRIANGLE_FAN, 0, count);
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		
	}

	

	public void customDraw(){
		initBuffer();
		draw( Gdx.graphics.getGL10());
	}


	public boolean getStarFound() {
		return this.starFound;
	}
	
	public void foundStar(){
		this.starFound = true;
		sticky.getSounds().starFound();
	}
	
	public Body[] getRadialBodies() {
		return radialBodies;
	}


	
}
