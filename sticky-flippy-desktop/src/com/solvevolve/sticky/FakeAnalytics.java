package com.solvevolve.sticky;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.solvevolve.actors.MyBoxActor;

public class FakeAnalytics implements Analytics {

	@Override
	public void logSucess(Level level, float time, boolean foundStar,
			boolean beatTime) {
		Map<String, String> articleParams = new HashMap<String, String>();

		articleParams.put("Time", MathUtils.floor(time*100)/100f+"");
		articleParams.put("Secret", foundStar+"");
		articleParams.put("Bet Time", beatTime+"");

		
		
	}

	@Override
	public void logFail(Level level, float time, MyBoxActor goodOne) {
		Vector2 pos = goodOne.body.getPosition();
		pos.x = MathUtils.ceil(pos.x*10)/10f;
		pos.y = MathUtils.ceil(pos.y*10)/10f;
		Map<String, String> articleParams = new HashMap<String, String>();

		articleParams.put("Time", MathUtils.floor(time*100)/100f+"");
		articleParams.put("DueTo", goodOne.getClass().getSimpleName());
		articleParams.put("Position", pos.toString());

		
	}

}
